# TallOrder API

TypeScript + Serverless project

# Getting Started

```
npm install -g typescript
npm install -g tslint
npm install -g serverless
npm install -g sequelize-auto
npm install
```


# Development

Check out package.json `scripts` section for all available scripts

The most important ones are

```
npm run build       # will compile the project into the dist folder
npm run pack        # will create the zip artifact for Lambda
npm run deploy      # will deploy the project as a lambda function

npm run bpd         # will do all 3 above
```

You have to define the `DEV_ALIAS` environment variable to ensure that your deployment
is independent of the other devvers.

*Mac* - put the following line in your `~/.bash_profile`

```
export DEV_ALIAS=mdw
```

## Tailing the logs

```
serverless logs -f get-all-devices -t
```

# Config

Serverless configration is in serverless.yml

check out https://serverless.com/framework/docs/providers/aws/guide/serverless.yml

We're going to define different function entry points,

# Database Models

We need to regenerate the Sequelize models whenever changes are made to the database.

Use the npm scripts to update the files and commit the changes to the repo

```
npm run generate-models:tallorder
```

# Dev Endpoints

https://b9rcmvk3t4.execute-api.eu-west-1.amazonaws.com/dev/device-register?tid={tenantID}

https://b9rcmvk3t4.execute-api.eu-west-1.amazonaws.com/dev/device-register/{id}/?{tenantID}
