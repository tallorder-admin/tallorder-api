const shell = require('shelljs');

console.log('removing ./dist/node_module');
shell.rm('-rf', './dist/node_modules');
shell.mkdir('-p', './tmp');
shell.cd('tmp');
shell.cp('../package.json', './');

console.log('installing production packages');
shell.exec('npm install --production');

console.log('moving to dist...');
shell.cp('-R','./node_modules', '../dist/');

console.log('cleaning up');
shell.cd('..');
shell.rm('-rf', './tmp');
console.log('done!');
