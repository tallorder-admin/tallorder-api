const cmd = require('node-cmd');
const Promise = require('bluebird');

let promise;

const getAsync = Promise.promisify(cmd.get, {multiArgs: true, context: cmd});


if (process.platform === 'darwin') {
    console.log(process.cwd());
    promise = getAsync('cd dist && zip -r ../tallorder-api-ts.zip ./* > /dev/null');

} else {
    promise = new Promise((resolve, reject) => {
        let zip = require('bestzip');
        console.log('Now zipping contents of ./dist folder to ./tallorder-api-ts.zip');
        zip('./tallorder-api-ts.zip', ['./dist/*'], function (err) {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    })

}


Promise.resolve(promise)
    .then(() => {
        console.log('./dist folder zip success: ./tallorder-api-ts.zip');
    }).catch(err => {
    console.log('./dist folder zip error:', err);
});
