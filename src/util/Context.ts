export class Context {
    static logger: any = {
        info: (scope, text) => console.log(`${scope} ${text ? text : ''}`),
        debug: (scope, text) => console.log(`${scope} ${text ? text : ''}`),
        silly: (scope, text) => console.log(`${scope} ${text ? text : ''}`),
        warn: (scope, text) => console.log(`${scope} ${text ? text : ''}`),
        error: (text) => console.log(`ERROR ${text ? text : ''}`),
    };
    static tallorder: any;
    static sequelize: any;
    static tallorderBI: any;
    static tenantId: number;
    static userId: number;
    static isLambda: boolean;
    static auth: object;
}
