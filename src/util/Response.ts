import {HttpError} from "./Error";

export class Response {

    public static error(error: HttpError) {
        return {
            statusCode: error.code,
            headers: {
                "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
            },
            body: JSON.stringify({
                result: 'ERROR',
                type: error.type,
                ref: new Date().toTimeString(),
                reason: error.message
            })
        };
    }

    public static format(rsp: any, headers: object = {}) {

        headers["Access-Control-Allow-Origin"] = "*";
        headers["Access-Control-Allow-Credentials"] = true;

        return {
            statusCode: 200,
            headers: headers,
            body: JSON.stringify(rsp)
        };
    }

}
