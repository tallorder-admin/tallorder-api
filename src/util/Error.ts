export class HttpError extends Error {
    get code(): number {
        return this._code;
    }


    constructor(code: number, type: string, message: string) {
        super(message);
        this._code = code;
        this._type = type;
    }


    private _code: number;
    private _type: string;

    get type(): string {
        return this._type;
    }

    public toString() {
        return JSON.stringify({
            code: this.code,
            type: this.type,
            message: this.message,
        });
    }
}