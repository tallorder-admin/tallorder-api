export function associations(db) {

    db.Menus.hasMany(db.MenuTree,{
        foreignKey: "MenuID"
    });

    db.MenuTree.belongsTo(db.MenuItems,{
        foreignKey: "ItemID"
    });


    db.MenuItems.hasMany(db.MenuAddOns, {
        foreignKey: "MenuItemID"
    });

    db.MenuItems.hasMany(db.MenuOptions, {
        foreignKey: "MenuItemID"
    });

    db.MenuItems.hasMany(db.MenuPortions, {
        foreignKey: "MenuItemID"
    });

    db.MenuItems.hasMany(db.RecipeIngredients, {
        foreignKey: "IngredientMenuItemID"
    });

    db.MenuItems.hasOne(db.StockItems, {
        foreignKey: "MenuItem"
    });

    db.MenuItems.belongsTo(db.TaxGroups,{
        foreignKey: "TaxGroupID"
    });

    db.TaxGroups.hasMany(db.TaxTypeLinks,{
        foreignKey: "TaxGroupID"
    });

    db.TaxTypeLinks.belongsTo(db.TaxTypes,{
        foreignKey: "TaxTypeID"
    });


    db.MenuItems.belongsTo(db.MenuItemCategory,{
        foreignKey: "CategoryID"
    });

    db.MenuItems.belongsTo(db.StockUnits,{
        foreignKey: "UnitID"
    });

    db.MenuItems.hasMany(db.MenuItemTags, {
        foreignKey: "MenuItemID"
    });

    db.MenuItemTags.belongsTo(db.Tag,{
        foreignKey: "TagID"
    });

    db.MenuItems.hasOne(db.StockLevels, {
        foreignKey: "MenuItemID"
    });


    db.MenuItems.belongsTo(db.StockUnits,{
        foreignKey: "UnitID"
    });


    db.MenuItems.hasOne(db.ItemLocal, {
        foreignKey: "MenuItemID"
    });

    db.MenuItems.hasOne(db.Descriptions, {
        foreignKey: "MenuItemID"
    });

    db.StockItems.belongsTo(db.StockUnits, {
        foreignKey: "BaseUnit"
    });

    db.StockItems.hasMany(db.StockItemSuppliers, {
        foreignKey: "StockItem"
    });

    db.StockItemSuppliers.belongsTo(db.TenantSuppliers, {
        foreignKey: "Supplier"
    });


    db.StockItemSuppliers.hasMany(db.StockItemSupplierQuantities, {
        foreignKey: "StockItemSupplierID"
    });

    db.StockItemSupplierQuantities.belongsTo(db.StockItemUnits, {
        foreignKey: "StockItemUnitID"
    });

    db.StockItemSuppliers.belongsTo(db.StockItemUnits, {
        foreignKey: "EconomicOrderStockItemUnitID"
    });

    db.StockItemUnits.belongsTo(db.StockUnits, {
        foreignKey: "StockUnitID"
    });


    db.StockItems.hasMany(db.StockItemStockTakeTypes, {
        foreignKey: "StockItemID"
    });


    db.StockItemStockTakeTypes.belongsTo(db.StockTakeTypes, {
        foreignKey: "StockTakeTypeID"
    });
    
    
    db.DeviceRegister.belongsTo(db.DeviceTypes,{
        foreignKey: "DeviceTypeID"
    });

}