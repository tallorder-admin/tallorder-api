export class ArrayHelper {

    public static getColumn(obj: Array<object>, colName: string) {
        //  reduce the provided list to an array only containing the requested field
        const arr = [];
        for (const item of obj) {
            //  check if the item is actually an object and does contain the field
            if (typeof item === 'object' && colName in item) {
                arr.push(item[colName]);
            }
        }
        return arr;

    }

    public static containsAll(...arr: any[]) {
        const output: Array<string> = [];
        const cntObj: object = {};
        let array: Array<string> = [];
        let item: string = "";
        let cnt: number = 0;
        // for each array passed as an argument to the function
        for (let i = 0; i < arguments.length; i++) {
            array = arguments[i];
            // for each element in the array
            for (let j = 0; j < array.length; j++) {
                item = "-" + array[j];
                cnt = cntObj[item] || 0;
                // if cnt is exactly the number of previous arrays,
                // then increment by one so we count only one per array
                if (cnt == i) {
                    cntObj[item] = cnt + 1;
                }
            }
        }
        // now collect all results that are in all arrays
        for (item in cntObj) {
            if (cntObj.hasOwnProperty(item) && cntObj[item] === arguments.length) {
                output.push(item.substring(1));
            }
        }
        return(output);
    }



}

