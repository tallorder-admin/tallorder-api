/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {DynamicMasterDataTypeServicesInstance, DynamicMasterDataTypeServicesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<DynamicMasterDataTypeServicesInstance, DynamicMasterDataTypeServicesAttribute>('DynamicMasterDataTypeServices', {
    DynamicMasterDataTypeID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'DynamicMasterDataTypes',
        key: 'ID'
      }
    },
    DynamicServiceConfigID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'DynamicServiceConfig',
        key: 'ID'
      }
    },
    EventType: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    Service: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    Description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'DynamicMasterDataTypeServices'
  });
};
