/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {BrandItemUnitsInstance, BrandItemUnitsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<BrandItemUnitsInstance, BrandItemUnitsAttribute>('BrandItemUnits', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    BrandItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'BrandItems',
        key: 'ID'
      }
    },
    StockUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'StockUnits',
        key: 'ID'
      }
    },
    Barcode: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'BrandItemUnits'
  });
};
