/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {POSIntVersionsInstance, POSIntVersionsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<POSIntVersionsInstance, POSIntVersionsAttribute>('POSIntVersions', {
    ID: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    POSVendorID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TOPIVer: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    POSVer: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    POSAPIVer: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'POSIntVersions'
  });
};
