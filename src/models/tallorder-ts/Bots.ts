/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {BotsInstance, BotsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<BotsInstance, BotsAttribute>('Bots', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    BotName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    MenuID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Menus',
        key: 'ID'
      }
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'Bots'
  });
};
