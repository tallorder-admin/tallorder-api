/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {faqInstance, faqAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<faqInstance, faqAttribute>('faq', {
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    parent: {
      type: DataTypes.INTEGER(10),
      allowNull: false
    },
    title: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    detail: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    faq_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false
    },
    order: {
      type: DataTypes.INTEGER(5),
      allowNull: false
    }
  }, {
    tableName: 'faq'
  });
};
