/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {AddOnEnumerationValuesInstance, AddOnEnumerationValuesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<AddOnEnumerationValuesInstance, AddOnEnumerationValuesAttribute>('AddOnEnumerationValues', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    AddOnSlotTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'AddOnGroupNameSlotTypes',
        key: 'ID'
      }
    },
    AddOnEnumerationValue: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AddOnID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'AddOns',
        key: 'ID'
      }
    }
  }, {
    tableName: 'AddOnEnumerationValues'
  });
};
