/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {BotIntentsInstance, BotIntentsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<BotIntentsInstance, BotIntentsAttribute>('BotIntents', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    IDUtteranceGroup: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'BotUtteranceGroups',
        key: 'ID'
      }
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    ConfirmationPrompt: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ConfirmationCancel: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    SlotType: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    SlotName: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    LambdaFunction: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'BotIntents'
  });
};
