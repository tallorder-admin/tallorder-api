/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {DiscountsInstance, DiscountsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<DiscountsInstance, DiscountsAttribute>('Discounts', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    DiscountType: {
      type: DataTypes.ENUM('Percentage','Value'),
      allowNull: true,
      defaultValue: 'Percentage'
    },
    Discount: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00'
    },
    StartDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    EndDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    StartTime: {
      type: DataTypes.TIME,
      allowNull: true
    },
    EndTime: {
      type: DataTypes.TIME,
      allowNull: true
    },
    DaysOfWeek: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    MinQty: {
      type: DataTypes.INTEGER(9),
      allowNull: true
    },
    MaxQty: {
      type: DataTypes.INTEGER(9),
      allowNull: true
    },
    CustomerTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'CustomerTypes',
        key: 'ID'
      }
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'MenuItems',
        key: 'ID'
      }
    },
    MenuItemCategoryID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'MenuItemCategory',
        key: 'ID'
      }
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'Discounts'
  });
};
