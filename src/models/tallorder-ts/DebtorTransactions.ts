/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {DebtorTransactionsInstance, DebtorTransactionsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<DebtorTransactionsInstance, DebtorTransactionsAttribute>('DebtorTransactions', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    AccountID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Accounts',
        key: 'ID'
      }
    },
    TransactionType: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    Reference: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    Amount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    Age: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    AmountDue: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'DebtorTransactions'
  });
};
