/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {RecipeIngredientsInstance, RecipeIngredientsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<RecipeIngredientsInstance, RecipeIngredientsAttribute>('RecipeIngredients', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    RecipeMenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'MenuItems',
        key: 'ID'
      }
    },
    IngredientMenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'MenuItems',
        key: 'ID'
      }
    },
    Quantity: {
      type: "DOUBLE",
      allowNull: true
    },
    StockUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'StockUnits',
        key: 'ID'
      }
    },
    WastePercentage: {
      type: "DOUBLE",
      allowNull: true
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'RecipeIngredients'
  });
};
