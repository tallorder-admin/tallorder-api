/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {AccessPermissionsInstance, AccessPermissionsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<AccessPermissionsInstance, AccessPermissionsAttribute>('AccessPermissions', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Controller: {
      type: DataTypes.STRING(75),
      allowNull: false
    },
    Action: {
      type: DataTypes.STRING(55),
      allowNull: false
    },
    eType: {
      type: DataTypes.ENUM('eStaffType','eProdType','eTenantType'),
      allowNull: true,
      defaultValue: 'eStaffType'
    },
    eTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Allowed: {
      type: DataTypes.INTEGER(1).UNSIGNED,
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'AccessPermissions'
  });
};
