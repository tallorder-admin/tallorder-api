/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TabOrderStatusInstance, TabOrderStatusAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TabOrderStatusInstance, TabOrderStatusAttribute>('TabOrderStatus', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TabID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Note: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    OrderStatusID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TabOrderStatus'
  });
};
