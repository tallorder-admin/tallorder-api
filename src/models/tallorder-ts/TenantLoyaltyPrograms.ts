/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantLoyaltyProgramsInstance, TenantLoyaltyProgramsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantLoyaltyProgramsInstance, TenantLoyaltyProgramsAttribute>('TenantLoyaltyPrograms', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ProgramID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    LoyaltyNr: {
      type: DataTypes.CHAR(20),
      allowNull: false
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TenantLoyaltyPrograms'
  });
};
