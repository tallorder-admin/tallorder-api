/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {MenuItemSyncHistoryInstance, MenuItemSyncHistoryAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<MenuItemSyncHistoryInstance, MenuItemSyncHistoryAttribute>('MenuItemSyncHistory', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    AccountingItemID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    LocalItemData: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    AccountingItemData: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    SyncDateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'MenuItemSyncHistory'
  });
};
