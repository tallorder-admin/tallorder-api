/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TaxTypesInstance, TaxTypesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TaxTypesInstance, TaxTypesAttribute>('TaxTypes', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TaxRegime: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '1'
    },
    TaxType: {
      type: DataTypes.CHAR(50),
      allowNull: true
    },
    Seq: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    InclusiveTax: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '1'
    },
    TaxRate: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    TaxReference: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    HostTaxID: {
      type: DataTypes.STRING(75),
      allowNull: true
    },
    TaxStartDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    TaxEndDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TaxTypes'
  });
};
