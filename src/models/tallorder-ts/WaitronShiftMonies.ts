/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {WaitronShiftMoniesInstance, WaitronShiftMoniesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<WaitronShiftMoniesInstance, WaitronShiftMoniesAttribute>('WaitronShiftMonies', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    WaitronShiftID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TenantPaymentTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Amount: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'WaitronShiftMonies'
  });
};
