/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ToGoFeaturedItemsInstance, ToGoFeaturedItemsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ToGoFeaturedItemsInstance, ToGoFeaturedItemsAttribute>('ToGoFeaturedItems', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'MenuItems',
        key: 'ID'
      }
    },
    MediaID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Media',
        key: 'ID'
      }
    },
    Seq: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    PriceText: {
      type: DataTypes.STRING(127),
      allowNull: true
    },
    Title: {
      type: DataTypes.STRING(127),
      allowNull: true
    },
    SubTitle: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Description: {
      type: DataTypes.STRING(511),
      allowNull: true
    },
    Active: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '1'
    },
    StartDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    EndDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'ToGoFeaturedItems'
  });
};
