/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {WaitronRatingsInstance, WaitronRatingsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<WaitronRatingsInstance, WaitronRatingsAttribute>('WaitronRatings', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TabID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    RatingDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    AttitudeRatng: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SpeedRating: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OverallRating: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'WaitronRatings'
  });
};
