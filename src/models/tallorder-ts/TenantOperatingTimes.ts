/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantOperatingTimesInstance, TenantOperatingTimesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantOperatingTimesInstance, TenantOperatingTimesAttribute>('TenantOperatingTimes', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    WeekDay: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    StartTime: {
      type: DataTypes.TIME,
      allowNull: true,
      defaultValue: '08:00:00'
    },
    EndTime: {
      type: DataTypes.TIME,
      allowNull: true,
      defaultValue: '17:00:00'
    },
    OpArea: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: 'POS'
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'TenantOperatingTimes'
  });
};
