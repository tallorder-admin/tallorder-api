/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TagAttributesInstance, TagAttributesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TagAttributesInstance, TagAttributesAttribute>('TagAttributes', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TagID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tag',
        key: 'ID'
      }
    },
    AttributeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Attributes',
        key: 'ID'
      }
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TagAttributes'
  });
};
