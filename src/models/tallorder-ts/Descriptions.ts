/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {DescriptionsInstance, DescriptionsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<DescriptionsInstance, DescriptionsAttribute>('Descriptions', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    BrandItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    LangID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ItemName: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    Description: {
      type: DataTypes.STRING(512),
      allowNull: true
    },
    Nutrition: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    URL: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ProofRead: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'Descriptions'
  });
};
