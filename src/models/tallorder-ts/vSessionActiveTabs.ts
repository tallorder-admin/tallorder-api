/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {vSessionActiveTabsInstance, vSessionActiveTabsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<vSessionActiveTabsInstance, vSessionActiveTabsAttribute>('vSessionActiveTabs', {
    TabID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TableID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TabName: {
      type: DataTypes.CHAR(50),
      allowNull: true
    },
    Party: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ''
    },
    GuestCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OpenTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CloseTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CustomerID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    NetOrderTotal: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00000'
    },
    TaxTotal: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00000'
    },
    Gratuity: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00'
    },
    InvoiceTotal: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00'
    }
  }, {
    tableName: 'vSessionActiveTabs'
  });
};
