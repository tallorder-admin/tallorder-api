/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PortionEnumerationValuesInstance, PortionEnumerationValuesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PortionEnumerationValuesInstance, PortionEnumerationValuesAttribute>('PortionEnumerationValues', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    PortionSlotTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'PortionGroupNameSlotTypes',
        key: 'ID'
      }
    },
    PortionEnumerationValue: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PortionID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Portions',
        key: 'ID'
      }
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'PortionEnumerationValues'
  });
};
