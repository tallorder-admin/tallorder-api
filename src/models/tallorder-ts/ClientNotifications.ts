/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ClientNotificationsInstance, ClientNotificationsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ClientNotificationsInstance, ClientNotificationsAttribute>('ClientNotifications', {
    ID: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Title: {
      type: DataTypes.STRING(155),
      allowNull: false
    },
    DescriptionShort: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    DescriptionFull: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    Type: {
      type: DataTypes.STRING(25),
      allowNull: false
    },
    VersionPos: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    VersionTopi: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    Read: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ShowFrom: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SNSSent: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'ClientNotifications'
  });
};
