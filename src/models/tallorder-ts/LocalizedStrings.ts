/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {LocalizedStringsInstance, LocalizedStringsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<LocalizedStringsInstance, LocalizedStringsAttribute>('LocalizedStrings', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    EnumID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    LangID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Text: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'LocalizedStrings'
  });
};
