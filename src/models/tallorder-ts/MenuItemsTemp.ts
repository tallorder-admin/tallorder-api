/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {MenuItemsTempInstance, MenuItemsTempAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<MenuItemsTempInstance, MenuItemsTempAttribute>('MenuItemsTemp', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    MenuID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ParentID: {
      type: DataTypes.CHAR(15),
      allowNull: true
    },
    ItemID: {
      type: DataTypes.CHAR(15),
      allowNull: true
    },
    ShortName: {
      type: DataTypes.STRING(30),
      allowNull: false,
      defaultValue: 'New item'
    },
    CategoryID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Seq: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    IsModOnly: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    BrandItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ShortcutID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Cost: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    Price: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    PriceVar: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    IsTaxable: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    AllowLocalPrice: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ReqMgrSignOff: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    Notes: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    PrepTime: {
      type: DataTypes.TIME,
      allowNull: true
    },
    ServeFrom: {
      type: DataTypes.TIME,
      allowNull: true
    },
    ServeUntil: {
      type: DataTypes.TIME,
      allowNull: true
    },
    SrvSu: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvMo: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvTu: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvWe: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvTh: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvFr: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvSa: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    HostPLU: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    HostedAccountID: {
      type: DataTypes.STRING(75),
      allowNull: true
    },
    BarCode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    HostSalesDept: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    HaveImages: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    HasPromoImage: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    PromoServiceGroupID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    WaiterOnly: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AvailableToGo: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'MenuItemsTemp'
  });
};
