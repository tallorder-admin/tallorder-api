/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {SyncClashItemsInstance, SyncClashItemsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<SyncClashItemsInstance, SyncClashItemsAttribute>('SyncClashItems', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    MenueItemId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SyncItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SageOneItem: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'SyncClashItems'
  });
};
