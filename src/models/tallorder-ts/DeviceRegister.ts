/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {DeviceRegisterInstance, DeviceRegisterAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<DeviceRegisterInstance, DeviceRegisterAttribute>('DeviceRegister', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    DeviceTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '4'
    },
    Device: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DateReg: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Active: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '1'
    },
    Online: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    DeviceUID: {
      type: DataTypes.STRING(60),
      allowNull: false
    },
    Master: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    Address: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Hostname: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DefaultPrinter: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'StationDevices',
        key: 'ID'
      }
    },
    DevicePIN: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    PosGPS: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LastConnectDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Version: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    AuthCode: {
      type: DataTypes.STRING(16),
      allowNull: true
    },
    PushDeviceToken: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LocationID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Locations',
        key: 'ID'
      }
    }
  }, {
    tableName: 'DeviceRegister'
  });
};
