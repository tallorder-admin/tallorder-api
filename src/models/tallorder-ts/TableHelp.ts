/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TableHelpInstance, TableHelpAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TableHelpInstance, TableHelpAttribute>('TableHelp', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TableName: {
      type: DataTypes.CHAR(40),
      allowNull: false
    },
    TableEnum: {
      type: DataTypes.STRING(4),
      allowNull: false
    },
    LangID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ToolTip: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HelpText: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TableHelp'
  });
};
