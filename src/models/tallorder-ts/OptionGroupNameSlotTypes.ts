/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {OptionGroupNameSlotTypesInstance, OptionGroupNameSlotTypesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<OptionGroupNameSlotTypesInstance, OptionGroupNameSlotTypesAttribute>('OptionGroupNameSlotTypes', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    MenuID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Menus',
        key: 'ID'
      }
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'MenuItems',
        key: 'ID'
      }
    },
    OptionGroupID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'OptionGroups',
        key: 'ID'
      }
    },
    OptionSlotType: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BotID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'OptionGroupNameSlotTypes'
  });
};
