/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {LocaleIDInstance, LocaleIDAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<LocaleIDInstance, LocaleIDAttribute>('LocaleID', {
    LocaleID: {
      type: DataTypes.CHAR(12),
      allowNull: false,
      defaultValue: '',
      primaryKey: true
    },
    TimeZone: {
      type: DataTypes.STRING(150),
      allowNull: true
    }
  }, {
    tableName: 'LocaleID'
  });
};
