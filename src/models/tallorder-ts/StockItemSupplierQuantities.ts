/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StockItemSupplierQuantitiesInstance, StockItemSupplierQuantitiesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StockItemSupplierQuantitiesInstance, StockItemSupplierQuantitiesAttribute>('StockItemSupplierQuantities', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    StockItemSupplierID: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'StockItemSuppliers',
        key: 'ID'
      }
    },
    Description: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Barcode: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PLU: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    StockItemUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'StockItemUnits',
        key: 'ID'
      }
    },
    EconomicOrderQuantity: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    StockOrderUnit: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    StockOrderUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ConversionFactor: {
      type: "DOUBLE",
      allowNull: true,
      defaultValue: '1'
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'StockItemSupplierQuantities'
  });
};
