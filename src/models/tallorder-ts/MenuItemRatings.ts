/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {MenuItemRatingsInstance, MenuItemRatingsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<MenuItemRatingsInstance, MenuItemRatingsAttribute>('MenuItemRatings', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    CustomerID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    OrderID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    RatingDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Rating: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Comment: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'MenuItemRatings'
  });
};
