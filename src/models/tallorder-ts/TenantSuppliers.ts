/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantSuppliersInstance, TenantSuppliersAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantSuppliersInstance, TenantSuppliersAttribute>('TenantSuppliers', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    GlobalSupplierID: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'GlobalSuppliers',
        key: 'ID'
      }
    },
    Name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ContactPerson: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Phone: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Email: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Website: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TermsAndConditions: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    CreditLimit: {
      type: "DOUBLE",
      allowNull: true
    },
    AccountingSystemLink: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TenantSuppliers'
  });
};
