/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {WebsocketEventsInstance, WebsocketEventsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<WebsocketEventsInstance, WebsocketEventsAttribute>('WebsocketEvents', {
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true
    },
    EventSource: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EventDestination: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DeviceUid: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Type: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Data: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    RetryCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    Status: {
      type: DataTypes.STRING(20),
      allowNull: true,
      defaultValue: 'Sent'
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    UpdateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'WebsocketEvents'
  });
};
