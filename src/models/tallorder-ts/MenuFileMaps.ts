/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {MenuFileMapsInstance, MenuFileMapsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<MenuFileMapsInstance, MenuFileMapsAttribute>('MenuFileMaps', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    MenuID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Menus',
        key: 'ID'
      }
    },
    ColumnNames: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ColumnsMap: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    FileKey: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    BucketName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    FileS3Url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DefaultValueMap: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Updating: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    UpdateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    Error: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'MenuFileMaps'
  });
};
