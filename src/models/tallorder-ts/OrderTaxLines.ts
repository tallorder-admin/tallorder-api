/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {OrderTaxLinesInstance, OrderTaxLinesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<OrderTaxLinesInstance, OrderTaxLinesAttribute>('OrderTaxLines', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    OrderLineID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TaxTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    TaxID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ArchivedDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'OrderTaxLines'
  });
};
