/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantTargetsInstance, TenantTargetsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantTargetsInstance, TenantTargetsAttribute>('TenantTargets', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Name: {
      type: DataTypes.CHAR(30),
      allowNull: false
    },
    ePeriodID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    StartDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    EndDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    TargetMoney: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    BonusMoney: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    BonusPercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TenantTargets'
  });
};
