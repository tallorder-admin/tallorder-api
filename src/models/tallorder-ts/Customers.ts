/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {CustomersInstance, CustomersAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<CustomersInstance, CustomersAttribute>('Customers', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'User',
        key: 'ID'
      }
    },
    NFCTagUID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CustomerTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OTP: {
      type: DataTypes.CHAR(6),
      allowNull: true
    },
    LastVisit: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LastAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    MarketingPermissionFlag: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    LoyaltyProgRef: {
      type: DataTypes.CHAR(64),
      allowNull: true
    },
    Note: {
      type: "BLOB",
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    CustomerSource: {
      type: DataTypes.STRING(75),
      allowNull: true
    },
    CustomerSourceUserID: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    TaxReference: {
      type: DataTypes.STRING(75),
      allowNull: false
    },
    SourceAvailCredit: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00'
    },
    CreditLimit: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    ProfilePic: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    remote_ref: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'Customers'
  });
};
