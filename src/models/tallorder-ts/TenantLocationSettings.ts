/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantLocationSettingsInstance, TenantLocationSettingsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantLocationSettingsInstance, TenantLocationSettingsAttribute>('TenantLocationSettings', {
    ID: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TenantLocationID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'TenantLocations',
        key: 'ID'
      }
    },
    SettingID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Settings',
        key: 'ID'
      }
    },
    SettingValue: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    tableName: 'TenantLocationSettings'
  });
};
