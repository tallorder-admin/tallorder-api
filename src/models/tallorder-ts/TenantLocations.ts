/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantLocationsInstance, TenantLocationsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantLocationsInstance, TenantLocationsAttribute>('TenantLocations', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    LocationName: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    AddressTableID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'AddressTable',
        key: 'ID'
      }
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'TenantLocations'
  });
};
