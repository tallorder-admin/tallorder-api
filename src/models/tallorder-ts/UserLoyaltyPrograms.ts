/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {UserLoyaltyProgramsInstance, UserLoyaltyProgramsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<UserLoyaltyProgramsInstance, UserLoyaltyProgramsAttribute>('UserLoyaltyPrograms', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ProgramID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    LoyaltyNumber: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    DateLastUsed: {
      type: DataTypes.DATE,
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'UserLoyaltyPrograms'
  });
};
