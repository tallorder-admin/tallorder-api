/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {UserPreferencesInstance, UserPreferencesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<UserPreferencesInstance, UserPreferencesAttribute>('UserPreferences', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'User',
        key: 'ID'
      }
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PrefName: {
      type: DataTypes.STRING(55),
      allowNull: false
    },
    PrefValue: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    tableName: 'UserPreferences'
  });
};
