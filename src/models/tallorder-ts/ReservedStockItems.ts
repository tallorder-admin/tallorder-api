/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ReservedStockItemsInstance, ReservedStockItemsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ReservedStockItemsInstance, ReservedStockItemsAttribute>('ReservedStockItems', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    StockItem: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'StockItems',
        key: 'ID'
      }
    },
    CustomerOrder: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Quantity: {
      type: "DOUBLE",
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'ReservedStockItems'
  });
};
