/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {CreditAllocationsInstance, CreditAllocationsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<CreditAllocationsInstance, CreditAllocationsAttribute>('CreditAllocations', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    AccountID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Accounts',
        key: 'ID'
      }
    },
    InvoiceID: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    PaymentID: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    Amount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'CreditAllocations'
  });
};
