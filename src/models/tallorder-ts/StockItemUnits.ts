/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StockItemUnitsInstance, StockItemUnitsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StockItemUnitsInstance, StockItemUnitsAttribute>('StockItemUnits', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    StockItemID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'StockItems',
        key: 'ID'
      }
    },
    StockUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'StockUnits',
        key: 'ID'
      }
    },
    Barcode: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'StockItemUnits'
  });
};
