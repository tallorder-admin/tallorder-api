/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {SettingsInstance, SettingsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<SettingsInstance, SettingsAttribute>('Settings', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Name: {
      type: DataTypes.STRING(55),
      allowNull: false,
      unique: true
    },
    Description: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    SettingArea: {
      type: DataTypes.ENUM('Tenants','TenantLocations'),
      allowNull: true,
      defaultValue: 'TenantLocations'
    },
    SettingGroup: {
      type: DataTypes.STRING(55),
      allowNull: false
    },
    FieldType: {
      type: DataTypes.STRING(25),
      allowNull: true,
      defaultValue: 'textInput'
    },
    ListLabelValue: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    ValueMaxLen: {
      type: DataTypes.INTEGER(6).UNSIGNED,
      allowNull: false,
      defaultValue: '1'
    },
    DefaultValue: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Hint: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Required: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AdminOnly: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    Seq: {
      type: DataTypes.INTEGER(3),
      allowNull: false
    },
    SettingGroupIcon: {
      type: DataTypes.STRING(52),
      allowNull: false,
      defaultValue: ''
    },
    ShowRestaurant: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    ShowRetail: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    }
  }, {
    tableName: 'Settings'
  });
};
