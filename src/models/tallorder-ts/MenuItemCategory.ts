/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {MenuItemCategoryInstance, MenuItemCategoryAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<MenuItemCategoryInstance, MenuItemCategoryAttribute>('MenuItemCategory', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    CategoryName: {
      type: DataTypes.CHAR(40),
      allowNull: false
    },
    IsArchived: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    Seq: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'MenuItemCategory'
  });
};
