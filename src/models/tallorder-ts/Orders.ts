/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {OrdersInstance, OrdersAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<OrdersInstance, OrdersAttribute>('Orders', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    LineNo: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: '0'
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ParentOrder: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    AddOnID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Qty: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '1.00'
    },
    InvQty: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '1.00'
    },
    Price: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    LineTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    NetTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.000000000'
    },
    Discount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    TaxTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.000000000'
    },
    CostTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    TabID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    GuestNum: {
      type: DataTypes.INTEGER(6),
      allowNull: true,
      defaultValue: '0'
    },
    OnHold: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    OnHoldManual: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    HoldGroup: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    OrderDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DeliveredDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    PrimaryMenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PrintCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    Note: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HostOrderID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PaymentGroupID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SaleType: {
      type: DataTypes.STRING(10),
      allowNull: true,
      defaultValue: 'Sale'
    },
    Discounted: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    Voided: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    Returned: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    UserCode: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ArchivedDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    isRecommended: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'Orders'
  });
};
