/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ItemLocalInstance, ItemLocalAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ItemLocalInstance, ItemLocalAttribute>('ItemLocal', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'MenuItems',
        key: 'ID'
      }
    },
    IsActive: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    MgrAuthRequired: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    TabAuthRequired: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    IsAvailable: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SuspendToGo: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    WaitronAlert: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    GuestAlert: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LocalPrice: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    LocalAllowOpenPrice: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    LocalTaxGroupID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SpecialFlag: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    SQFlag: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    SpecialPrice: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SQPrice: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    StationID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    StationDeviceID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'StationDevices',
        key: 'ID'
      }
    },
    WtrnPoints: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    WtrnPointsEndDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    WtrnComPercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    WtrnComValue: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    WtrnComEndDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'ItemLocal'
  });
};
