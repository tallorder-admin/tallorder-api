/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {MenuTreeInstance, MenuTreeAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<MenuTreeInstance, MenuTreeAttribute>('MenuTree', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    MenuID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'MenuItems',
        key: 'ID'
      }
    },
    ParentID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    CloneTreeID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Seq: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    MenuTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '73',
      references: {
        model: 'AppEnums',
        key: 'ID'
      }
    }
  }, {
    tableName: 'MenuTree'
  });
};
