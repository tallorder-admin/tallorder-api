/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {OptionEnumerationValuesInstance, OptionEnumerationValuesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<OptionEnumerationValuesInstance, OptionEnumerationValuesAttribute>('OptionEnumerationValues', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    OptionSlotTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'OptionGroupNameSlotTypes',
        key: 'ID'
      }
    },
    OptionEnumerationValue: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    OptionID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Options',
        key: 'ID'
      }
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'OptionEnumerationValues'
  });
};
