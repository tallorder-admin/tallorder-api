/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StaffInstance, StaffAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StaffInstance, StaffAttribute>('Staff', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'User',
        key: 'ID'
      }
    },
    eTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    VoidPIN: {
      type: DataTypes.CHAR(6),
      allowNull: true
    },
    AssignDevicePIN: {
      type: DataTypes.CHAR(6),
      allowNull: true
    },
    DiscountPIN: {
      type: DataTypes.CHAR(6),
      allowNull: true
    },
    UserCode: {
      type: DataTypes.CHAR(6),
      allowNull: true
    },
    UserDef1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    UserDef2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HostID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    NFCTagUID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'Staff'
  });
};
