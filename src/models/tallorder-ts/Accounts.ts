/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {AccountsInstance, AccountsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<AccountsInstance, AccountsAttribute>('Accounts', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    CustomerID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Customers',
        key: 'ID'
      }
    },
    TotalOutstanding: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    Current: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    Days30: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    Days60: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    Days90: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    TermDays: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    LastPurchaseDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    LastPaymentDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    OverDue: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    HoldFlag: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    }
  }, {
    tableName: 'Accounts'
  });
};
