/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StaffShiftInstance, StaffShiftAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StaffShiftInstance, StaffShiftAttribute>('StaffShift', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    HostShiftID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    StaffID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    WaitronScheduleID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TableAreaID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    DeviceID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ReplacingWaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ReplacingWaitronReason: {
      type: DataTypes.ENUM('Worked','Cancelled','Sick','Absent','Rescheduled'),
      allowNull: true
    },
    DateTimeSignIn: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DateTimeSignOut: {
      type: DataTypes.DATE,
      allowNull: true
    },
    UserCode: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    InitialCashFloat: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    FinalCashFloat: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    TotalTablesServed: {
      type: DataTypes.INTEGER(3),
      allowNull: true
    },
    TotalRestaurantRevenue: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    TotalTips: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    CashDue: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    CommEarned: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    IncentiveComm: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    IncentivePoints: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    VoidsCount: {
      type: DataTypes.INTEGER(2),
      allowNull: true
    },
    VoidsValue: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    CashUpNote: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'StaffShift'
  });
};
