/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {UserDevicesInstance, UserDevicesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<UserDevicesInstance, UserDevicesAttribute>('UserDevices', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    DeviceID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    DeviceModelID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    DateFirstUse: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DateLastUse: {
      type: DataTypes.DATE,
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'UserDevices'
  });
};
