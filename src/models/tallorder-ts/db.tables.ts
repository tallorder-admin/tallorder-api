// tslint:disable
import * as path from 'path';
import * as sequelize from 'sequelize';
import * as def from './db';

export interface ITables {
  AccessPermissions:def.AccessPermissionsModel;
  Accounts:def.AccountsModel;
  AccessControllers:def.AccessControllersModel;
  AccountSystems:def.AccountSystemsModel;
  AccessActions:def.AccessActionsModel;
  AddOnEnumerationValues:def.AddOnEnumerationValuesModel;
  AddOnGroupNameSlotTypes:def.AddOnGroupNameSlotTypesModel;
  AddOnGroups:def.AddOnGroupsModel;
  AddOns:def.AddOnsModel;
  AddressTable:def.AddressTableModel;
  Admins:def.AdminsModel;
  ApiAccessKeys:def.ApiAccessKeysModel;
  ApiKeys:def.ApiKeysModel;
  AppEnums:def.AppEnumsModel;
  AppFeatures:def.AppFeaturesModel;
  AppNutrition:def.AppNutritionModel;
  Attributes:def.AttributesModel;
  Audit:def.AuditModel;
  AuditSources:def.AuditSourcesModel;
  BotConversationPrompts:def.BotConversationPromptsModel;
  BotLexTabUsers:def.BotLexTabUsersModel;
  BotIntents:def.BotIntentsModel;
  BotUtteranceGroups:def.BotUtteranceGroupsModel;
  BotUtterances:def.BotUtterancesModel;
  Bots:def.BotsModel;
  BrandCategories:def.BrandCategoriesModel;
  BrandItemUnits:def.BrandItemUnitsModel;
  BrandItems:def.BrandItemsModel;
  ClientNotifications:def.ClientNotificationsModel;
  Country:def.CountryModel;
  CreditAllocations:def.CreditAllocationsModel;
  Currency:def.CurrencyModel;
  CustomerSessions:def.CustomerSessionsModel;
  CustomerSyncHistory:def.CustomerSyncHistoryModel;
  CustomerTypes:def.CustomerTypesModel;
  Customers:def.CustomersModel;
  DebtorTransactions:def.DebtorTransactionsModel;
  Descriptions:def.DescriptionsModel;
  DeviceManufacturers:def.DeviceManufacturersModel;
  DeviceModels:def.DeviceModelsModel;
  DevicePairings:def.DevicePairingsModel;
  DeviceRegister:def.DeviceRegisterModel;
  DeviceTypes:def.DeviceTypesModel;
  Discounts:def.DiscountsModel;
  DynamicMasterDataTypeServices:def.DynamicMasterDataTypeServicesModel;
  DynamicMasterDataTypes:def.DynamicMasterDataTypesModel;
  DynamicServiceConfig:def.DynamicServiceConfigModel;
  ExRates:def.ExRatesModel;
  GlobalSuppliers:def.GlobalSuppliersModel;
  GlobalTenantTemplates:def.GlobalTenantTemplatesModel;
  InvoicePaymentInfo:def.InvoicePaymentInfoModel;
  ItemLocal:def.ItemLocalModel;
  Languages:def.LanguagesModel;
  LocaleID:def.LocaleIDModel;
  LocalizedStrings:def.LocalizedStringsModel;
  Locations:def.LocationsModel;
  LoyaltyPrograms:def.LoyaltyProgramsModel;
  LoyaltySponsors:def.LoyaltySponsorsModel;
  MasterDataTypeServices:def.MasterDataTypeServicesModel;
  MasterDataTypes:def.MasterDataTypesModel;
  Media:def.MediaModel;
  MediaTags:def.MediaTagsModel;
  MenuAddOns:def.MenuAddOnsModel;
  MenuDiffs:def.MenuDiffsModel;
  MenuFileMaps:def.MenuFileMapsModel;
  MenuItemCategory:def.MenuItemCategoryModel;
  MenuItemFavorites:def.MenuItemFavoritesModel;
  MenuItemIntents:def.MenuItemIntentsModel;
  MenuItemRatings:def.MenuItemRatingsModel;
  MenuItemSyncHistory:def.MenuItemSyncHistoryModel;
  MenuItemTags:def.MenuItemTagsModel;
  MenuItems:def.MenuItemsModel;
  MenuItemsTemp:def.MenuItemsTempModel;
  MenuOptions:def.MenuOptionsModel;
  MenuPortions:def.MenuPortionsModel;
  MenuPublishVersions:def.MenuPublishVersionsModel;
  MenuTree:def.MenuTreeModel;
  Menus:def.MenusModel;
  Nav:def.NavModel;
  Nutrition:def.NutritionModel;
  OptionEnumerationValues:def.OptionEnumerationValuesModel;
  OptionGroupNameSlotTypes:def.OptionGroupNameSlotTypesModel;
  OptionGroups:def.OptionGroupsModel;
  Options:def.OptionsModel;
  OrderAddOns:def.OrderAddOnsModel;
  OrderOptions:def.OrderOptionsModel;
  OrderPortion:def.OrderPortionModel;
  OrderTaxLines:def.OrderTaxLinesModel;
  Orders:def.OrdersModel;
  OverridePLU:def.OverridePLUModel;
  POSIntVersions:def.POSIntVersionsModel;
  POSVendors:def.POSVendorsModel;
  PastelCSVUploads:def.PastelCSVUploadsModel;
  PastelProducts:def.PastelProductsModel;
  PastelTenantConfig:def.PastelTenantConfigModel;
  PastelTenantCustomers:def.PastelTenantCustomersModel;
  PaymentInfo:def.PaymentInfoModel;
  PaymentType:def.PaymentTypeModel;
  PeachPayments:def.PeachPaymentsModel;
  PettyCashReasons:def.PettyCashReasonsModel;
  PettyPayments:def.PettyPaymentsModel;
  PortionEnumerationValues:def.PortionEnumerationValuesModel;
  PortionGroupNameSlotTypes:def.PortionGroupNameSlotTypesModel;
  PortionGroups:def.PortionGroupsModel;
  Portions:def.PortionsModel;
  PosDatabaseBackups:def.PosDatabaseBackupsModel;
  PosSyncDispatcherLog:def.PosSyncDispatcherLogModel;
  PrinterDefaults:def.PrinterDefaultsModel;
  Printers:def.PrintersModel;
  PurchaseOrderItems:def.PurchaseOrderItemsModel;
  PublishedSettings:def.PublishedSettingsModel;
  PurchaseOrders:def.PurchaseOrdersModel;
  RecipeIngredients:def.RecipeIngredientsModel;
  ReservedStockItems:def.ReservedStockItemsModel;
  ResourceSyncLog:def.ResourceSyncLogModel;
  RestaurantRatings:def.RestaurantRatingsModel;
  SagePettyCashAccounts:def.SagePettyCashAccountsModel;
  ServiceConfig:def.ServiceConfigModel;
  Settings:def.SettingsModel;
  ServiceEvent:def.ServiceEventModel;
  Staff:def.StaffModel;
  StaffRoster:def.StaffRosterModel;
  StaffShift:def.StaffShiftModel;
  StaffTargets:def.StaffTargetsModel;
  Station:def.StationModel;
  StationDevices:def.StationDevicesModel;
  StockAdjustments:def.StockAdjustmentsModel;
  StockItemLevels:def.StockItemLevelsModel;
  StockItemStockTakeTypes:def.StockItemStockTakeTypesModel;
  StockItemSupplierQuantities:def.StockItemSupplierQuantitiesModel;
  StockItemSuppliers:def.StockItemSuppliersModel;
  StockItemUnits:def.StockItemUnitsModel;
  StockItems:def.StockItemsModel;
  StockLevels:def.StockLevelsModel;
  StockTakeTypes:def.StockTakeTypesModel;
  StockUnitConversions:def.StockUnitConversionsModel;
  StockUnits:def.StockUnitsModel;
  SyncClashItems:def.SyncClashItemsModel;
  SyncItems:def.SyncItemsModel;
  TabOrderStatus:def.TabOrderStatusModel;
  TabPaymentGroups:def.TabPaymentGroupsModel;
  TableAreas:def.TableAreasModel;
  TableHelp:def.TableHelpModel;
  Tables:def.TablesModel;
  Tabs:def.TabsModel;
  Tag:def.TagModel;
  TagAttributes:def.TagAttributesModel;
  TaxGroups:def.TaxGroupsModel;
  TaxLines:def.TaxLinesModel;
  TaxRegimes:def.TaxRegimesModel;
  TaxTypeLinks:def.TaxTypeLinksModel;
  TaxTypes:def.TaxTypesModel;
  TenantAccountSystem:def.TenantAccountSystemModel;
  TenantAndMenuPublishVersions:def.TenantAndMenuPublishVersionsModel;
  TenantDiffs:def.TenantDiffsModel;
  TenantLanguages:def.TenantLanguagesModel;
  TenantLocationSettings:def.TenantLocationSettingsModel;
  TenantLocations:def.TenantLocationsModel;
  TenantLoyaltyPrograms:def.TenantLoyaltyProgramsModel;
  TenantOperatingTimes:def.TenantOperatingTimesModel;
  TenantPaymentType:def.TenantPaymentTypeModel;
  TenantPerpShifts:def.TenantPerpShiftsModel;
  TenantPublishVersions:def.TenantPublishVersionsModel;
  TenantServiceConfig:def.TenantServiceConfigModel;
  TenantServices:def.TenantServicesModel;
  TenantSettings:def.TenantSettingsModel;
  TenantSuppliers:def.TenantSuppliersModel;
  TenantTOPIAddresses:def.TenantTOPIAddressesModel;
  TenantTagLinks:def.TenantTagLinksModel;
  TenantTags:def.TenantTagsModel;
  TenantTargets:def.TenantTargetsModel;
  TenantTemplates:def.TenantTemplatesModel;
  Tenants:def.TenantsModel;
  ToGoFeaturedItems:def.ToGoFeaturedItemsModel;
  TogoUserFeedback:def.TogoUserFeedbackModel;
  TopiLog:def.TopiLogModel;
  Translators:def.TranslatorsModel;
  User:def.UserModel;
  TransportType:def.TransportTypeModel;
  UserDevices:def.UserDevicesModel;
  UserPaymentMethods:def.UserPaymentMethodsModel;
  UserLoyaltyPrograms:def.UserLoyaltyProgramsModel;
  UserPreferences:def.UserPreferencesModel;
  WaitronDevice:def.WaitronDeviceModel;
  WaitronRatings:def.WaitronRatingsModel;
  WaitronService:def.WaitronServiceModel;
  WaitronShiftMonies:def.WaitronShiftMoniesModel;
  WaitronTables:def.WaitronTablesModel;
  WebSession:def.WebSessionModel;
  WebsocketEvent:def.WebsocketEventModel;
  WebsocketEvents:def.WebsocketEventsModel;
  auth_consumer:def.auth_consumerModel;
  auth_consumer_nonce:def.auth_consumer_nonceModel;
  auth_token:def.auth_tokenModel;
  faq:def.faqModel;
  auth_token_type:def.auth_token_typeModel;
  faq_list:def.faq_listModel;
  flyway_schema_history:def.flyway_schema_historyModel;
  migration:def.migrationModel;
  play_evolutions:def.play_evolutionsModel;
  suppliers:def.suppliersModel;
  vPrintDevices:def.vPrintDevicesModel;
  vSessionActiveTabs:def.vSessionActiveTabsModel;
  vTabs:def.vTabsModel;
  v_today:def.v_todayModel;
}

export const getModels = function(seq:sequelize.Sequelize):ITables {
  const tables:ITables = {
    AccessPermissions: seq.import(path.join(__dirname, './AccessPermissions')),
    Accounts: seq.import(path.join(__dirname, './Accounts')),
    AccessControllers: seq.import(path.join(__dirname, './AccessControllers')),
    AccountSystems: seq.import(path.join(__dirname, './AccountSystems')),
    AccessActions: seq.import(path.join(__dirname, './AccessActions')),
    AddOnEnumerationValues: seq.import(path.join(__dirname, './AddOnEnumerationValues')),
    AddOnGroupNameSlotTypes: seq.import(path.join(__dirname, './AddOnGroupNameSlotTypes')),
    AddOnGroups: seq.import(path.join(__dirname, './AddOnGroups')),
    AddOns: seq.import(path.join(__dirname, './AddOns')),
    AddressTable: seq.import(path.join(__dirname, './AddressTable')),
    Admins: seq.import(path.join(__dirname, './Admins')),
    ApiAccessKeys: seq.import(path.join(__dirname, './ApiAccessKeys')),
    ApiKeys: seq.import(path.join(__dirname, './ApiKeys')),
    AppEnums: seq.import(path.join(__dirname, './AppEnums')),
    AppFeatures: seq.import(path.join(__dirname, './AppFeatures')),
    AppNutrition: seq.import(path.join(__dirname, './AppNutrition')),
    Attributes: seq.import(path.join(__dirname, './Attributes')),
    Audit: seq.import(path.join(__dirname, './Audit')),
    AuditSources: seq.import(path.join(__dirname, './AuditSources')),
    BotConversationPrompts: seq.import(path.join(__dirname, './BotConversationPrompts')),
    BotLexTabUsers: seq.import(path.join(__dirname, './BotLexTabUsers')),
    BotIntents: seq.import(path.join(__dirname, './BotIntents')),
    BotUtteranceGroups: seq.import(path.join(__dirname, './BotUtteranceGroups')),
    BotUtterances: seq.import(path.join(__dirname, './BotUtterances')),
    Bots: seq.import(path.join(__dirname, './Bots')),
    BrandCategories: seq.import(path.join(__dirname, './BrandCategories')),
    BrandItemUnits: seq.import(path.join(__dirname, './BrandItemUnits')),
    BrandItems: seq.import(path.join(__dirname, './BrandItems')),
    ClientNotifications: seq.import(path.join(__dirname, './ClientNotifications')),
    Country: seq.import(path.join(__dirname, './Country')),
    CreditAllocations: seq.import(path.join(__dirname, './CreditAllocations')),
    Currency: seq.import(path.join(__dirname, './Currency')),
    CustomerSessions: seq.import(path.join(__dirname, './CustomerSessions')),
    CustomerSyncHistory: seq.import(path.join(__dirname, './CustomerSyncHistory')),
    CustomerTypes: seq.import(path.join(__dirname, './CustomerTypes')),
    Customers: seq.import(path.join(__dirname, './Customers')),
    DebtorTransactions: seq.import(path.join(__dirname, './DebtorTransactions')),
    Descriptions: seq.import(path.join(__dirname, './Descriptions')),
    DeviceManufacturers: seq.import(path.join(__dirname, './DeviceManufacturers')),
    DeviceModels: seq.import(path.join(__dirname, './DeviceModels')),
    DevicePairings: seq.import(path.join(__dirname, './DevicePairings')),
    DeviceRegister: seq.import(path.join(__dirname, './DeviceRegister')),
    DeviceTypes: seq.import(path.join(__dirname, './DeviceTypes')),
    Discounts: seq.import(path.join(__dirname, './Discounts')),
    DynamicMasterDataTypeServices: seq.import(path.join(__dirname, './DynamicMasterDataTypeServices')),
    DynamicMasterDataTypes: seq.import(path.join(__dirname, './DynamicMasterDataTypes')),
    DynamicServiceConfig: seq.import(path.join(__dirname, './DynamicServiceConfig')),
    ExRates: seq.import(path.join(__dirname, './ExRates')),
    GlobalSuppliers: seq.import(path.join(__dirname, './GlobalSuppliers')),
    GlobalTenantTemplates: seq.import(path.join(__dirname, './GlobalTenantTemplates')),
    InvoicePaymentInfo: seq.import(path.join(__dirname, './InvoicePaymentInfo')),
    ItemLocal: seq.import(path.join(__dirname, './ItemLocal')),
    Languages: seq.import(path.join(__dirname, './Languages')),
    LocaleID: seq.import(path.join(__dirname, './LocaleID')),
    LocalizedStrings: seq.import(path.join(__dirname, './LocalizedStrings')),
    Locations: seq.import(path.join(__dirname, './Locations')),
    LoyaltyPrograms: seq.import(path.join(__dirname, './LoyaltyPrograms')),
    LoyaltySponsors: seq.import(path.join(__dirname, './LoyaltySponsors')),
    MasterDataTypeServices: seq.import(path.join(__dirname, './MasterDataTypeServices')),
    MasterDataTypes: seq.import(path.join(__dirname, './MasterDataTypes')),
    Media: seq.import(path.join(__dirname, './Media')),
    MediaTags: seq.import(path.join(__dirname, './MediaTags')),
    MenuAddOns: seq.import(path.join(__dirname, './MenuAddOns')),
    MenuDiffs: seq.import(path.join(__dirname, './MenuDiffs')),
    MenuFileMaps: seq.import(path.join(__dirname, './MenuFileMaps')),
    MenuItemCategory: seq.import(path.join(__dirname, './MenuItemCategory')),
    MenuItemFavorites: seq.import(path.join(__dirname, './MenuItemFavorites')),
    MenuItemIntents: seq.import(path.join(__dirname, './MenuItemIntents')),
    MenuItemRatings: seq.import(path.join(__dirname, './MenuItemRatings')),
    MenuItemSyncHistory: seq.import(path.join(__dirname, './MenuItemSyncHistory')),
    MenuItemTags: seq.import(path.join(__dirname, './MenuItemTags')),
    MenuItems: seq.import(path.join(__dirname, './MenuItems')),
    MenuItemsTemp: seq.import(path.join(__dirname, './MenuItemsTemp')),
    MenuOptions: seq.import(path.join(__dirname, './MenuOptions')),
    MenuPortions: seq.import(path.join(__dirname, './MenuPortions')),
    MenuPublishVersions: seq.import(path.join(__dirname, './MenuPublishVersions')),
    MenuTree: seq.import(path.join(__dirname, './MenuTree')),
    Menus: seq.import(path.join(__dirname, './Menus')),
    Nav: seq.import(path.join(__dirname, './Nav')),
    Nutrition: seq.import(path.join(__dirname, './Nutrition')),
    OptionEnumerationValues: seq.import(path.join(__dirname, './OptionEnumerationValues')),
    OptionGroupNameSlotTypes: seq.import(path.join(__dirname, './OptionGroupNameSlotTypes')),
    OptionGroups: seq.import(path.join(__dirname, './OptionGroups')),
    Options: seq.import(path.join(__dirname, './Options')),
    OrderAddOns: seq.import(path.join(__dirname, './OrderAddOns')),
    OrderOptions: seq.import(path.join(__dirname, './OrderOptions')),
    OrderPortion: seq.import(path.join(__dirname, './OrderPortion')),
    OrderTaxLines: seq.import(path.join(__dirname, './OrderTaxLines')),
    Orders: seq.import(path.join(__dirname, './Orders')),
    OverridePLU: seq.import(path.join(__dirname, './OverridePLU')),
    POSIntVersions: seq.import(path.join(__dirname, './POSIntVersions')),
    POSVendors: seq.import(path.join(__dirname, './POSVendors')),
    PastelCSVUploads: seq.import(path.join(__dirname, './PastelCSVUploads')),
    PastelProducts: seq.import(path.join(__dirname, './PastelProducts')),
    PastelTenantConfig: seq.import(path.join(__dirname, './PastelTenantConfig')),
    PastelTenantCustomers: seq.import(path.join(__dirname, './PastelTenantCustomers')),
    PaymentInfo: seq.import(path.join(__dirname, './PaymentInfo')),
    PaymentType: seq.import(path.join(__dirname, './PaymentType')),
    PeachPayments: seq.import(path.join(__dirname, './PeachPayments')),
    PettyCashReasons: seq.import(path.join(__dirname, './PettyCashReasons')),
    PettyPayments: seq.import(path.join(__dirname, './PettyPayments')),
    PortionEnumerationValues: seq.import(path.join(__dirname, './PortionEnumerationValues')),
    PortionGroupNameSlotTypes: seq.import(path.join(__dirname, './PortionGroupNameSlotTypes')),
    PortionGroups: seq.import(path.join(__dirname, './PortionGroups')),
    Portions: seq.import(path.join(__dirname, './Portions')),
    PosDatabaseBackups: seq.import(path.join(__dirname, './PosDatabaseBackups')),
    PosSyncDispatcherLog: seq.import(path.join(__dirname, './PosSyncDispatcherLog')),
    PrinterDefaults: seq.import(path.join(__dirname, './PrinterDefaults')),
    Printers: seq.import(path.join(__dirname, './Printers')),
    PurchaseOrderItems: seq.import(path.join(__dirname, './PurchaseOrderItems')),
    PublishedSettings: seq.import(path.join(__dirname, './PublishedSettings')),
    PurchaseOrders: seq.import(path.join(__dirname, './PurchaseOrders')),
    RecipeIngredients: seq.import(path.join(__dirname, './RecipeIngredients')),
    ReservedStockItems: seq.import(path.join(__dirname, './ReservedStockItems')),
    ResourceSyncLog: seq.import(path.join(__dirname, './ResourceSyncLog')),
    RestaurantRatings: seq.import(path.join(__dirname, './RestaurantRatings')),
    SagePettyCashAccounts: seq.import(path.join(__dirname, './SagePettyCashAccounts')),
    ServiceConfig: seq.import(path.join(__dirname, './ServiceConfig')),
    Settings: seq.import(path.join(__dirname, './Settings')),
    ServiceEvent: seq.import(path.join(__dirname, './ServiceEvent')),
    Staff: seq.import(path.join(__dirname, './Staff')),
    StaffRoster: seq.import(path.join(__dirname, './StaffRoster')),
    StaffShift: seq.import(path.join(__dirname, './StaffShift')),
    StaffTargets: seq.import(path.join(__dirname, './StaffTargets')),
    Station: seq.import(path.join(__dirname, './Station')),
    StationDevices: seq.import(path.join(__dirname, './StationDevices')),
    StockAdjustments: seq.import(path.join(__dirname, './StockAdjustments')),
    StockItemLevels: seq.import(path.join(__dirname, './StockItemLevels')),
    StockItemStockTakeTypes: seq.import(path.join(__dirname, './StockItemStockTakeTypes')),
    StockItemSupplierQuantities: seq.import(path.join(__dirname, './StockItemSupplierQuantities')),
    StockItemSuppliers: seq.import(path.join(__dirname, './StockItemSuppliers')),
    StockItemUnits: seq.import(path.join(__dirname, './StockItemUnits')),
    StockItems: seq.import(path.join(__dirname, './StockItems')),
    StockLevels: seq.import(path.join(__dirname, './StockLevels')),
    StockTakeTypes: seq.import(path.join(__dirname, './StockTakeTypes')),
    StockUnitConversions: seq.import(path.join(__dirname, './StockUnitConversions')),
    StockUnits: seq.import(path.join(__dirname, './StockUnits')),
    SyncClashItems: seq.import(path.join(__dirname, './SyncClashItems')),
    SyncItems: seq.import(path.join(__dirname, './SyncItems')),
    TabOrderStatus: seq.import(path.join(__dirname, './TabOrderStatus')),
    TabPaymentGroups: seq.import(path.join(__dirname, './TabPaymentGroups')),
    TableAreas: seq.import(path.join(__dirname, './TableAreas')),
    TableHelp: seq.import(path.join(__dirname, './TableHelp')),
    Tables: seq.import(path.join(__dirname, './Tables')),
    Tabs: seq.import(path.join(__dirname, './Tabs')),
    Tag: seq.import(path.join(__dirname, './Tag')),
    TagAttributes: seq.import(path.join(__dirname, './TagAttributes')),
    TaxGroups: seq.import(path.join(__dirname, './TaxGroups')),
    TaxLines: seq.import(path.join(__dirname, './TaxLines')),
    TaxRegimes: seq.import(path.join(__dirname, './TaxRegimes')),
    TaxTypeLinks: seq.import(path.join(__dirname, './TaxTypeLinks')),
    TaxTypes: seq.import(path.join(__dirname, './TaxTypes')),
    TenantAccountSystem: seq.import(path.join(__dirname, './TenantAccountSystem')),
    TenantAndMenuPublishVersions: seq.import(path.join(__dirname, './TenantAndMenuPublishVersions')),
    TenantDiffs: seq.import(path.join(__dirname, './TenantDiffs')),
    TenantLanguages: seq.import(path.join(__dirname, './TenantLanguages')),
    TenantLocationSettings: seq.import(path.join(__dirname, './TenantLocationSettings')),
    TenantLocations: seq.import(path.join(__dirname, './TenantLocations')),
    TenantLoyaltyPrograms: seq.import(path.join(__dirname, './TenantLoyaltyPrograms')),
    TenantOperatingTimes: seq.import(path.join(__dirname, './TenantOperatingTimes')),
    TenantPaymentType: seq.import(path.join(__dirname, './TenantPaymentType')),
    TenantPerpShifts: seq.import(path.join(__dirname, './TenantPerpShifts')),
    TenantPublishVersions: seq.import(path.join(__dirname, './TenantPublishVersions')),
    TenantServiceConfig: seq.import(path.join(__dirname, './TenantServiceConfig')),
    TenantServices: seq.import(path.join(__dirname, './TenantServices')),
    TenantSettings: seq.import(path.join(__dirname, './TenantSettings')),
    TenantSuppliers: seq.import(path.join(__dirname, './TenantSuppliers')),
    TenantTOPIAddresses: seq.import(path.join(__dirname, './TenantTOPIAddresses')),
    TenantTagLinks: seq.import(path.join(__dirname, './TenantTagLinks')),
    TenantTags: seq.import(path.join(__dirname, './TenantTags')),
    TenantTargets: seq.import(path.join(__dirname, './TenantTargets')),
    TenantTemplates: seq.import(path.join(__dirname, './TenantTemplates')),
    Tenants: seq.import(path.join(__dirname, './Tenants')),
    ToGoFeaturedItems: seq.import(path.join(__dirname, './ToGoFeaturedItems')),
    TogoUserFeedback: seq.import(path.join(__dirname, './TogoUserFeedback')),
    TopiLog: seq.import(path.join(__dirname, './TopiLog')),
    Translators: seq.import(path.join(__dirname, './Translators')),
    User: seq.import(path.join(__dirname, './User')),
    TransportType: seq.import(path.join(__dirname, './TransportType')),
    UserDevices: seq.import(path.join(__dirname, './UserDevices')),
    UserPaymentMethods: seq.import(path.join(__dirname, './UserPaymentMethods')),
    UserLoyaltyPrograms: seq.import(path.join(__dirname, './UserLoyaltyPrograms')),
    UserPreferences: seq.import(path.join(__dirname, './UserPreferences')),
    WaitronDevice: seq.import(path.join(__dirname, './WaitronDevice')),
    WaitronRatings: seq.import(path.join(__dirname, './WaitronRatings')),
    WaitronService: seq.import(path.join(__dirname, './WaitronService')),
    WaitronShiftMonies: seq.import(path.join(__dirname, './WaitronShiftMonies')),
    WaitronTables: seq.import(path.join(__dirname, './WaitronTables')),
    WebSession: seq.import(path.join(__dirname, './WebSession')),
    WebsocketEvent: seq.import(path.join(__dirname, './WebsocketEvent')),
    WebsocketEvents: seq.import(path.join(__dirname, './WebsocketEvents')),
    auth_consumer: seq.import(path.join(__dirname, './auth_consumer')),
    auth_consumer_nonce: seq.import(path.join(__dirname, './auth_consumer_nonce')),
    auth_token: seq.import(path.join(__dirname, './auth_token')),
    faq: seq.import(path.join(__dirname, './faq')),
    auth_token_type: seq.import(path.join(__dirname, './auth_token_type')),
    faq_list: seq.import(path.join(__dirname, './faq_list')),
    flyway_schema_history: seq.import(path.join(__dirname, './flyway_schema_history')),
    migration: seq.import(path.join(__dirname, './migration')),
    play_evolutions: seq.import(path.join(__dirname, './play_evolutions')),
    suppliers: seq.import(path.join(__dirname, './suppliers')),
    vPrintDevices: seq.import(path.join(__dirname, './vPrintDevices')),
    vSessionActiveTabs: seq.import(path.join(__dirname, './vSessionActiveTabs')),
    vTabs: seq.import(path.join(__dirname, './vTabs')),
    v_today: seq.import(path.join(__dirname, './v_today')),
  };
  return tables;
};
