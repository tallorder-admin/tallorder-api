/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {auth_consumer_nonceInstance, auth_consumer_nonceAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<auth_consumer_nonceInstance, auth_consumer_nonceAttribute>('auth_consumer_nonce', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    consumer_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    timestamp: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    nonce: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    tableName: 'auth_consumer_nonce'
  });
};
