/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ApiAccessKeysInstance, ApiAccessKeysAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ApiAccessKeysInstance, ApiAccessKeysAttribute>('ApiAccessKeys', {
    api_key: {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true
    },
    secret: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    token: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    tenant_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    user_id: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    device_register_id: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    admin_id: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    staff_id: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    expiry_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: '2099-01-01 00:00:00'
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'ApiAccessKeys'
  });
};
