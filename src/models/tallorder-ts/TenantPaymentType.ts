/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantPaymentTypeInstance, TenantPaymentTypeAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantPaymentTypeInstance, TenantPaymentTypeAttribute>('TenantPaymentType', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    PaymentTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    DefaultPayment: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    DisplayName: {
      type: DataTypes.STRING(75),
      allowNull: false,
      defaultValue: ''
    },
    PaymentUser: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    PaymentPass: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    PaymentNotifyKey: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    DecryptionKey: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LevyType: {
      type: DataTypes.ENUM('none','value','percentage'),
      allowNull: false,
      defaultValue: 'none'
    },
    LevyValue: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00'
    },
    Seq: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    QRCode: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ChangeGiven: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    OpenCashDrawer: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    HostAllocate: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    AlterTypeAllowed: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    HostBankID: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ReferenceRequired: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    UserEnabled: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    EnabledPOS: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    EnabledToGo: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    EnabledApps: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    EnabledTA: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TenantPaymentType'
  });
};
