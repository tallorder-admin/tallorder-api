/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantServicesInstance, TenantServicesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantServicesInstance, TenantServicesAttribute>('TenantServices', {
    TenantId: {
      type: DataTypes.INTEGER(20),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    ServiceId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'ServiceConfig',
        key: 'ID'
      }
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TenantServices'
  });
};
