/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PublishedSettingsInstance, PublishedSettingsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PublishedSettingsInstance, PublishedSettingsAttribute>('PublishedSettings', {
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    SourceID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Type: {
      type: DataTypes.STRING(31),
      allowNull: false,
      defaultValue: 'MENU',
      primaryKey: true
    },
    Version: {
      type: DataTypes.STRING(31),
      allowNull: false,
      primaryKey: true
    },
    FileKey: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    BucketName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    S3Url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PatchFileKey: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PatchS3Url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'PublishedSettings'
  });
};
