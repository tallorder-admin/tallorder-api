/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TabsInstance, TabsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TabsInstance, TabsAttribute>('Tabs', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    StaffShift: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Origin: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    WaitronServiceID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    CustomerID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    LoyaltyProgRef: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    TableID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TabName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Party: {
      type: DataTypes.CHAR(64),
      allowNull: true
    },
    GuestCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    OpenTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CloseTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Voided: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    NetOrderTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    Gratuity: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    ChangeGiven: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    TaxTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    InvoiceTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    CostTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    InvoiceNo: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HostProcessed: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    HostTabID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HostWaiterID: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    HostWaiterName: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    Note: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SubscribeType: {
      type: DataTypes.CHAR(3),
      allowNull: true
    },
    SubscribeID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    OrderStatus: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '1'
    },
    IsFutureOrder: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    SaleType: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: 'Sale'
    },
    UserCode: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ArchivedDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    percentage_discount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0'
    },
    reportDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    CashOnCollection: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    PrepTime: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    EstimatedCompletionTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    NumPrepTimeUpdates: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    PaymentConfirmationReceived: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    ServingTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    TransportTypeID: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'TransportType',
        key: 'ID'
      }
    }
  }, {
    tableName: 'Tabs'
  });
};
