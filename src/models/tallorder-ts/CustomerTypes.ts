/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {CustomerTypesInstance, CustomerTypesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<CustomerTypesInstance, CustomerTypesAttribute>('CustomerTypes', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TypeName: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    HostID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'CustomerTypes'
  });
};
