/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StockItemsInstance, StockItemsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StockItemsInstance, StockItemsAttribute>('StockItems', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    MenuItem: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'MenuItems',
        key: 'ID'
      }
    },
    Name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Type: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'AppEnums',
        key: 'ID'
      }
    },
    LastCostPerBaseUnit: {
      type: "DOUBLE",
      allowNull: true,
      defaultValue: '0'
    },
    AverageCostPerBaseUnit: {
      type: "DOUBLE",
      allowNull: true,
      defaultValue: '0'
    },
    StockTakePeriod: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IsRandomStockTake: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    AllowNegativeStockLevel: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '1'
    },
    ReorderLevel: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    AlertLevel: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    RunningStockValue: {
      type: "DOUBLE",
      allowNull: true,
      defaultValue: '0'
    },
    RunningStockQuantity: {
      type: "DOUBLE",
      allowNull: true,
      defaultValue: '0'
    },
    BaseUnit: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'StockItems'
  });
};
