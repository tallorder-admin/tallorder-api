/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {MenusInstance, MenusAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<MenusInstance, MenusAttribute>('Menus', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Name: {
      type: DataTypes.CHAR(40),
      allowNull: false
    },
    S3URL: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Active: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    BusyPublishing: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    BusyTranslating: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastMenuChange: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LastMenuPublish: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LatestVersion: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PublishError: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'Menus'
  });
};
