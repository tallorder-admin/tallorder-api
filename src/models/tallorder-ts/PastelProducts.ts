/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PastelProductsInstance, PastelProductsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PastelProductsInstance, PastelProductsAttribute>('PastelProducts', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'MenuItems',
        key: 'ID'
      }
    },
    StockCode: {
      type: DataTypes.STRING(15),
      allowNull: false
    },
    LineType: {
      type: DataTypes.CHAR(1),
      allowNull: false
    },
    Store: {
      type: DataTypes.STRING(3),
      allowNull: true
    }
  }, {
    tableName: 'PastelProducts'
  });
};
