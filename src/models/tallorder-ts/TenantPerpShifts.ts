/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantPerpShiftsInstance, TenantPerpShiftsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantPerpShiftsInstance, TenantPerpShiftsAttribute>('TenantPerpShifts', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    eDayNameID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ShiftName: {
      type: DataTypes.CHAR(20),
      allowNull: false
    },
    StartTime: {
      type: DataTypes.TIME,
      allowNull: false
    },
    EndTime: {
      type: DataTypes.TIME,
      allowNull: false
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TenantPerpShifts'
  });
};
