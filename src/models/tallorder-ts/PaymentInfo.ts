/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PaymentInfoInstance, PaymentInfoAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PaymentInfoInstance, PaymentInfoAttribute>('PaymentInfo', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    HostPaymentID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    StaffShift: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    PaymentTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    PaymentRef: {
      type: DataTypes.CHAR(64),
      allowNull: true
    },
    TxID: {
      type: DataTypes.STRING(75),
      allowNull: true
    },
    TxNotes: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Amount: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    Gratuity: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    ChangeGiven: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    Balance: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    AltCurrencyID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    AltAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    TabID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ArchivedDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    percentage_discount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0'
    }
  }, {
    tableName: 'PaymentInfo'
  });
};
