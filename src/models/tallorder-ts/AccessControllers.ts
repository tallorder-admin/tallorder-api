/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {AccessControllersInstance, AccessControllersAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<AccessControllersInstance, AccessControllersAttribute>('AccessControllers', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Controller: {
      type: DataTypes.STRING(75),
      allowNull: false,
      unique: true
    },
    ControllerID: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    Name: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    TenantRequired: {
      type: DataTypes.INTEGER(1).UNSIGNED,
      allowNull: false,
      defaultValue: '0'
    },
    AdminOnly: {
      type: DataTypes.INTEGER(1).UNSIGNED,
      allowNull: false,
      defaultValue: '0'
    },
    Seq: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: false,
      defaultValue: '1'
    }
  }, {
    tableName: 'AccessControllers'
  });
};
