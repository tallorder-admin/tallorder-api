/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ExRatesInstance, ExRatesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ExRatesInstance, ExRatesAttribute>('ExRates', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Symbol: {
      type: DataTypes.CHAR(3),
      allowNull: true
    },
    SourceCurrency: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TargetCurrency: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ExRate: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'ExRates'
  });
};
