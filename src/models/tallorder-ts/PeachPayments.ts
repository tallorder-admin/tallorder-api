/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PeachPaymentsInstance, PeachPaymentsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PeachPaymentsInstance, PeachPaymentsAttribute>('PeachPayments', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    PeachTransactionID: {
      type: DataTypes.STRING(127),
      allowNull: false
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    TabID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tabs',
        key: 'ID'
      }
    },
    PaymentStatus: {
      type: DataTypes.STRING(127),
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'PeachPayments'
  });
};
