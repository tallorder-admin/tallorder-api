/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {MasterDataTypeServicesInstance, MasterDataTypeServicesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<MasterDataTypeServicesInstance, MasterDataTypeServicesAttribute>('MasterDataTypeServices', {
    MasterDataTypeID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'MasterDataTypes',
        key: 'ID'
      }
    },
    ServiceConfigID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'ServiceConfig',
        key: 'ID'
      }
    },
    EventType: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    Service: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    Description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'MasterDataTypeServices'
  });
};
