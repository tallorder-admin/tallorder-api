/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PortionsInstance, PortionsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PortionsInstance, PortionsAttribute>('Portions', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    PortionGroupID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    PortionName: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    PortionNameStringID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PortionPercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    AddOnPortionPercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    AddOnPricePercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    PricePercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    Seq: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'Portions'
  });
};
