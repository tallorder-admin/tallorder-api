/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {WebsocketEventInstance, WebsocketEventAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<WebsocketEventInstance, WebsocketEventAttribute>('WebsocketEvent', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    EventSource: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EventDestination: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DeviceUid: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    EventID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Event: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Acknowledged: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    AcknowledgedTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    RetryCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    UpdateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'WebsocketEvent'
  });
};
