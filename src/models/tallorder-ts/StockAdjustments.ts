/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StockAdjustmentsInstance, StockAdjustmentsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StockAdjustmentsInstance, StockAdjustmentsAttribute>('StockAdjustments', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    StockItem: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'StockItems',
        key: 'ID'
      }
    },
    Type: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'AppEnums',
        key: 'ID'
      }
    },
    Delta: {
      type: "DOUBLE",
      allowNull: true
    },
    PurchaseOrder: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    Tab: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    QuantityBefore: {
      type: "DOUBLE",
      allowNull: true
    },
    QuantityAfter: {
      type: "DOUBLE",
      allowNull: true
    },
    User: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'StockAdjustments'
  });
};
