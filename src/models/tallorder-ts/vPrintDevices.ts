/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {vPrintDevicesInstance, vPrintDevicesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<vPrintDevicesInstance, vPrintDevicesAttribute>('vPrintDevices', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    StationName: {
      type: DataTypes.CHAR(30),
      allowNull: true
    },
    DeviceName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'vPrintDevices'
  });
};
