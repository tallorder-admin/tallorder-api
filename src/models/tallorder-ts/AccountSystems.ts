/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {AccountSystemsInstance, AccountSystemsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<AccountSystemsInstance, AccountSystemsAttribute>('AccountSystems', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    SystemName: {
      type: DataTypes.STRING(55),
      allowNull: false
    },
    SourceName: {
      type: DataTypes.STRING(55),
      allowNull: false
    },
    ApiUrl: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PromptID: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    PromptSecret: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'AccountSystems'
  });
};
