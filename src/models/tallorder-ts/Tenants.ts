/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantsInstance, TenantsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantsInstance, TenantsAttribute>('Tenants', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    TenantName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    TenantTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    BusinessTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '50'
    },
    ProdTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Address: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'User',
        key: 'ID'
      }
    },
    CountryID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    CurrencyID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Base64Logo: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    LangID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    MenuTenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ManagerTenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    POSVendorID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    UseSupersetMenu: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    RefFranchiseeID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TenantAuthCode: {
      type: DataTypes.CHAR(16),
      allowNull: true
    },
    TimeZone: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    LocaleID: {
      type: DataTypes.CHAR(12),
      allowNull: true,
      defaultValue: 'en_ZA'
    },
    LocLong: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    LocLat: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.0000'
    },
    ShowTimes: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    ShowPrepTimes: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ShowDeliveryTimes: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    ProcessItemRatings: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ProcessRestRatings: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ShowItemRatings: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ShowItemRatingComments: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ShowRestRatings: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ShowRestRatingComments: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ShowThumbs: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ShowCurrencySymbol: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AllowLocalPrices: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    AllowSS: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    PublishSS: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AllowVoid: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ApplyVoidPINReq: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ApplyDiscountPINReq: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    StaffPinSignIn: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    ShowShiftStats: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    OrderClosingMethod: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '2'
    },
    ShowOrdConfirm: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ShowQuickCashBtn: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    ShowTotalsInTab: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    MarkOrdDeliv: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    UseTallOrderBackupServer: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    DebugMode: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ResumeLastTab: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    PrintServerURL: {
      type: DataTypes.CHAR(128),
      allowNull: true
    },
    POSLicense: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    POSDeviceCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    POSPostingURL: {
      type: DataTypes.CHAR(60),
      allowNull: true
    },
    POSIntegratorURL: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POSIntegratorParameters: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    POSImportMenu: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '0'
    },
    POSInactiveTimeout: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    POSTableNoReq: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    AddItemPromptNewOrder: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    CompletePaymentPromptPlaceOrder: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    GuestCountReq: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    UseGratuityMechanism: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ContactWork: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    ReceiptHeader: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ReceiptFooter: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    MPOSPaymentConfig: {
      type: DataTypes.STRING(512),
      allowNull: true
    },
    GTPaymentConfig: {
      type: DataTypes.STRING(512),
      allowNull: true
    },
    ProcessGuaranteeTab: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    FromEmailAddress: {
      type: DataTypes.CHAR(60),
      allowNull: true
    },
    ServerURL: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: 'https://www.tallorder.mobi/tallorder/'
    },
    MoneyFormat: {
      type: DataTypes.CHAR(32),
      allowNull: true,
      defaultValue: '%1.2f'
    },
    DateFormat: {
      type: DataTypes.CHAR(32),
      allowNull: true,
      defaultValue: 'yyyy-MM-DD HH:MM'
    },
    FirstDayOfMonth: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    FirstDayOfWeek: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    DefaultWaitronCommPercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ExcludeTaxesFromCommCalc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    UseWaitronScheduling: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ThemeBUrl: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    },
    ThemeBTile: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ThemeBColor: {
      type: DataTypes.CHAR(10),
      allowNull: true,
      defaultValue: '59B211'
    },
    ThemeFColor: {
      type: DataTypes.CHAR(10),
      allowNull: true,
      defaultValue: 'FFFFFF'
    },
    HeadBColor: {
      type: DataTypes.CHAR(10),
      allowNull: true,
      defaultValue: 'A9C089'
    },
    HeadFColor: {
      type: DataTypes.CHAR(10),
      allowNull: true,
      defaultValue: '000000'
    },
    ItemBColor: {
      type: DataTypes.CHAR(10),
      allowNull: true,
      defaultValue: 'E5E5D3'
    },
    ItemFColor: {
      type: DataTypes.CHAR(10),
      allowNull: true,
      defaultValue: '000000'
    },
    WaiterTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true,
      defaultValue: 'Waiter'
    },
    MenuTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true,
      defaultValue: 'Menu'
    },
    MenusTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true,
      defaultValue: 'Menus'
    },
    TabTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true,
      defaultValue: 'Tab'
    },
    TabsTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true,
      defaultValue: 'Tabs'
    },
    GratuityTerm: {
      type: DataTypes.STRING(75),
      allowNull: false,
      defaultValue: 'Tip'
    },
    GratuityPercentages: {
      type: DataTypes.STRING(75),
      allowNull: false,
      defaultValue: '5,10,15,20'
    },
    UserDefLabel1: {
      type: DataTypes.CHAR(16),
      allowNull: true,
      defaultValue: 'Message 1'
    },
    UserDefLabel2: {
      type: DataTypes.CHAR(16),
      allowNull: true,
      defaultValue: 'Message 2'
    },
    ListOrGridView: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ReportCatWidth: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    UseGuestNumbering: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    FreeFormTblNmbrs: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    AllowDirectCloudSignIn: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AllowCloseTabAmountChange: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AllowPaymentDelete: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    RestrictTableNumXWaiters: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ShowiAppNotes: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    TwitterUrl: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FacebookUrl: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    InvoicePrefix: {
      type: DataTypes.STRING(6),
      allowNull: false
    },
    InvNoMinLength: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
      defaultValue: '6'
    },
    StorePayments: {
      type: DataTypes.ENUM('live','cashup'),
      allowNull: false,
      defaultValue: 'live'
    },
    TGMenuAvail: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ToGoFeaturedItemId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TGShowThumbs: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    TGWaiterID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    TGMUDelivPcnt: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    TGWSUris: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    TGSubdomain: {
      type: DataTypes.CHAR(40),
      allowNull: true,
      unique: true
    },
    TGFailoverEmailAddr: {
      type: DataTypes.CHAR(60),
      allowNull: true
    },
    TGLandingContent: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    TGTsAndCs: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    TGAbout: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    TGRestaurantTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true
    },
    TGRestaurantsTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true
    },
    TGOrderTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true
    },
    TGTabTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true
    },
    TGReqPreAuthSSOrders: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    TGAllowSSPayments: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    TGStartTableNumber: {
      type: DataTypes.INTEGER(3),
      allowNull: true
    },
    TGMaxTableNumber: {
      type: DataTypes.INTEGER(3),
      allowNull: true
    },
    TGStartTimeMon: {
      type: DataTypes.TIME,
      allowNull: true
    },
    TGEndTimeMon: {
      type: DataTypes.TIME,
      allowNull: true
    },
    TGStartTimeTue: {
      type: DataTypes.TIME,
      allowNull: true
    },
    TGEndTimeTue: {
      type: DataTypes.TIME,
      allowNull: true
    },
    TGStartTimeWed: {
      type: DataTypes.TIME,
      allowNull: true
    },
    TGEndTimeWed: {
      type: DataTypes.TIME,
      allowNull: true
    },
    TGStartTimeThu: {
      type: DataTypes.TIME,
      allowNull: true
    },
    TGEndTimeThu: {
      type: DataTypes.TIME,
      allowNull: true
    },
    TGStartTimeFri: {
      type: DataTypes.TIME,
      allowNull: true
    },
    TGEndTimeFri: {
      type: DataTypes.TIME,
      allowNull: true
    },
    TGStartTimeSat: {
      type: DataTypes.TIME,
      allowNull: true
    },
    TGEndTimeSat: {
      type: DataTypes.TIME,
      allowNull: true
    },
    TGStartTimeSun: {
      type: DataTypes.TIME,
      allowNull: true
    },
    TGEndTimeSun: {
      type: DataTypes.TIME,
      allowNull: true
    },
    LastActiveMenuPub: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: '2013-11-01 00:00:00'
    },
    LastPOSSync: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DemoMode: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    TogoLogoURL: {
      type: DataTypes.STRING(512),
      allowNull: true
    },
    AllowPOSAutoUpdates: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '1'
    },
    AllowMasterStationOverride: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    }
  }, {
    tableName: 'Tenants'
  });
};
