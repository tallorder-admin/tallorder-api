/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {AddressTableInstance, AddressTableAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<AddressTableInstance, AddressTableAttribute>('AddressTable', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    OwnerType: {
      type: "SET('USER','TENANT','GLOBALSUPPLIERS','TENANTSUPPLIERS')",
      allowNull: true
    },
    OwnerID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    AddType: {
      type: DataTypes.CHAR(30),
      allowNull: true
    },
    AddLine1: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    AddLine2: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    AddLine3: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    AddCity: {
      type: DataTypes.CHAR(100),
      allowNull: true
    },
    AddZip: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    AddCountry: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PosLat: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    PosLong: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'AddressTable'
  });
};
