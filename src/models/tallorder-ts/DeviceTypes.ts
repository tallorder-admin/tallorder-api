/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {DeviceTypesInstance, DeviceTypesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<DeviceTypesInstance, DeviceTypesAttribute>('DeviceTypes', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    DeviceEnum: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    DeviceInfo: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DeviceURL: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'DeviceTypes'
  });
};
