/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StockItemSuppliersInstance, StockItemSuppliersAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StockItemSuppliersInstance, StockItemSuppliersAttribute>('StockItemSuppliers', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    StockItem: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'StockItems',
        key: 'ID'
      }
    },
    Supplier: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'TenantSuppliers',
        key: 'ID'
      }
    },
    Description: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Rank: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    StockUnitConversionID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    BarCode: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PLU: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PreferedSupplier: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    EconomicOrderQuantity: {
      type: "DOUBLE",
      allowNull: true
    },
    EconomicOrderStockItemUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'StockItemUnits',
        key: 'ID'
      }
    },
    StockOrderUnit: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ConversionFactor: {
      type: "DOUBLE",
      allowNull: true
    },
    LastCostPerBaseUnit: {
      type: "DOUBLE",
      allowNull: true
    },
    AverageReplenishmentCycleDays: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'StockItemSuppliers'
  });
};
