/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantServiceConfigInstance, TenantServiceConfigAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantServiceConfigInstance, TenantServiceConfigAttribute>('TenantServiceConfig', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    Name: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    Type: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IAMKey: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    IAMSecret: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    AWSQueueURL: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    QueueARN: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    Region: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    MessageCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    Interval: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    FetchDelaySeconds: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    WorkerCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    MaxWorkerCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '10'
    },
    Active: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    RetryThreshold: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '-1'
    },
    QueueMonitorThreshold: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '200'
    },
    LogEvent: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    Synchronous: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    }
  }, {
    tableName: 'TenantServiceConfig'
  });
};
