/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantPublishVersionsInstance, TenantPublishVersionsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantPublishVersionsInstance, TenantPublishVersionsAttribute>('TenantPublishVersions', {
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    Version: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true
    },
    TenantFileKey: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    PatchFileKey: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BucketName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    TenantObject: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    PatchObject: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    PatchS3Url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TenantS3Url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TenantPublishVersions'
  });
};
