/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PastelTenantConfigInstance, PastelTenantConfigAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PastelTenantConfigInstance, PastelTenantConfigAttribute>('PastelTenantConfig', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    FinancialYearStart: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    ScheduledCSVUpload: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    EmailAddress: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    SendEmail: {
      type: DataTypes.INTEGER(1).UNSIGNED,
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'PastelTenantConfig'
  });
};
