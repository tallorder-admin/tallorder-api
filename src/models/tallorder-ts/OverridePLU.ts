/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {OverridePLUInstance, OverridePLUAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<OverridePLUInstance, OverridePLUAttribute>('OverridePLU', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    MenuID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    eModifierTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ModifierGroupID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ModifierID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    HostOverridePLU: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: ''
    },
    OverrideItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OverridePrice: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'OverridePLU'
  });
};
