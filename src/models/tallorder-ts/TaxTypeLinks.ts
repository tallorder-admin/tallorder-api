/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TaxTypeLinksInstance, TaxTypeLinksAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TaxTypeLinksInstance, TaxTypeLinksAttribute>('TaxTypeLinks', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TaxTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'TaxTypes',
        key: 'ID'
      }
    },
    TaxGroupID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'TaxGroups',
        key: 'ID'
      }
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'TaxTypeLinks'
  });
};
