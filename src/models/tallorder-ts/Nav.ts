/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {NavInstance, NavAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<NavInstance, NavAttribute>('Nav', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    ParentID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Title: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    ControllerID: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    Controller: {
      type: DataTypes.STRING(75),
      allowNull: false
    },
    Action: {
      type: DataTypes.STRING(55),
      allowNull: false
    },
    OverrideTenantRequired: {
      type: DataTypes.INTEGER(1).UNSIGNED,
      allowNull: true
    },
    IconClass: {
      type: DataTypes.STRING(75),
      allowNull: false,
      defaultValue: 'gi gi-more_items'
    },
    Active: {
      type: DataTypes.INTEGER(1).UNSIGNED,
      allowNull: false,
      defaultValue: '1'
    },
    Seq: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: false,
      defaultValue: '1'
    }
  }, {
    tableName: 'Nav'
  });
};
