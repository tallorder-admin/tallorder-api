/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ResourceSyncLogInstance, ResourceSyncLogAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ResourceSyncLogInstance, ResourceSyncLogAttribute>('ResourceSyncLog', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Host: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Method: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Endpoint: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Headers: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Body: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Acknowledged: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    AcknowledgedTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ErrorResponse: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ErrorMessage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    RetryCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    UpdateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'ResourceSyncLog'
  });
};
