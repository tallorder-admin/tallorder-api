/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StockLevelsInstance, StockLevelsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StockLevelsInstance, StockLevelsAttribute>('StockLevels', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'MenuItems',
        key: 'ID'
      }
    },
    LastUpdateSource: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    Available: {
      type: "DOUBLE",
      allowNull: true
    },
    BackOrder: {
      type: "DOUBLE",
      allowNull: true
    },
    Reserved: {
      type: "DOUBLE",
      allowNull: true
    },
    RunningTotal: {
      type: "DOUBLE",
      allowNull: true
    }
  }, {
    tableName: 'StockLevels'
  });
};
