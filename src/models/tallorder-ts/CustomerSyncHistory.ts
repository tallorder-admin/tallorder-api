/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {CustomerSyncHistoryInstance, CustomerSyncHistoryAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<CustomerSyncHistoryInstance, CustomerSyncHistoryAttribute>('CustomerSyncHistory', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    CustomerID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    AccountingCustomerID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    LocalItemData: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    AccountingItemData: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    SyncDateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'CustomerSyncHistory'
  });
};
