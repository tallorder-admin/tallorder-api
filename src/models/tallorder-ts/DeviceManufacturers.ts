/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {DeviceManufacturersInstance, DeviceManufacturersAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<DeviceManufacturersInstance, DeviceManufacturersAttribute>('DeviceManufacturers', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Manufacturer: {
      type: DataTypes.CHAR(60),
      allowNull: false,
      unique: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(4),
      allowNull: true,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'DeviceManufacturers'
  });
};
