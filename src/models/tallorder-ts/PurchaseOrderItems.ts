/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PurchaseOrderItemsInstance, PurchaseOrderItemsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PurchaseOrderItemsInstance, PurchaseOrderItemsAttribute>('PurchaseOrderItems', {
    BaseUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    BaseUnitDescription: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    BaseUnitQuantity: {
      type: "DOUBLE",
      allowNull: true
    },
    BaseUnitCost: {
      type: "DOUBLE",
      allowNull: true
    },
    PurchaseOrderID: {
      type: DataTypes.STRING(255),
      allowNull: true,
      references: {
        model: 'PurchaseOrders',
        key: 'ID'
      }
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'MenuItems',
        key: 'ID'
      }
    },
    ItemDescription: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    SupplierID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SupplierName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    SelectedUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    SelectedUnitDescription: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    SelectedUnitQuantity: {
      type: "DOUBLE",
      allowNull: true
    },
    Total: {
      type: "DOUBLE",
      allowNull: true
    },
    ProportionalDeliveryCost: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ConversionFactor: {
      type: "DOUBLE",
      allowNull: true
    }
  }, {
    tableName: 'PurchaseOrderItems'
  });
};
