/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StationDevicesInstance, StationDevicesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StationDevicesInstance, StationDevicesAttribute>('StationDevices', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    StationID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '1'
    },
    DeviceTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    DeviceName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LocalPrinterName: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: ''
    },
    LinkedDeviceUID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MaxChar: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    FontBMaxChar: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OrderLineSpacing: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TOPRAddress: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: ''
    },
    ComPort: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: ''
    },
    BaudRate: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: ''
    },
    ReprintCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    DefaultPrinter: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    eStationDeviceID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    StationURI: {
      type: DataTypes.CHAR(255),
      allowNull: true
    },
    PrintPrefix: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PrintSuffix: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PrintReceiptSuffix: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CashDrwSq: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'StationDevices'
  });
};
