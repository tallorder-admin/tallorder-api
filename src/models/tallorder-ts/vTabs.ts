/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {vTabsInstance, vTabsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<vTabsInstance, vTabsAttribute>('vTabs', {
    TabID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    TenantName: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    WaitronName: {
      type: DataTypes.STRING(81),
      allowNull: true
    },
    TableID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TabName: {
      type: DataTypes.CHAR(50),
      allowNull: true
    },
    Party: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ''
    },
    GuestCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OpenDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    OpenDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    OpenTime: {
      type: DataTypes.TIME,
      allowNull: true
    },
    OpenHour: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    CloseDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CloseDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    CloseTime: {
      type: DataTypes.TIME,
      allowNull: true
    },
    CustomerID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    NetOrderTotal: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00000'
    },
    TaxTotal: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00000'
    },
    Gratuity: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00'
    },
    InvoiceTotal: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00'
    },
    InvoiceNo: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    }
  }, {
    tableName: 'vTabs'
  });
};
