/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {AppEnumsInstance, AppEnumsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<AppEnumsInstance, AppEnumsAttribute>('AppEnums', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    eGroup: {
      type: DataTypes.CHAR(15),
      allowNull: false
    },
    eValue: {
      type: DataTypes.CHAR(9),
      allowNull: false,
      defaultValue: '',
      unique: true
    },
    eText: {
      type: DataTypes.CHAR(30),
      allowNull: false
    },
    Seq: {
      type: DataTypes.INTEGER(2),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'AppEnums'
  });
};
