// tslint:disable
import * as Sequelize from 'sequelize';


// table: AccessPermissions
export interface AccessPermissionsAttribute {
  ID:number;
  Controller:string;
  Action:string;
  eType?:any;
  eTypeID:number;
  Allowed:number;
}
export interface AccessPermissionsInstance extends Sequelize.Instance<AccessPermissionsAttribute>, AccessPermissionsAttribute { }
export interface AccessPermissionsModel extends Sequelize.Model<AccessPermissionsInstance, AccessPermissionsAttribute> { }

// table: Accounts
export interface AccountsAttribute {
  ID:number;
  CustomerID?:number;
  TotalOutstanding?:number;
  Current?:number;
  Days30?:number;
  Days60?:number;
  Days90?:number;
  TermDays?:number;
  LastPurchaseDate?:Date;
  LastPaymentDate?:Date;
  OverDue?:number;
  HoldFlag?:number;
}
export interface AccountsInstance extends Sequelize.Instance<AccountsAttribute>, AccountsAttribute { }
export interface AccountsModel extends Sequelize.Model<AccountsInstance, AccountsAttribute> { }

// table: AccessControllers
export interface AccessControllersAttribute {
  ID:number;
  Controller:string;
  ControllerID:string;
  Name:string;
  TenantRequired:number;
  AdminOnly:number;
  Seq:number;
}
export interface AccessControllersInstance extends Sequelize.Instance<AccessControllersAttribute>, AccessControllersAttribute { }
export interface AccessControllersModel extends Sequelize.Model<AccessControllersInstance, AccessControllersAttribute> { }

// table: AccountSystems
export interface AccountSystemsAttribute {
  ID:number;
  SystemName:string;
  SourceName:string;
  ApiUrl?:string;
  PromptID?:string;
  PromptSecret?:string;
  IsArchived:number;
}
export interface AccountSystemsInstance extends Sequelize.Instance<AccountSystemsAttribute>, AccountSystemsAttribute { }
export interface AccountSystemsModel extends Sequelize.Model<AccountSystemsInstance, AccountSystemsAttribute> { }

// table: AccessActions
export interface AccessActionsAttribute {
  ID:number;
  AccessControllerID:number;
  Action:string;
  ActionName:string;
  AllowManage:number;
  AllowedDefault:number;
  Seq:number;
}
export interface AccessActionsInstance extends Sequelize.Instance<AccessActionsAttribute>, AccessActionsAttribute { }
export interface AccessActionsModel extends Sequelize.Model<AccessActionsInstance, AccessActionsAttribute> { }

// table: AddOnEnumerationValues
export interface AddOnEnumerationValuesAttribute {
  ID:number;
  AddOnSlotTypeID?:number;
  AddOnEnumerationValue?:string;
  AddOnID?:number;
}
export interface AddOnEnumerationValuesInstance extends Sequelize.Instance<AddOnEnumerationValuesAttribute>, AddOnEnumerationValuesAttribute { }
export interface AddOnEnumerationValuesModel extends Sequelize.Model<AddOnEnumerationValuesInstance, AddOnEnumerationValuesAttribute> { }

// table: AddOnGroupNameSlotTypes
export interface AddOnGroupNameSlotTypesAttribute {
  ID:number;
  TenantID?:number;
  MenuID?:number;
  MenuItemID?:number;
  AddOnGroupID?:number;
  AddOnSlotType?:string;
  BotID?:number;
}
export interface AddOnGroupNameSlotTypesInstance extends Sequelize.Instance<AddOnGroupNameSlotTypesAttribute>, AddOnGroupNameSlotTypesAttribute { }
export interface AddOnGroupNameSlotTypesModel extends Sequelize.Model<AddOnGroupNameSlotTypesInstance, AddOnGroupNameSlotTypesAttribute> { }

// table: AddOnGroups
export interface AddOnGroupsAttribute {
  ID:number;
  TenantID:number;
  GroupName:string;
  GroupNameStringID?:number;
  ChargeFlag:number;
  NoCharge:number;
  RecommGroup:number;
  HoldGroup:number;
  OpenExpanded:number;
  MinSelect:number;
  MaxSelect:number;
  POSGrpID?:number;
  AvailableSS:number;
  AvailableTG:number;
  AvailableTA:number;
  IsArchived:number;
  LastUpdate:Date;
  SimpleAddOn:number;
  UseMainItemStation?:number;
}
export interface AddOnGroupsInstance extends Sequelize.Instance<AddOnGroupsAttribute>, AddOnGroupsAttribute { }
export interface AddOnGroupsModel extends Sequelize.Model<AddOnGroupsInstance, AddOnGroupsAttribute> { }

// table: AddOns
export interface AddOnsAttribute {
  ID:number;
  TenantID:number;
  AddOnGroupID:number;
  AddOnItemID:number;
  HoldPLU?:string;
  Seq?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface AddOnsInstance extends Sequelize.Instance<AddOnsAttribute>, AddOnsAttribute { }
export interface AddOnsModel extends Sequelize.Model<AddOnsInstance, AddOnsAttribute> { }

// table: AddressTable
export interface AddressTableAttribute {
  ID:number;
  OwnerType?:any;
  OwnerID?:number;
  AddType?:string;
  AddLine1?:string;
  AddLine2?:string;
  AddLine3?:string;
  AddCity?:string;
  AddZip?:string;
  AddCountry?:number;
  PosLat?:number;
  PosLong?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface AddressTableInstance extends Sequelize.Instance<AddressTableAttribute>, AddressTableAttribute { }
export interface AddressTableModel extends Sequelize.Model<AddressTableInstance, AddressTableAttribute> { }

// table: Admins
export interface AdminsAttribute {
  ID:number;
  Name:string;
  eMail:string;
  pWord:string;
  IsArchived:number;
  LastUpdate:Date;
}
export interface AdminsInstance extends Sequelize.Instance<AdminsAttribute>, AdminsAttribute { }
export interface AdminsModel extends Sequelize.Model<AdminsInstance, AdminsAttribute> { }

// table: ApiAccessKeys
export interface ApiAccessKeysAttribute {
  api_key:string;
  secret?:string;
  token:string;
  tenant_id?:number;
  user_id?:number;
  device_register_id?:number;
  admin_id?:number;
  staff_id?:number;
  expiry_time?:Date;
  create_time?:Date;
}
export interface ApiAccessKeysInstance extends Sequelize.Instance<ApiAccessKeysAttribute>, ApiAccessKeysAttribute { }
export interface ApiAccessKeysModel extends Sequelize.Model<ApiAccessKeysInstance, ApiAccessKeysAttribute> { }

// table: ApiKeys
export interface ApiKeysAttribute {
  ID:number;
  ApiKey:string;
  Description:string;
}
export interface ApiKeysInstance extends Sequelize.Instance<ApiKeysAttribute>, ApiKeysAttribute { }
export interface ApiKeysModel extends Sequelize.Model<ApiKeysInstance, ApiKeysAttribute> { }

// table: AppEnums
export interface AppEnumsAttribute {
  ID:number;
  eGroup:string;
  eValue:string;
  eText:string;
  Seq?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface AppEnumsInstance extends Sequelize.Instance<AppEnumsAttribute>, AppEnumsAttribute { }
export interface AppEnumsModel extends Sequelize.Model<AppEnumsInstance, AppEnumsAttribute> { }

// table: AppFeatures
export interface AppFeaturesAttribute {
  FeatGroup:string;
  eFeat:string;
  Description:string;
  eInd:number;
  eFor:number;
  eFee:number;
  eBrand:number;
  eOwn:number;
  eMan:number;
  eParAdm:number;
  eWai:number;
  eTra:number;
  ePro:number;
  eStd:number;
  eTGPro:number;
  eTGStd:number;
  eMerc:number;
  eLite:number;
  eBr1:number;
  BitVal:number;
}
export interface AppFeaturesInstance extends Sequelize.Instance<AppFeaturesAttribute>, AppFeaturesAttribute { }
export interface AppFeaturesModel extends Sequelize.Model<AppFeaturesInstance, AppFeaturesAttribute> { }

// table: AppNutrition
export interface AppNutritionAttribute {
  ID:number;
  ParentID?:number;
  LangID:number;
  Name:string;
  IsArchived:number;
  LastUpdate:Date;
}
export interface AppNutritionInstance extends Sequelize.Instance<AppNutritionAttribute>, AppNutritionAttribute { }
export interface AppNutritionModel extends Sequelize.Model<AppNutritionInstance, AppNutritionAttribute> { }

// table: Attributes
export interface AttributesAttribute {
  ID:number;
  AttributeName:string;
  SetByDefault:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface AttributesInstance extends Sequelize.Instance<AttributesAttribute>, AttributesAttribute { }
export interface AttributesModel extends Sequelize.Model<AttributesInstance, AttributesAttribute> { }

// table: Audit
export interface AuditAttribute {
  ID:number;
  WhenStamp:Date;
  SourceID:number;
  AdminID:number;
  UserID:number;
  TenantID:number;
  Verb:string;
  Noun?:string;
  JsonBefore?:string;
  JsonAfter?:string;
}
export interface AuditInstance extends Sequelize.Instance<AuditAttribute>, AuditAttribute { }
export interface AuditModel extends Sequelize.Model<AuditInstance, AuditAttribute> { }

// table: AuditSources
export interface AuditSourcesAttribute {
  ID:number;
  Name:string;
}
export interface AuditSourcesInstance extends Sequelize.Instance<AuditSourcesAttribute>, AuditSourcesAttribute { }
export interface AuditSourcesModel extends Sequelize.Model<AuditSourcesInstance, AuditSourcesAttribute> { }

// table: BotConversationPrompts
export interface BotConversationPromptsAttribute {
  ID:number;
  TenantID?:number;
  ConversationPrompt?:string;
  IntentID?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface BotConversationPromptsInstance extends Sequelize.Instance<BotConversationPromptsAttribute>, BotConversationPromptsAttribute { }
export interface BotConversationPromptsModel extends Sequelize.Model<BotConversationPromptsInstance, BotConversationPromptsAttribute> { }

// table: BotLexTabUsers
export interface BotLexTabUsersAttribute {
  ID:number;
  MenuID?:number;
  TenantID?:number;
  TabID?:number;
  BotID?:number;
  UserID?:number;
  LexChannelUserID?:string;
  LexChannelType?:string;
  LexChannelName?:string;
  LexChannelID?:string;
  LexPageAccessToken?:string;
  LexTabSnapScanURL?:string;
  LexTabSnapScanURLLink?:string;
  LexTabZapperURL?:string;
  LexTabZapperURLLink?:string;
  LexTabSnapScanURLS3?:string;
  LexTabZapperURLS3?:string;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface BotLexTabUsersInstance extends Sequelize.Instance<BotLexTabUsersAttribute>, BotLexTabUsersAttribute { }
export interface BotLexTabUsersModel extends Sequelize.Model<BotLexTabUsersInstance, BotLexTabUsersAttribute> { }

// table: BotIntents
export interface BotIntentsAttribute {
  ID:number;
  IDUtteranceGroup:number;
  TenantID?:number;
  ConfirmationPrompt?:string;
  ConfirmationCancel?:string;
  SlotType?:string;
  SlotName?:string;
  LambdaFunction?:string;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface BotIntentsInstance extends Sequelize.Instance<BotIntentsAttribute>, BotIntentsAttribute { }
export interface BotIntentsModel extends Sequelize.Model<BotIntentsInstance, BotIntentsAttribute> { }

// table: BotUtteranceGroups
export interface BotUtteranceGroupsAttribute {
  ID:number;
  Intent?:string;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface BotUtteranceGroupsInstance extends Sequelize.Instance<BotUtteranceGroupsAttribute>, BotUtteranceGroupsAttribute { }
export interface BotUtteranceGroupsModel extends Sequelize.Model<BotUtteranceGroupsInstance, BotUtteranceGroupsAttribute> { }

// table: BotUtterances
export interface BotUtterancesAttribute {
  ID:number;
  TenantID?:number;
  IDUtteranceGroup:number;
  Utterance?:string;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface BotUtterancesInstance extends Sequelize.Instance<BotUtterancesAttribute>, BotUtterancesAttribute { }
export interface BotUtterancesModel extends Sequelize.Model<BotUtterancesInstance, BotUtterancesAttribute> { }

// table: Bots
export interface BotsAttribute {
  ID:number;
  BotName?:string;
  TenantID?:number;
  MenuID?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface BotsInstance extends Sequelize.Instance<BotsAttribute>, BotsAttribute { }
export interface BotsModel extends Sequelize.Model<BotsInstance, BotsAttribute> { }

// table: BrandCategories
export interface BrandCategoriesAttribute {
  ID:number;
  CountryOfOriginID?:number;
  Category:string;
  CategoryStringID?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface BrandCategoriesInstance extends Sequelize.Instance<BrandCategoriesAttribute>, BrandCategoriesAttribute { }
export interface BrandCategoriesModel extends Sequelize.Model<BrandCategoriesInstance, BrandCategoriesAttribute> { }

// table: BrandItemUnits
export interface BrandItemUnitsAttribute {
  ID:number;
  TenantID?:number;
  BrandItemID?:number;
  StockUnitID:number;
  Barcode?:string;
  LastUpdate:Date;
}
export interface BrandItemUnitsInstance extends Sequelize.Instance<BrandItemUnitsAttribute>, BrandItemUnitsAttribute { }
export interface BrandItemUnitsModel extends Sequelize.Model<BrandItemUnitsInstance, BrandItemUnitsAttribute> { }

// table: BrandItems
export interface BrandItemsAttribute {
  ID:number;
  TenantID:number;
  ShortName:string;
  BarCode?:string;
  PLU?:string;
  Rating?:number;
  CategoryID:number;
  HaveImages:number;
  ImageMediaID?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface BrandItemsInstance extends Sequelize.Instance<BrandItemsAttribute>, BrandItemsAttribute { }
export interface BrandItemsModel extends Sequelize.Model<BrandItemsInstance, BrandItemsAttribute> { }

// table: ClientNotifications
export interface ClientNotificationsAttribute {
  ID:number;
  Title:string;
  DescriptionShort:string;
  DescriptionFull:string;
  Type:string;
  VersionPos?:string;
  VersionTopi?:string;
  Read:number;
  ShowFrom?:Date;
  SNSSent:number;
  LastUpdate:Date;
}
export interface ClientNotificationsInstance extends Sequelize.Instance<ClientNotificationsAttribute>, ClientNotificationsAttribute { }
export interface ClientNotificationsModel extends Sequelize.Model<ClientNotificationsInstance, ClientNotificationsAttribute> { }

// table: Country
export interface CountryAttribute {
  ID:number;
  CountryCode:string;
  CountryName:string;
  UsageCount?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface CountryInstance extends Sequelize.Instance<CountryAttribute>, CountryAttribute { }
export interface CountryModel extends Sequelize.Model<CountryInstance, CountryAttribute> { }

// table: CreditAllocations
export interface CreditAllocationsAttribute {
  ID:number;
  TenantID?:number;
  AccountID?:number;
  InvoiceID?:string;
  PaymentID?:string;
  Amount?:number;
  CreateTime?:Date;
}
export interface CreditAllocationsInstance extends Sequelize.Instance<CreditAllocationsAttribute>, CreditAllocationsAttribute { }
export interface CreditAllocationsModel extends Sequelize.Model<CreditAllocationsInstance, CreditAllocationsAttribute> { }

// table: Currency
export interface CurrencyAttribute {
  ID:number;
  CurrencyCode:string;
  Symbol?:string;
  SymbolPos:string;
  Currency:string;
  UsageCount?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface CurrencyInstance extends Sequelize.Instance<CurrencyAttribute>, CurrencyAttribute { }
export interface CurrencyModel extends Sequelize.Model<CurrencyInstance, CurrencyAttribute> { }

// table: CustomerSessions
export interface CustomerSessionsAttribute {
  ID:number;
  TenantID:number;
  TabID:number;
  UserID?:number;
  UserDevice?:number;
  StartTime?:Date;
  EndTime?:Date;
  IsArchived:number;
  LastUpdate:Date;
}
export interface CustomerSessionsInstance extends Sequelize.Instance<CustomerSessionsAttribute>, CustomerSessionsAttribute { }
export interface CustomerSessionsModel extends Sequelize.Model<CustomerSessionsInstance, CustomerSessionsAttribute> { }

// table: CustomerSyncHistory
export interface CustomerSyncHistoryAttribute {
  ID:number;
  TenantID:number;
  CustomerID:number;
  AccountingCustomerID:string;
  LocalItemData?:string;
  AccountingItemData?:string;
  SyncDateTime?:Date;
}
export interface CustomerSyncHistoryInstance extends Sequelize.Instance<CustomerSyncHistoryAttribute>, CustomerSyncHistoryAttribute { }
export interface CustomerSyncHistoryModel extends Sequelize.Model<CustomerSyncHistoryInstance, CustomerSyncHistoryAttribute> { }

// table: CustomerTypes
export interface CustomerTypesAttribute {
  ID:number;
  TypeName:string;
  TenantID:number;
  HostID?:number;
}
export interface CustomerTypesInstance extends Sequelize.Instance<CustomerTypesAttribute>, CustomerTypesAttribute { }
export interface CustomerTypesModel extends Sequelize.Model<CustomerTypesInstance, CustomerTypesAttribute> { }

// table: Customers
export interface CustomersAttribute {
  ID:number;
  TenantID:number;
  UserID?:number;
  NFCTagUID?:string;
  CustomerTypeID?:number;
  OTP?:string;
  LastVisit?:Date;
  LastAmount?:number;
  MarketingPermissionFlag?:number;
  LoyaltyProgRef?:string;
  Note?:any;
  IsArchived:number;
  DateCreated:Date;
  LastUpdate:Date;
  CustomerSource?:string;
  CustomerSourceUserID?:string;
  TaxReference:string;
  SourceAvailCredit:number;
  CreditLimit?:number;
  ProfilePic?:string;
  remote_ref?:string;
}
export interface CustomersInstance extends Sequelize.Instance<CustomersAttribute>, CustomersAttribute { }
export interface CustomersModel extends Sequelize.Model<CustomersInstance, CustomersAttribute> { }

// table: DebtorTransactions
export interface DebtorTransactionsAttribute {
  ID:number;
  TenantID?:number;
  AccountID?:number;
  TransactionType?:string;
  Reference?:string;
  Amount?:number;
  Age?:number;
  AmountDue?:number;
  CreateTime?:Date;
}
export interface DebtorTransactionsInstance extends Sequelize.Instance<DebtorTransactionsAttribute>, DebtorTransactionsAttribute { }
export interface DebtorTransactionsModel extends Sequelize.Model<DebtorTransactionsInstance, DebtorTransactionsAttribute> { }

// table: Descriptions
export interface DescriptionsAttribute {
  ID:number;
  TenantID:number;
  MenuItemID?:number;
  BrandItemID?:number;
  LangID:number;
  ItemName?:string;
  Description?:string;
  Nutrition?:string;
  URL?:string;
  ProofRead:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface DescriptionsInstance extends Sequelize.Instance<DescriptionsAttribute>, DescriptionsAttribute { }
export interface DescriptionsModel extends Sequelize.Model<DescriptionsInstance, DescriptionsAttribute> { }

// table: DeviceManufacturers
export interface DeviceManufacturersAttribute {
  ID:number;
  Manufacturer:string;
  IsArchived?:number;
  LastUpdate:Date;
}
export interface DeviceManufacturersInstance extends Sequelize.Instance<DeviceManufacturersAttribute>, DeviceManufacturersAttribute { }
export interface DeviceManufacturersModel extends Sequelize.Model<DeviceManufacturersInstance, DeviceManufacturersAttribute> { }

// table: DeviceModels
export interface DeviceModelsAttribute {
  ID:number;
  ModelName?:string;
  ManufacturerID:number;
  IsArchived?:number;
  LastUpdate:Date;
}
export interface DeviceModelsInstance extends Sequelize.Instance<DeviceModelsAttribute>, DeviceModelsAttribute { }
export interface DeviceModelsModel extends Sequelize.Model<DeviceModelsInstance, DeviceModelsAttribute> { }

// table: DevicePairings
export interface DevicePairingsAttribute {
  ID:number;
  TenantID:number;
  DateTime:Date;
  StaffID:number;
  HostID:number;
  DeviceID:number;
  Adhoc:number;
  LastUpdate:Date;
}
export interface DevicePairingsInstance extends Sequelize.Instance<DevicePairingsAttribute>, DevicePairingsAttribute { }
export interface DevicePairingsModel extends Sequelize.Model<DevicePairingsInstance, DevicePairingsAttribute> { }

// table: DeviceRegister
export interface DeviceRegisterAttribute {
  ID:number;
  TenantID:number;
  DeviceTypeID:number;
  Device?:string;
  DateReg?:Date;
  Active?:number;
  Online?:number;
  DeviceUID:string;
  Master?:number;
  Address?:string;
  Hostname?:string;
  DefaultPrinter?:number;
  DevicePIN?:string;
  PosGPS?:string;
  LastConnectDateTime?:Date;
  Version?:string;
  AuthCode?:string;
  PushDeviceToken?:string;
  IsArchived:number;
  LastUpdate:Date;
  LocationID?:number;
}
export interface DeviceRegisterInstance extends Sequelize.Instance<DeviceRegisterAttribute>, DeviceRegisterAttribute { }
export interface DeviceRegisterModel extends Sequelize.Model<DeviceRegisterInstance, DeviceRegisterAttribute> { }

// table: DeviceTypes
export interface DeviceTypesAttribute {
  ID:number;
  DeviceEnum?:string;
  DeviceInfo?:string;
  DeviceURL?:string;
  IsArchived:number;
  LastUpdate:Date;
}
export interface DeviceTypesInstance extends Sequelize.Instance<DeviceTypesAttribute>, DeviceTypesAttribute { }
export interface DeviceTypesModel extends Sequelize.Model<DeviceTypesInstance, DeviceTypesAttribute> { }

// table: Discounts
export interface DiscountsAttribute {
  ID:number;
  Name:string;
  DiscountType?:any;
  Discount:number;
  StartDate?:Date;
  EndDate?:Date;
  StartTime?:any;
  EndTime?:any;
  DaysOfWeek?:string;
  MinQty?:number;
  MaxQty?:number;
  CustomerTypeID?:number;
  MenuItemID?:number;
  MenuItemCategoryID?:number;
  TenantID:number;
  IsArchived:number;
}
export interface DiscountsInstance extends Sequelize.Instance<DiscountsAttribute>, DiscountsAttribute { }
export interface DiscountsModel extends Sequelize.Model<DiscountsInstance, DiscountsAttribute> { }

// table: DynamicMasterDataTypeServices
export interface DynamicMasterDataTypeServicesAttribute {
  DynamicMasterDataTypeID:number;
  DynamicServiceConfigID:number;
  EventType?:string;
  Service?:string;
  Description?:string;
  LastUpdate?:Date;
  CreateTime?:Date;
}
export interface DynamicMasterDataTypeServicesInstance extends Sequelize.Instance<DynamicMasterDataTypeServicesAttribute>, DynamicMasterDataTypeServicesAttribute { }
export interface DynamicMasterDataTypeServicesModel extends Sequelize.Model<DynamicMasterDataTypeServicesInstance, DynamicMasterDataTypeServicesAttribute> { }

// table: DynamicMasterDataTypes
export interface DynamicMasterDataTypesAttribute {
  ID:number;
  Type:string;
  TopicARN?:string;
  Description?:string;
  LastUpdate?:Date;
}
export interface DynamicMasterDataTypesInstance extends Sequelize.Instance<DynamicMasterDataTypesAttribute>, DynamicMasterDataTypesAttribute { }
export interface DynamicMasterDataTypesModel extends Sequelize.Model<DynamicMasterDataTypesInstance, DynamicMasterDataTypesAttribute> { }

// table: DynamicServiceConfig
export interface DynamicServiceConfigAttribute {
  ID:number;
  Name:string;
  IAMKey:string;
  IAMSecret:string;
  AWSQueueURL:string;
  QueueARN?:string;
  Region:string;
  MessageCount?:number;
  Interval?:number;
  FetchDelaySeconds?:number;
  WorkerCount?:number;
  MaxWorkerCount?:number;
  Active?:number;
  RetryThreshold:number;
  QueueMonitorThreshold:number;
  Dispatcher?:number;
  LogEvent:number;
  LastUpdate?:Date;
  CreateTime?:Date;
  Synchronous?:number;
  LastSeen?:Date;
  ServiceOnline?:number;
}
export interface DynamicServiceConfigInstance extends Sequelize.Instance<DynamicServiceConfigAttribute>, DynamicServiceConfigAttribute { }
export interface DynamicServiceConfigModel extends Sequelize.Model<DynamicServiceConfigInstance, DynamicServiceConfigAttribute> { }

// table: ExRates
export interface ExRatesAttribute {
  ID:number;
  Symbol?:string;
  SourceCurrency?:number;
  TargetCurrency?:number;
  ExRate?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface ExRatesInstance extends Sequelize.Instance<ExRatesAttribute>, ExRatesAttribute { }
export interface ExRatesModel extends Sequelize.Model<ExRatesInstance, ExRatesAttribute> { }

// table: GlobalSuppliers
export interface GlobalSuppliersAttribute {
  ID:number;
  Name?:string;
  ContactPerson?:string;
  Phone?:string;
  Email?:string;
  Website?:string;
  TermsAndConditions?:string;
  CreditLimit?:any;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface GlobalSuppliersInstance extends Sequelize.Instance<GlobalSuppliersAttribute>, GlobalSuppliersAttribute { }
export interface GlobalSuppliersModel extends Sequelize.Model<GlobalSuppliersInstance, GlobalSuppliersAttribute> { }

// table: GlobalTenantTemplates
export interface GlobalTenantTemplatesAttribute {
  ID:number;
  Name:string;
  Body?:string;
  CreateTime?:Date;
  UpdateTime?:Date;
}
export interface GlobalTenantTemplatesInstance extends Sequelize.Instance<GlobalTenantTemplatesAttribute>, GlobalTenantTemplatesAttribute { }
export interface GlobalTenantTemplatesModel extends Sequelize.Model<GlobalTenantTemplatesInstance, GlobalTenantTemplatesAttribute> { }

// table: InvoicePaymentInfo
export interface InvoicePaymentInfoAttribute {
  ID:number;
  TenantID:number;
  PaymentTypeID:number;
  PaymentRef?:string;
  Amount?:number;
  PayDate?:Date;
  InvoiceNo?:string;
  WaitronID?:number;
  AuthCode?:string;
  TransID?:number;
  CustomerReceipt?:string;
  MerchantReceipt?:string;
  SignatureImage?:any;
  IsArchived:number;
  LastUpdate:Date;
}
export interface InvoicePaymentInfoInstance extends Sequelize.Instance<InvoicePaymentInfoAttribute>, InvoicePaymentInfoAttribute { }
export interface InvoicePaymentInfoModel extends Sequelize.Model<InvoicePaymentInfoInstance, InvoicePaymentInfoAttribute> { }

// table: ItemLocal
export interface ItemLocalAttribute {
  ID:number;
  TenantID:number;
  MenuItemID:number;
  IsActive:number;
  MgrAuthRequired:number;
  TabAuthRequired:number;
  IsAvailable:number;
  SuspendToGo?:number;
  WaitronAlert?:string;
  GuestAlert?:string;
  LocalPrice?:number;
  LocalAllowOpenPrice?:number;
  LocalTaxGroupID?:number;
  SpecialFlag:number;
  SQFlag:number;
  SpecialPrice?:number;
  SQPrice?:number;
  StationID?:number;
  StationDeviceID?:number;
  WtrnPoints?:number;
  WtrnPointsEndDate?:Date;
  WtrnComPercent?:number;
  WtrnComValue?:number;
  WtrnComEndDate?:Date;
  IsArchived:number;
  LastUpdate:Date;
}
export interface ItemLocalInstance extends Sequelize.Instance<ItemLocalAttribute>, ItemLocalAttribute { }
export interface ItemLocalModel extends Sequelize.Model<ItemLocalInstance, ItemLocalAttribute> { }

// table: Languages
export interface LanguagesAttribute {
  ID:number;
  LangCode:string;
  Description?:string;
  UsageCount?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface LanguagesInstance extends Sequelize.Instance<LanguagesAttribute>, LanguagesAttribute { }
export interface LanguagesModel extends Sequelize.Model<LanguagesInstance, LanguagesAttribute> { }

// table: LocaleID
export interface LocaleIDAttribute {
  LocaleID:string;
  TimeZone?:string;
}
export interface LocaleIDInstance extends Sequelize.Instance<LocaleIDAttribute>, LocaleIDAttribute { }
export interface LocaleIDModel extends Sequelize.Model<LocaleIDInstance, LocaleIDAttribute> { }

// table: LocalizedStrings
export interface LocalizedStringsAttribute {
  ID:number;
  EnumID?:number;
  ItemID?:number;
  LangID?:number;
  Text?:string;
  TenantID?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface LocalizedStringsInstance extends Sequelize.Instance<LocalizedStringsAttribute>, LocalizedStringsAttribute { }
export interface LocalizedStringsModel extends Sequelize.Model<LocalizedStringsInstance, LocalizedStringsAttribute> { }

// table: Locations
export interface LocationsAttribute {
  ID:number;
  HostLocationID?:string;
  TenantID?:number;
  Name?:string;
  Description?:string;
  CreateTime?:Date;
  UpdateTime?:Date;
}
export interface LocationsInstance extends Sequelize.Instance<LocationsAttribute>, LocationsAttribute { }
export interface LocationsModel extends Sequelize.Model<LocationsInstance, LocationsAttribute> { }

// table: LoyaltyPrograms
export interface LoyaltyProgramsAttribute {
  ID:number;
  SponsorID:number;
  ProgramName:string;
  SignupMsg?:string;
  SignupURL?:string;
  Note?:string;
  IsArchived?:number;
  LastUpdate:Date;
}
export interface LoyaltyProgramsInstance extends Sequelize.Instance<LoyaltyProgramsAttribute>, LoyaltyProgramsAttribute { }
export interface LoyaltyProgramsModel extends Sequelize.Model<LoyaltyProgramsInstance, LoyaltyProgramsAttribute> { }

// table: LoyaltySponsors
export interface LoyaltySponsorsAttribute {
  ID:number;
  SponsorName:string;
  URL?:string;
  IsArchived?:number;
  LastUpdate:Date;
}
export interface LoyaltySponsorsInstance extends Sequelize.Instance<LoyaltySponsorsAttribute>, LoyaltySponsorsAttribute { }
export interface LoyaltySponsorsModel extends Sequelize.Model<LoyaltySponsorsInstance, LoyaltySponsorsAttribute> { }

// table: MasterDataTypeServices
export interface MasterDataTypeServicesAttribute {
  MasterDataTypeID:number;
  ServiceConfigID:number;
  EventType?:string;
  Service?:string;
  Description?:string;
  LastUpdate?:Date;
  CreateTime?:Date;
}
export interface MasterDataTypeServicesInstance extends Sequelize.Instance<MasterDataTypeServicesAttribute>, MasterDataTypeServicesAttribute { }
export interface MasterDataTypeServicesModel extends Sequelize.Model<MasterDataTypeServicesInstance, MasterDataTypeServicesAttribute> { }

// table: MasterDataTypes
export interface MasterDataTypesAttribute {
  ID:number;
  Type:string;
  TopicARN?:string;
  Description?:string;
  LastUpdate?:Date;
}
export interface MasterDataTypesInstance extends Sequelize.Instance<MasterDataTypesAttribute>, MasterDataTypesAttribute { }
export interface MasterDataTypesModel extends Sequelize.Model<MasterDataTypesInstance, MasterDataTypesAttribute> { }

// table: Media
export interface MediaAttribute {
  ID:number;
  TenantID?:number;
  ImgName:string;
  Orientation:string;
  FileName:string;
  LastUpdate:Date;
  IsArchived:number;
  BasicUrl?:string;
}
export interface MediaInstance extends Sequelize.Instance<MediaAttribute>, MediaAttribute { }
export interface MediaModel extends Sequelize.Model<MediaInstance, MediaAttribute> { }

// table: MediaTags
export interface MediaTagsAttribute {
  ID:number;
  TenantID:number;
  MediaID:number;
  TagName:string;
  LastUpdate:Date;
  IsArchived:number;
}
export interface MediaTagsInstance extends Sequelize.Instance<MediaTagsAttribute>, MediaTagsAttribute { }
export interface MediaTagsModel extends Sequelize.Model<MediaTagsInstance, MediaTagsAttribute> { }

// table: MenuAddOns
export interface MenuAddOnsAttribute {
  ID:number;
  TenantID:number;
  MenuItemID:number;
  AddOnGroupID:number;
  Seq?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface MenuAddOnsInstance extends Sequelize.Instance<MenuAddOnsAttribute>, MenuAddOnsAttribute { }
export interface MenuAddOnsModel extends Sequelize.Model<MenuAddOnsInstance, MenuAddOnsAttribute> { }

// table: MenuDiffs
export interface MenuDiffsAttribute {
  ID:number;
  TenantID:number;
  MenuID:number;
  CurrentVersion:string;
  LatestVersion:string;
  Diff?:string;
  DiffKey?:string;
  CreateTime?:Date;
}
export interface MenuDiffsInstance extends Sequelize.Instance<MenuDiffsAttribute>, MenuDiffsAttribute { }
export interface MenuDiffsModel extends Sequelize.Model<MenuDiffsInstance, MenuDiffsAttribute> { }

// table: MenuFileMaps
export interface MenuFileMapsAttribute {
  ID:number;
  TenantID:number;
  MenuID?:number;
  ColumnNames?:string;
  ColumnsMap?:string;
  FileKey:string;
  BucketName:string;
  FileS3Url?:string;
  DefaultValueMap?:string;
  Updating:number;
  CreateTime?:Date;
  UpdateTime?:Date;
  Error?:string;
}
export interface MenuFileMapsInstance extends Sequelize.Instance<MenuFileMapsAttribute>, MenuFileMapsAttribute { }
export interface MenuFileMapsModel extends Sequelize.Model<MenuFileMapsInstance, MenuFileMapsAttribute> { }

// table: MenuItemCategory
export interface MenuItemCategoryAttribute {
  ID:number;
  TenantID:number;
  CategoryName:string;
  IsArchived:number;
  LastUpdate:Date;
  Seq?:number;
}
export interface MenuItemCategoryInstance extends Sequelize.Instance<MenuItemCategoryAttribute>, MenuItemCategoryAttribute { }
export interface MenuItemCategoryModel extends Sequelize.Model<MenuItemCategoryInstance, MenuItemCategoryAttribute> { }

// table: MenuItemFavorites
export interface MenuItemFavoritesAttribute {
  ID:number;
  TenantID:number;
  CustomerID:number;
  MenuItemID?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface MenuItemFavoritesInstance extends Sequelize.Instance<MenuItemFavoritesAttribute>, MenuItemFavoritesAttribute { }
export interface MenuItemFavoritesModel extends Sequelize.Model<MenuItemFavoritesInstance, MenuItemFavoritesAttribute> { }

// table: MenuItemIntents
export interface MenuItemIntentsAttribute {
  ID:number;
  MenuItemID?:number;
  TenantID?:number;
  MenuID?:number;
  IntentName?:string;
  BotID?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface MenuItemIntentsInstance extends Sequelize.Instance<MenuItemIntentsAttribute>, MenuItemIntentsAttribute { }
export interface MenuItemIntentsModel extends Sequelize.Model<MenuItemIntentsInstance, MenuItemIntentsAttribute> { }

// table: MenuItemRatings
export interface MenuItemRatingsAttribute {
  ID:number;
  TenantID:number;
  CustomerID:number;
  OrderID?:number;
  MenuItemID?:number;
  RatingDateTime?:Date;
  Rating?:number;
  Comment?:string;
  IsArchived:number;
  LastUpdate:Date;
}
export interface MenuItemRatingsInstance extends Sequelize.Instance<MenuItemRatingsAttribute>, MenuItemRatingsAttribute { }
export interface MenuItemRatingsModel extends Sequelize.Model<MenuItemRatingsInstance, MenuItemRatingsAttribute> { }

// table: MenuItemSyncHistory
export interface MenuItemSyncHistoryAttribute {
  ID:number;
  TenantID:number;
  MenuItemID:number;
  AccountingItemID:string;
  LocalItemData?:string;
  AccountingItemData?:string;
  SyncDateTime?:Date;
}
export interface MenuItemSyncHistoryInstance extends Sequelize.Instance<MenuItemSyncHistoryAttribute>, MenuItemSyncHistoryAttribute { }
export interface MenuItemSyncHistoryModel extends Sequelize.Model<MenuItemSyncHistoryInstance, MenuItemSyncHistoryAttribute> { }

// table: MenuItemTags
export interface MenuItemTagsAttribute {
  ID:number;
  TenantID:number;
  TagID:number;
  Seq?:number;
  MenuItemID:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface MenuItemTagsInstance extends Sequelize.Instance<MenuItemTagsAttribute>, MenuItemTagsAttribute { }
export interface MenuItemTagsModel extends Sequelize.Model<MenuItemTagsInstance, MenuItemTagsAttribute> { }

// table: MenuItems
export interface MenuItemsAttribute {
  ID:number;
  TenantID:number;
  ItemType:string;
  MenuID?:number;
  ParentID?:string;
  ItemID?:string;
  ShortName?:string;
  PrintSlipName?:string;
  CategoryID?:number;
  StockItemID?:number;
  Seq?:number;
  IsModOnly?:number;
  IsIngredOnly?:number;
  IsProduced:number;
  Req86Countdown?:number;
  BrandItemID?:number;
  ShortcutID?:number;
  Unit?:string;
  UnitID?:number;
  Cost?:number;
  Price?:number;
  PriceVar?:string;
  AllowOpenPrice?:number;
  OpenPriceAuthReq?:number;
  AllowLocalOpenPrice?:number;
  TaxGroupID?:number;
  IsTaxable:number;
  AllowLocalPrice:number;
  ReqMgrSignOff:number;
  Notes?:string;
  PrepTime?:any;
  ServeFrom?:any;
  ServeUntil?:any;
  SrvSu:number;
  SrvMo:number;
  SrvTu:number;
  SrvWe:number;
  SrvTh:number;
  SrvFr:number;
  SrvSa:number;
  HostPLU?:string;
  HostedAccountID?:string;
  HostedAccountCode?:string;
  BarCode?:string;
  HostSalesDept?:string;
  ImageMediaID?:number;
  PromoImageMediaID?:number;
  HaveImages:number;
  HasPromoImage:number;
  PromoServiceGroupID?:number;
  WaiterOnly:number;
  AvailableToGo:number;
  AvailableTA:number;
  AvailableSelfService:number;
  AutoAcceptToGo:number;
  ReturnPeriodDays?:number;
  WarrantyPeriodMnths?:number;
  WarrantyNote?:string;
  IsArchived:number;
  LastUpdate:Date;
}
export interface MenuItemsInstance extends Sequelize.Instance<MenuItemsAttribute>, MenuItemsAttribute { }
export interface MenuItemsModel extends Sequelize.Model<MenuItemsInstance, MenuItemsAttribute> { }

// table: MenuItemsTemp
export interface MenuItemsTempAttribute {
  ID:number;
  TenantID:number;
  MenuID:number;
  ParentID?:string;
  ItemID?:string;
  ShortName:string;
  CategoryID?:number;
  Seq?:number;
  IsModOnly:number;
  BrandItemID?:number;
  ShortcutID?:number;
  Cost?:number;
  Price?:number;
  PriceVar?:string;
  IsTaxable:number;
  AllowLocalPrice:number;
  ReqMgrSignOff:number;
  Notes?:string;
  PrepTime?:any;
  ServeFrom?:any;
  ServeUntil?:any;
  SrvSu:number;
  SrvMo:number;
  SrvTu:number;
  SrvWe:number;
  SrvTh:number;
  SrvFr:number;
  SrvSa:number;
  HostPLU?:string;
  HostedAccountID?:string;
  BarCode?:string;
  HostSalesDept?:string;
  HaveImages:number;
  HasPromoImage:number;
  PromoServiceGroupID?:number;
  WaiterOnly:number;
  AvailableToGo:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface MenuItemsTempInstance extends Sequelize.Instance<MenuItemsTempAttribute>, MenuItemsTempAttribute { }
export interface MenuItemsTempModel extends Sequelize.Model<MenuItemsTempInstance, MenuItemsTempAttribute> { }

// table: MenuOptions
export interface MenuOptionsAttribute {
  ID:number;
  TenantID:number;
  MenuItemID:number;
  OptionGroupID:number;
  Seq?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface MenuOptionsInstance extends Sequelize.Instance<MenuOptionsAttribute>, MenuOptionsAttribute { }
export interface MenuOptionsModel extends Sequelize.Model<MenuOptionsInstance, MenuOptionsAttribute> { }

// table: MenuPortions
export interface MenuPortionsAttribute {
  ID:number;
  TenantID?:number;
  MenuItemID?:number;
  PortionGroupID?:number;
  Seq?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface MenuPortionsInstance extends Sequelize.Instance<MenuPortionsAttribute>, MenuPortionsAttribute { }
export interface MenuPortionsModel extends Sequelize.Model<MenuPortionsInstance, MenuPortionsAttribute> { }

// table: MenuPublishVersions
export interface MenuPublishVersionsAttribute {
  Version:string;
  TenantID:number;
  MenuID:number;
  MenuFileKey:string;
  PatchFileKey?:string;
  BucketName:string;
  MenuObject:string;
  PatchObject?:string;
  PatchS3Url?:string;
  MenuS3Url?:string;
  CreateTime?:Date;
}
export interface MenuPublishVersionsInstance extends Sequelize.Instance<MenuPublishVersionsAttribute>, MenuPublishVersionsAttribute { }
export interface MenuPublishVersionsModel extends Sequelize.Model<MenuPublishVersionsInstance, MenuPublishVersionsAttribute> { }

// table: MenuTree
export interface MenuTreeAttribute {
  ID:number;
  TenantID?:number;
  MenuID?:number;
  ItemID?:number;
  ParentID?:number;
  CloneTreeID?:number;
  Seq?:number;
  LastUpdate:Date;
  IsArchived:number;
  MenuTypeID:number;
}
export interface MenuTreeInstance extends Sequelize.Instance<MenuTreeAttribute>, MenuTreeAttribute { }
export interface MenuTreeModel extends Sequelize.Model<MenuTreeInstance, MenuTreeAttribute> { }

// table: Menus
export interface MenusAttribute {
  ID:number;
  TenantID:number;
  Name:string;
  S3URL:string;
  Active:number;
  BusyPublishing:number;
  BusyTranslating:number;
  IsArchived:number;
  LastUpdate:Date;
  LastMenuChange?:Date;
  LastMenuPublish?:Date;
  LatestVersion?:string;
  PublishError?:string;
}
export interface MenusInstance extends Sequelize.Instance<MenusAttribute>, MenusAttribute { }
export interface MenusModel extends Sequelize.Model<MenusInstance, MenusAttribute> { }

// table: Nav
export interface NavAttribute {
  ID:number;
  ParentID?:number;
  Title:string;
  ControllerID:string;
  Controller:string;
  Action:string;
  OverrideTenantRequired?:number;
  IconClass:string;
  Active:number;
  Seq:number;
}
export interface NavInstance extends Sequelize.Instance<NavAttribute>, NavAttribute { }
export interface NavModel extends Sequelize.Model<NavInstance, NavAttribute> { }

// table: Nutrition
export interface NutritionAttribute {
  ID:number;
  TenantID:number;
  BrandItemID?:number;
  MenuItemID?:number;
  NameID:number;
  ValuePer100g?:number;
  ValuePerServing?:number;
  RDAPercent?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface NutritionInstance extends Sequelize.Instance<NutritionAttribute>, NutritionAttribute { }
export interface NutritionModel extends Sequelize.Model<NutritionInstance, NutritionAttribute> { }

// table: OptionEnumerationValues
export interface OptionEnumerationValuesAttribute {
  ID:number;
  OptionSlotTypeID?:number;
  OptionEnumerationValue?:string;
  OptionID?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface OptionEnumerationValuesInstance extends Sequelize.Instance<OptionEnumerationValuesAttribute>, OptionEnumerationValuesAttribute { }
export interface OptionEnumerationValuesModel extends Sequelize.Model<OptionEnumerationValuesInstance, OptionEnumerationValuesAttribute> { }

// table: OptionGroupNameSlotTypes
export interface OptionGroupNameSlotTypesAttribute {
  ID:number;
  TenantID?:number;
  MenuID?:number;
  MenuItemID?:number;
  OptionGroupID?:number;
  OptionSlotType?:string;
  BotID?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface OptionGroupNameSlotTypesInstance extends Sequelize.Instance<OptionGroupNameSlotTypesAttribute>, OptionGroupNameSlotTypesAttribute { }
export interface OptionGroupNameSlotTypesModel extends Sequelize.Model<OptionGroupNameSlotTypesInstance, OptionGroupNameSlotTypesAttribute> { }

// table: OptionGroups
export interface OptionGroupsAttribute {
  ID:number;
  TenantID:number;
  GroupName?:string;
  GroupNameStringID?:number;
  OpenExpanded:number;
  MinSelect:number;
  MaxSelect:number;
  AvailableSS:number;
  AvailableTG:number;
  AvailableTA:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface OptionGroupsInstance extends Sequelize.Instance<OptionGroupsAttribute>, OptionGroupsAttribute { }
export interface OptionGroupsModel extends Sequelize.Model<OptionGroupsInstance, OptionGroupsAttribute> { }

// table: Options
export interface OptionsAttribute {
  ID:number;
  TenantID:number;
  OptionGroupID:number;
  AddOnsID?:number;
  PortionsID?:number;
  Seq?:number;
  Description?:string;
  OptionDescriptionStringID?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface OptionsInstance extends Sequelize.Instance<OptionsAttribute>, OptionsAttribute { }
export interface OptionsModel extends Sequelize.Model<OptionsInstance, OptionsAttribute> { }

// table: OrderAddOns
export interface OrderAddOnsAttribute {
  ID:number;
  TenantID?:number;
  OrderID:number;
  AddOnID:number;
  IncludeFlag?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface OrderAddOnsInstance extends Sequelize.Instance<OrderAddOnsAttribute>, OrderAddOnsAttribute { }
export interface OrderAddOnsModel extends Sequelize.Model<OrderAddOnsInstance, OrderAddOnsAttribute> { }

// table: OrderOptions
export interface OrderOptionsAttribute {
  ID:number;
  TenantID?:number;
  OptionID:number;
  OrderID:number;
  IncludeFlag?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface OrderOptionsInstance extends Sequelize.Instance<OrderOptionsAttribute>, OrderOptionsAttribute { }
export interface OrderOptionsModel extends Sequelize.Model<OrderOptionsInstance, OrderOptionsAttribute> { }

// table: OrderPortion
export interface OrderPortionAttribute {
  ID:number;
  TenantID?:number;
  OrderID:number;
  PortionID:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface OrderPortionInstance extends Sequelize.Instance<OrderPortionAttribute>, OrderPortionAttribute { }
export interface OrderPortionModel extends Sequelize.Model<OrderPortionInstance, OrderPortionAttribute> { }

// table: OrderTaxLines
export interface OrderTaxLinesAttribute {
  ID:number;
  TenantID:number;
  OrderLineID:number;
  TaxTotal?:number;
  TaxID:number;
  IsArchived:number;
  ArchivedDateTime?:Date;
  LastUpdate:Date;
}
export interface OrderTaxLinesInstance extends Sequelize.Instance<OrderTaxLinesAttribute>, OrderTaxLinesAttribute { }
export interface OrderTaxLinesModel extends Sequelize.Model<OrderTaxLinesInstance, OrderTaxLinesAttribute> { }

// table: Orders
export interface OrdersAttribute {
  ID:number;
  TenantID:number;
  LineNo:number;
  MenuItemID:number;
  ParentOrder?:number;
  AddOnID?:number;
  Qty?:number;
  InvQty?:number;
  Price?:number;
  LineTotal?:number;
  NetTotal?:number;
  Discount?:number;
  TaxTotal?:number;
  CostTotal?:number;
  TabID?:number;
  GuestNum?:number;
  OnHold:number;
  OnHoldManual?:number;
  HoldGroup?:number;
  OrderDateTime?:Date;
  DeliveredDateTime?:Date;
  PrimaryMenuItemID?:number;
  PrintCount?:number;
  Note?:string;
  HostOrderID?:string;
  PaymentGroupID?:number;
  SaleType?:string;
  Discounted?:number;
  Voided?:number;
  Returned?:number;
  UserCode?:string;
  IsArchived:number;
  ArchivedDateTime?:Date;
  LastUpdate:Date;
  isRecommended:number;
}
export interface OrdersInstance extends Sequelize.Instance<OrdersAttribute>, OrdersAttribute { }
export interface OrdersModel extends Sequelize.Model<OrdersInstance, OrdersAttribute> { }

// table: OverridePLU
export interface OverridePLUAttribute {
  ID:number;
  TenantID:number;
  MenuID:number;
  MenuItemID:number;
  eModifierTypeID:number;
  ModifierGroupID:number;
  ModifierID:number;
  HostOverridePLU:string;
  OverrideItemID?:number;
  OverridePrice?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface OverridePLUInstance extends Sequelize.Instance<OverridePLUAttribute>, OverridePLUAttribute { }
export interface OverridePLUModel extends Sequelize.Model<OverridePLUInstance, OverridePLUAttribute> { }

// table: POSIntVersions
export interface POSIntVersionsAttribute {
  ID:number;
  TenantID:number;
  POSVendorID:number;
  TOPIVer?:string;
  POSVer?:string;
  POSAPIVer?:string;
  LastUpdate:Date;
}
export interface POSIntVersionsInstance extends Sequelize.Instance<POSIntVersionsAttribute>, POSIntVersionsAttribute { }
export interface POSIntVersionsModel extends Sequelize.Model<POSIntVersionsInstance, POSIntVersionsAttribute> { }

// table: POSVendors
export interface POSVendorsAttribute {
  ID:number;
  Name:string;
  eValue:string;
  IsArchived:number;
  LastUpdate:Date;
}
export interface POSVendorsInstance extends Sequelize.Instance<POSVendorsAttribute>, POSVendorsAttribute { }
export interface POSVendorsModel extends Sequelize.Model<POSVendorsInstance, POSVendorsAttribute> { }

// table: PastelCSVUploads
export interface PastelCSVUploadsAttribute {
  ID:number;
  TenantID:number;
  CustomerCode:string;
  UploadDate:Date;
  S3_URL:string;
  Error?:string;
  ScheduleType?:string;
  DataPeriod?:string;
}
export interface PastelCSVUploadsInstance extends Sequelize.Instance<PastelCSVUploadsAttribute>, PastelCSVUploadsAttribute { }
export interface PastelCSVUploadsModel extends Sequelize.Model<PastelCSVUploadsInstance, PastelCSVUploadsAttribute> { }

// table: PastelProducts
export interface PastelProductsAttribute {
  ID:number;
  TenantID:number;
  MenuItemID:number;
  StockCode:string;
  LineType:string;
  Store?:string;
}
export interface PastelProductsInstance extends Sequelize.Instance<PastelProductsAttribute>, PastelProductsAttribute { }
export interface PastelProductsModel extends Sequelize.Model<PastelProductsInstance, PastelProductsAttribute> { }

// table: PastelTenantConfig
export interface PastelTenantConfigAttribute {
  ID:number;
  TenantID:number;
  FinancialYearStart:string;
  ScheduledCSVUpload?:string;
  EmailAddress?:string;
  SendEmail:number;
}
export interface PastelTenantConfigInstance extends Sequelize.Instance<PastelTenantConfigAttribute>, PastelTenantConfigAttribute { }
export interface PastelTenantConfigModel extends Sequelize.Model<PastelTenantConfigInstance, PastelTenantConfigAttribute> { }

// table: PastelTenantCustomers
export interface PastelTenantCustomersAttribute {
  ID:number;
  TenantID:number;
  CustomerId:number;
  CustomerName?:string;
  CustomerCode:string;
  OrderNumber:string;
  IncExTax:string;
  InvoiceMessage:string;
  DeliveryAddress:string;
  SettlementTerms:number;
  Telephone:string;
  FaxNumber:string;
  ContactPerson:string;
  FreightMethod:string;
  ShipDeliver:string;
  Discount:number;
  AdditionalCosts:string;
  IsDefault:number;
}
export interface PastelTenantCustomersInstance extends Sequelize.Instance<PastelTenantCustomersAttribute>, PastelTenantCustomersAttribute { }
export interface PastelTenantCustomersModel extends Sequelize.Model<PastelTenantCustomersInstance, PastelTenantCustomersAttribute> { }

// table: PaymentInfo
export interface PaymentInfoAttribute {
  ID:number;
  HostPaymentID?:string;
  StaffShift?:number;
  TenantID:number;
  PaymentTypeID:number;
  PaymentRef?:string;
  TxID?:string;
  TxNotes?:string;
  Amount?:number;
  Gratuity?:number;
  ChangeGiven?:number;
  Balance?:number;
  AltCurrencyID?:number;
  AltAmount?:number;
  TabID?:number;
  WaitronID?:number;
  IsArchived:number;
  ArchivedDateTime?:Date;
  LastUpdate:Date;
  percentage_discount?:number;
}
export interface PaymentInfoInstance extends Sequelize.Instance<PaymentInfoAttribute>, PaymentInfoAttribute { }
export interface PaymentInfoModel extends Sequelize.Model<PaymentInfoInstance, PaymentInfoAttribute> { }

// table: PaymentType
export interface PaymentTypeAttribute {
  ID:number;
  TypeCode:string;
  PaymentDescr?:string;
  PaymentDescrStringID?:number;
  PaymentThirdPartyInfo?:string;
  AuthUserPrompt?:string;
  AuthPassPrompt?:string;
  AuthNotifyKeyPrompt?:string;
  DecryptionKeyPrompt?:string;
  LangID?:number;
  EnabledPOS:number;
  EnabledToGo:number;
  EnabledApps:number;
  EnabledTA:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface PaymentTypeInstance extends Sequelize.Instance<PaymentTypeAttribute>, PaymentTypeAttribute { }
export interface PaymentTypeModel extends Sequelize.Model<PaymentTypeInstance, PaymentTypeAttribute> { }

// table: PeachPayments
export interface PeachPaymentsAttribute {
  ID:number;
  PeachTransactionID:string;
  TenantID:number;
  TabID:number;
  PaymentStatus?:string;
  LastUpdate?:Date;
  CreateTime?:Date;
}
export interface PeachPaymentsInstance extends Sequelize.Instance<PeachPaymentsAttribute>, PeachPaymentsAttribute { }
export interface PeachPaymentsModel extends Sequelize.Model<PeachPaymentsInstance, PeachPaymentsAttribute> { }

// table: PettyCashReasons
export interface PettyCashReasonsAttribute {
  ID:number;
  TenantID:number;
  Name?:string;
  Description?:string;
  LastUpdate:Date;
  CreateTime:Date;
  TenantSupplierID?:number;
  Type?:string;
}
export interface PettyCashReasonsInstance extends Sequelize.Instance<PettyCashReasonsAttribute>, PettyCashReasonsAttribute { }
export interface PettyCashReasonsModel extends Sequelize.Model<PettyCashReasonsInstance, PettyCashReasonsAttribute> { }

// table: PettyPayments
export interface PettyPaymentsAttribute {
  ID:number;
  TenantID:number;
  Amount?:number;
  Notes?:string;
  WaitronID?:number;
  HostID?:number;
  DateCreated:Date;
  IsArchived:number;
}
export interface PettyPaymentsInstance extends Sequelize.Instance<PettyPaymentsAttribute>, PettyPaymentsAttribute { }
export interface PettyPaymentsModel extends Sequelize.Model<PettyPaymentsInstance, PettyPaymentsAttribute> { }

// table: PortionEnumerationValues
export interface PortionEnumerationValuesAttribute {
  ID:number;
  PortionSlotTypeID?:number;
  PortionEnumerationValue?:string;
  PortionID?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface PortionEnumerationValuesInstance extends Sequelize.Instance<PortionEnumerationValuesAttribute>, PortionEnumerationValuesAttribute { }
export interface PortionEnumerationValuesModel extends Sequelize.Model<PortionEnumerationValuesInstance, PortionEnumerationValuesAttribute> { }

// table: PortionGroupNameSlotTypes
export interface PortionGroupNameSlotTypesAttribute {
  ID:number;
  TenantID?:number;
  MenuID?:number;
  MenuItemID?:number;
  PortionGroupID?:number;
  PortionSlotType?:string;
  BotID?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface PortionGroupNameSlotTypesInstance extends Sequelize.Instance<PortionGroupNameSlotTypesAttribute>, PortionGroupNameSlotTypesAttribute { }
export interface PortionGroupNameSlotTypesModel extends Sequelize.Model<PortionGroupNameSlotTypesInstance, PortionGroupNameSlotTypesAttribute> { }

// table: PortionGroups
export interface PortionGroupsAttribute {
  ID:number;
  TenantID:number;
  GroupName?:string;
  GroupNameStringID?:number;
  WholeRequired:number;
  AvailableSS:number;
  AvailableTG:number;
  AvailableTA:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface PortionGroupsInstance extends Sequelize.Instance<PortionGroupsAttribute>, PortionGroupsAttribute { }
export interface PortionGroupsModel extends Sequelize.Model<PortionGroupsInstance, PortionGroupsAttribute> { }

// table: Portions
export interface PortionsAttribute {
  ID:number;
  TenantID:number;
  PortionGroupID:number;
  PortionName?:string;
  PortionNameStringID?:number;
  PortionPercent?:number;
  AddOnPortionPercent?:number;
  AddOnPricePercent?:number;
  PricePercent?:number;
  Seq?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface PortionsInstance extends Sequelize.Instance<PortionsAttribute>, PortionsAttribute { }
export interface PortionsModel extends Sequelize.Model<PortionsInstance, PortionsAttribute> { }

// table: PosDatabaseBackups
export interface PosDatabaseBackupsAttribute {
  ID:number;
  TenantID:number;
  DeviceUID:string;
  TimeStamp:Date;
  S3_URL:string;
  Error?:string;
}
export interface PosDatabaseBackupsInstance extends Sequelize.Instance<PosDatabaseBackupsAttribute>, PosDatabaseBackupsAttribute { }
export interface PosDatabaseBackupsModel extends Sequelize.Model<PosDatabaseBackupsInstance, PosDatabaseBackupsAttribute> { }

// table: PosSyncDispatcherLog
export interface PosSyncDispatcherLogAttribute {
  ID:string;
  TenantID?:number;
  Type?:string;
  TopicArn?:string;
  Subscriptions?:string;
  RequiredSubscriptions?:string;
  NotSubscribed?:string;
  Success?:number;
  Status?:string;
  Error?:string;
  RetryCount?:number;
  Data?:string;
  DispatchTime?:Date;
}
export interface PosSyncDispatcherLogInstance extends Sequelize.Instance<PosSyncDispatcherLogAttribute>, PosSyncDispatcherLogAttribute { }
export interface PosSyncDispatcherLogModel extends Sequelize.Model<PosSyncDispatcherLogInstance, PosSyncDispatcherLogAttribute> { }

// table: PrinterDefaults
export interface PrinterDefaultsAttribute {
  ID:number;
  Brand?:string;
  ModelName?:string;
  DefaultName?:string;
  FontAWidth?:number;
  FontBWidth?:number;
  LineSpacing?:number;
  CreateTime?:Date;
  UpdateTime?:Date;
}
export interface PrinterDefaultsInstance extends Sequelize.Instance<PrinterDefaultsAttribute>, PrinterDefaultsAttribute { }
export interface PrinterDefaultsModel extends Sequelize.Model<PrinterDefaultsInstance, PrinterDefaultsAttribute> { }

// table: Printers
export interface PrintersAttribute {
  ID:number;
  TenantID:number;
  PrinterName?:string;
  Location?:string;
  IPAddr?:string;
  IPPort?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface PrintersInstance extends Sequelize.Instance<PrintersAttribute>, PrintersAttribute { }
export interface PrintersModel extends Sequelize.Model<PrintersInstance, PrintersAttribute> { }

// table: PurchaseOrderItems
export interface PurchaseOrderItemsAttribute {
  BaseUnitID?:number;
  BaseUnitDescription?:string;
  BaseUnitQuantity?:any;
  BaseUnitCost?:any;
  PurchaseOrderID?:string;
  MenuItemID?:number;
  ItemDescription?:string;
  SupplierID?:number;
  SupplierName?:string;
  CreateTime?:Date;
  LastUpdate?:Date;
  ID:string;
  TenantID:number;
  SelectedUnitID:number;
  SelectedUnitDescription?:string;
  SelectedUnitQuantity?:any;
  Total?:any;
  ProportionalDeliveryCost?:number;
  ConversionFactor?:any;
}
export interface PurchaseOrderItemsInstance extends Sequelize.Instance<PurchaseOrderItemsAttribute>, PurchaseOrderItemsAttribute { }
export interface PurchaseOrderItemsModel extends Sequelize.Model<PurchaseOrderItemsInstance, PurchaseOrderItemsAttribute> { }

// table: PublishedSettings
export interface PublishedSettingsAttribute {
  TenantID:number;
  SourceID?:number;
  Type:string;
  Version:string;
  FileKey:string;
  BucketName:string;
  S3Url?:string;
  PatchFileKey?:string;
  PatchS3Url?:string;
  CreateTime?:Date;
}
export interface PublishedSettingsInstance extends Sequelize.Instance<PublishedSettingsAttribute>, PublishedSettingsAttribute { }
export interface PublishedSettingsModel extends Sequelize.Model<PublishedSettingsInstance, PublishedSettingsAttribute> { }

// table: PurchaseOrders
export interface PurchaseOrdersAttribute {
  ID:string;
  Supplier?:number;
  SupplierRef?:string;
  OrderDate?:Date;
  DateExpected?:Date;
  DateReceived?:Date;
  CreateTime?:Date;
  LastUpdate?:Date;
  PurchaseOrderNumber:string;
  IsArchived?:number;
  TenantID:number;
  Note?:string;
  Status?:string;
  Total?:number;
  DeliveryCost?:number;
  ReplenishmentDays?:number;
}
export interface PurchaseOrdersInstance extends Sequelize.Instance<PurchaseOrdersAttribute>, PurchaseOrdersAttribute { }
export interface PurchaseOrdersModel extends Sequelize.Model<PurchaseOrdersInstance, PurchaseOrdersAttribute> { }

// table: RecipeIngredients
export interface RecipeIngredientsAttribute {
  ID:number;
  RecipeMenuItemID?:number;
  IngredientMenuItemID?:number;
  Quantity?:any;
  StockUnitID?:number;
  WastePercentage?:any;
  create_time?:Date;
  update_time?:Date;
}
export interface RecipeIngredientsInstance extends Sequelize.Instance<RecipeIngredientsAttribute>, RecipeIngredientsAttribute { }
export interface RecipeIngredientsModel extends Sequelize.Model<RecipeIngredientsInstance, RecipeIngredientsAttribute> { }

// table: ReservedStockItems
export interface ReservedStockItemsAttribute {
  ID:number;
  StockItem?:number;
  CustomerOrder?:number;
  Quantity?:any;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface ReservedStockItemsInstance extends Sequelize.Instance<ReservedStockItemsAttribute>, ReservedStockItemsAttribute { }
export interface ReservedStockItemsModel extends Sequelize.Model<ReservedStockItemsInstance, ReservedStockItemsAttribute> { }

// table: ResourceSyncLog
export interface ResourceSyncLogAttribute {
  ID:number;
  Host?:string;
  Method?:string;
  Endpoint?:string;
  Headers?:string;
  Body?:string;
  Acknowledged?:number;
  AcknowledgedTime?:Date;
  ErrorResponse?:string;
  ErrorMessage?:string;
  RetryCount?:number;
  CreateTime?:Date;
  UpdateTime?:Date;
}
export interface ResourceSyncLogInstance extends Sequelize.Instance<ResourceSyncLogAttribute>, ResourceSyncLogAttribute { }
export interface ResourceSyncLogModel extends Sequelize.Model<ResourceSyncLogInstance, ResourceSyncLogAttribute> { }

// table: RestaurantRatings
export interface RestaurantRatingsAttribute {
  ID:number;
  TenantID:number;
  TabID:number;
  WaitronID:number;
  RatingDateTime?:Date;
  LocationRating?:number;
  ViewRating?:number;
  AblutionsRating?:number;
  OverallRating?:number;
  WineListRating?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface RestaurantRatingsInstance extends Sequelize.Instance<RestaurantRatingsAttribute>, RestaurantRatingsAttribute { }
export interface RestaurantRatingsModel extends Sequelize.Model<RestaurantRatingsInstance, RestaurantRatingsAttribute> { }

// table: SagePettyCashAccounts
export interface SagePettyCashAccountsAttribute {
  ID:number;
  TenantID:number;
  SageAccountID?:string;
  Name?:string;
  Type?:string;
  Description?:string;
  LastUpdate:Date;
  CreateTime:Date;
}
export interface SagePettyCashAccountsInstance extends Sequelize.Instance<SagePettyCashAccountsAttribute>, SagePettyCashAccountsAttribute { }
export interface SagePettyCashAccountsModel extends Sequelize.Model<SagePettyCashAccountsInstance, SagePettyCashAccountsAttribute> { }

// table: ServiceConfig
export interface ServiceConfigAttribute {
  ID:number;
  Name:string;
  IAMKey:string;
  IAMSecret:string;
  AWSQueueURL:string;
  QueueARN?:string;
  Region:string;
  MessageCount?:number;
  Interval?:number;
  FetchDelaySeconds?:number;
  WorkerCount?:number;
  MaxWorkerCount?:number;
  Active?:number;
  RetryThreshold:number;
  QueueMonitorThreshold:number;
  Dispatcher?:number;
  LogEvent:number;
  DispatchType:string;
  DispatchValue:string;
  LastUpdate?:Date;
  Synchronous?:number;
}
export interface ServiceConfigInstance extends Sequelize.Instance<ServiceConfigAttribute>, ServiceConfigAttribute { }
export interface ServiceConfigModel extends Sequelize.Model<ServiceConfigInstance, ServiceConfigAttribute> { }

// table: Settings
export interface SettingsAttribute {
  ID:number;
  Name:string;
  Description:string;
  SettingArea?:any;
  SettingGroup:string;
  FieldType?:string;
  ListLabelValue:string;
  ValueMaxLen:number;
  DefaultValue:string;
  Hint:string;
  Required:number;
  AdminOnly:number;
  Seq:number;
  SettingGroupIcon:string;
  ShowRestaurant:number;
  ShowRetail:number;
}
export interface SettingsInstance extends Sequelize.Instance<SettingsAttribute>, SettingsAttribute { }
export interface SettingsModel extends Sequelize.Model<SettingsInstance, SettingsAttribute> { }

// table: ServiceEvent
export interface ServiceEventAttribute {
  ID:string;
  TenantID?:number;
  ServiceName:string;
  Type?:string;
  MasterDataId?:string;
  JobStarted?:Date;
  JobCompleted?:Date;
  Completed?:number;
  Success?:number;
  Status?:string;
  Error?:string;
  RetryCount?:number;
  Data?:string;
  LastUpdate?:Date;
}
export interface ServiceEventInstance extends Sequelize.Instance<ServiceEventAttribute>, ServiceEventAttribute { }
export interface ServiceEventModel extends Sequelize.Model<ServiceEventInstance, ServiceEventAttribute> { }

// table: Staff
export interface StaffAttribute {
  ID:number;
  TenantID:number;
  UserID:number;
  eTypeID:number;
  VoidPIN?:string;
  AssignDevicePIN?:string;
  DiscountPIN?:string;
  UserCode?:string;
  UserDef1?:string;
  UserDef2?:string;
  HostID?:string;
  NFCTagUID?:string;
  IsArchived:number;
  LastUpdate:Date;
}
export interface StaffInstance extends Sequelize.Instance<StaffAttribute>, StaffAttribute { }
export interface StaffModel extends Sequelize.Model<StaffInstance, StaffAttribute> { }

// table: StaffRoster
export interface StaffRosterAttribute {
  ID:number;
  TenantID:number;
  TenantShiftID:number;
  StaffID:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface StaffRosterInstance extends Sequelize.Instance<StaffRosterAttribute>, StaffRosterAttribute { }
export interface StaffRosterModel extends Sequelize.Model<StaffRosterInstance, StaffRosterAttribute> { }

// table: StaffShift
export interface StaffShiftAttribute {
  ID:number;
  HostShiftID?:string;
  TenantID:number;
  StaffID?:number;
  WaitronScheduleID:number;
  TableAreaID:number;
  DeviceID:number;
  ReplacingWaitronID?:number;
  ReplacingWaitronReason?:any;
  DateTimeSignIn?:Date;
  DateTimeSignOut?:Date;
  UserCode?:string;
  InitialCashFloat?:number;
  FinalCashFloat?:number;
  TotalTablesServed?:number;
  TotalRestaurantRevenue?:number;
  TotalTips?:number;
  CashDue?:number;
  CommEarned?:number;
  IncentiveComm?:number;
  IncentivePoints?:number;
  VoidsCount?:number;
  VoidsValue?:number;
  CashUpNote:string;
  LastUpdate:Date;
}
export interface StaffShiftInstance extends Sequelize.Instance<StaffShiftAttribute>, StaffShiftAttribute { }
export interface StaffShiftModel extends Sequelize.Model<StaffShiftInstance, StaffShiftAttribute> { }

// table: StaffTargets
export interface StaffTargetsAttribute {
  ID:number;
  TenantID:number;
  TargetID:number;
  StaffID:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface StaffTargetsInstance extends Sequelize.Instance<StaffTargetsAttribute>, StaffTargetsAttribute { }
export interface StaffTargetsModel extends Sequelize.Model<StaffTargetsInstance, StaffTargetsAttribute> { }

// table: Station
export interface StationAttribute {
  ID:number;
  TenantID:number;
  StationName?:string;
  MasterStationDeviceID?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface StationInstance extends Sequelize.Instance<StationAttribute>, StationAttribute { }
export interface StationModel extends Sequelize.Model<StationInstance, StationAttribute> { }

// table: StationDevices
export interface StationDevicesAttribute {
  ID:number;
  TenantID:number;
  StationID?:number;
  DeviceTypeID:number;
  DeviceName?:string;
  LocalPrinterName?:string;
  LinkedDeviceUID?:string;
  MaxChar?:number;
  FontBMaxChar?:number;
  OrderLineSpacing?:number;
  TOPRAddress?:string;
  ComPort?:string;
  BaudRate?:string;
  ReprintCount?:number;
  DefaultPrinter?:number;
  eStationDeviceID:number;
  StationURI?:string;
  PrintPrefix?:string;
  PrintSuffix?:string;
  PrintReceiptSuffix?:string;
  CashDrwSq?:string;
  IsArchived:number;
  LastUpdate:Date;
}
export interface StationDevicesInstance extends Sequelize.Instance<StationDevicesAttribute>, StationDevicesAttribute { }
export interface StationDevicesModel extends Sequelize.Model<StationDevicesInstance, StationDevicesAttribute> { }

// table: StockAdjustments
export interface StockAdjustmentsAttribute {
  ID:number;
  StockItem?:number;
  Type?:number;
  Delta?:any;
  PurchaseOrder?:number;
  Tab?:number;
  QuantityBefore?:any;
  QuantityAfter?:any;
  User?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface StockAdjustmentsInstance extends Sequelize.Instance<StockAdjustmentsAttribute>, StockAdjustmentsAttribute { }
export interface StockAdjustmentsModel extends Sequelize.Model<StockAdjustmentsInstance, StockAdjustmentsAttribute> { }

// table: StockItemLevels
export interface StockItemLevelsAttribute {
  StockItem?:number;
  BackOrder?:any;
  Available?:any;
  Reserved?:any;
  RunningTotal?:any;
}
export interface StockItemLevelsInstance extends Sequelize.Instance<StockItemLevelsAttribute>, StockItemLevelsAttribute { }
export interface StockItemLevelsModel extends Sequelize.Model<StockItemLevelsInstance, StockItemLevelsAttribute> { }

// table: StockItemStockTakeTypes
export interface StockItemStockTakeTypesAttribute {
  ID:number;
  TenantID?:number;
  StockItemID:number;
  StockTakeTypeID:number;
}
export interface StockItemStockTakeTypesInstance extends Sequelize.Instance<StockItemStockTakeTypesAttribute>, StockItemStockTakeTypesAttribute { }
export interface StockItemStockTakeTypesModel extends Sequelize.Model<StockItemStockTakeTypesInstance, StockItemStockTakeTypesAttribute> { }

// table: StockItemSupplierQuantities
export interface StockItemSupplierQuantitiesAttribute {
  ID:number;
  StockItemSupplierID?:number;
  Description?:string;
  Barcode?:string;
  PLU?:string;
  StockItemUnitID?:number;
  EconomicOrderQuantity?:string;
  StockOrderUnit?:string;
  StockOrderUnitID?:number;
  ConversionFactor?:any;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface StockItemSupplierQuantitiesInstance extends Sequelize.Instance<StockItemSupplierQuantitiesAttribute>, StockItemSupplierQuantitiesAttribute { }
export interface StockItemSupplierQuantitiesModel extends Sequelize.Model<StockItemSupplierQuantitiesInstance, StockItemSupplierQuantitiesAttribute> { }

// table: StockItemSuppliers
export interface StockItemSuppliersAttribute {
  ID:number;
  StockItem?:number;
  Supplier?:number;
  Description?:string;
  Rank?:number;
  StockUnitConversionID?:number;
  BarCode?:string;
  PLU?:string;
  PreferedSupplier?:number;
  EconomicOrderQuantity?:any;
  EconomicOrderStockItemUnitID?:number;
  StockOrderUnit?:string;
  ConversionFactor?:any;
  LastCostPerBaseUnit?:any;
  AverageReplenishmentCycleDays?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface StockItemSuppliersInstance extends Sequelize.Instance<StockItemSuppliersAttribute>, StockItemSuppliersAttribute { }
export interface StockItemSuppliersModel extends Sequelize.Model<StockItemSuppliersInstance, StockItemSuppliersAttribute> { }

// table: StockItemUnits
export interface StockItemUnitsAttribute {
  ID:number;
  TenantID?:number;
  StockItemID:number;
  StockUnitID:number;
  Barcode?:string;
  LastUpdate:Date;
}
export interface StockItemUnitsInstance extends Sequelize.Instance<StockItemUnitsAttribute>, StockItemUnitsAttribute { }
export interface StockItemUnitsModel extends Sequelize.Model<StockItemUnitsInstance, StockItemUnitsAttribute> { }

// table: StockItems
export interface StockItemsAttribute {
  ID:number;
  TenantID?:number;
  MenuItem:number;
  Name?:string;
  Type?:number;
  LastCostPerBaseUnit?:any;
  AverageCostPerBaseUnit?:any;
  StockTakePeriod?:string;
  IsRandomStockTake?:number;
  AllowNegativeStockLevel?:number;
  ReorderLevel?:number;
  AlertLevel?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
  RunningStockValue?:any;
  RunningStockQuantity?:any;
  BaseUnit?:number;
}
export interface StockItemsInstance extends Sequelize.Instance<StockItemsAttribute>, StockItemsAttribute { }
export interface StockItemsModel extends Sequelize.Model<StockItemsInstance, StockItemsAttribute> { }

// table: StockLevels
export interface StockLevelsAttribute {
  ID:number;
  TenantID:number;
  MenuItemID:number;
  LastUpdateSource:string;
  LastUpdate?:Date;
  CreateTime?:Date;
  Available?:any;
  BackOrder?:any;
  Reserved?:any;
  RunningTotal?:any;
}
export interface StockLevelsInstance extends Sequelize.Instance<StockLevelsAttribute>, StockLevelsAttribute { }
export interface StockLevelsModel extends Sequelize.Model<StockLevelsInstance, StockLevelsAttribute> { }

// table: StockTakeTypes
export interface StockTakeTypesAttribute {
  ID:number;
  TenantID?:number;
  Name?:string;
  Description?:string;
  HostID?:string;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface StockTakeTypesInstance extends Sequelize.Instance<StockTakeTypesAttribute>, StockTakeTypesAttribute { }
export interface StockTakeTypesModel extends Sequelize.Model<StockTakeTypesInstance, StockTakeTypesAttribute> { }

// table: StockUnitConversions
export interface StockUnitConversionsAttribute {
  ID:number;
  FromUnitID:number;
  ToUnitID:number;
  ConversionFactor?:any;
  IsArchived:number;
  LastUpdate:Date;
}
export interface StockUnitConversionsInstance extends Sequelize.Instance<StockUnitConversionsAttribute>, StockUnitConversionsAttribute { }
export interface StockUnitConversionsModel extends Sequelize.Model<StockUnitConversionsInstance, StockUnitConversionsAttribute> { }

// table: StockUnits
export interface StockUnitsAttribute {
  ID:number;
  Unit:string;
  IsArchived:number;
  LastUpdate:Date;
}
export interface StockUnitsInstance extends Sequelize.Instance<StockUnitsAttribute>, StockUnitsAttribute { }
export interface StockUnitsModel extends Sequelize.Model<StockUnitsInstance, StockUnitsAttribute> { }

// table: SyncClashItems
export interface SyncClashItemsAttribute {
  ID:number;
  MenueItemId?:number;
  TenantID?:number;
  SyncItemID?:number;
  SageOneItem?:string;
  update_time?:Date;
}
export interface SyncClashItemsInstance extends Sequelize.Instance<SyncClashItemsAttribute>, SyncClashItemsAttribute { }
export interface SyncClashItemsModel extends Sequelize.Model<SyncClashItemsInstance, SyncClashItemsAttribute> { }

// table: SyncItems
export interface SyncItemsAttribute {
  ID:number;
  TenantID:number;
  MenuItemID:number;
  ItemType:string;
  MenuID:number;
  ParentID?:string;
  ItemID?:string;
  ShortName?:string;
  PrintSlipName?:string;
  CategoryID?:number;
  StockItemID?:number;
  Seq?:number;
  IsModOnly?:number;
  IsIngredOnly?:number;
  BrandItemID?:number;
  ShortcutID?:number;
  Cost?:number;
  Price?:number;
  PriceVar?:string;
  TaxGroupID?:number;
  IsTaxable:number;
  AllowLocalPrice:number;
  ReqMgrSignOff:number;
  Notes?:string;
  PrepTime?:any;
  ServeFrom?:any;
  ServeUntil?:any;
  SrvSu:number;
  SrvMo:number;
  SrvTu:number;
  SrvWe:number;
  SrvTh:number;
  SrvFr:number;
  SrvSa:number;
  HostPLU?:string;
  HostedAccountID?:string;
  BarCode?:string;
  HostSalesDept?:string;
  ImageMediaID?:number;
  PromoImageMediaID?:number;
  HaveImages:number;
  HasPromoImage:number;
  PromoServiceGroupID?:number;
  WaiterOnly:number;
  AvailableToGo:number;
  AvailableTA:number;
  AutoAcceptToGo:number;
  ReturnPeriodDays?:number;
  WarrantyPeriodMnths?:number;
  WarrantyNote?:string;
  IsArchived:number;
  LastUpdate:Date;
}
export interface SyncItemsInstance extends Sequelize.Instance<SyncItemsAttribute>, SyncItemsAttribute { }
export interface SyncItemsModel extends Sequelize.Model<SyncItemsInstance, SyncItemsAttribute> { }

// table: TabOrderStatus
export interface TabOrderStatusAttribute {
  ID:number;
  TenantID:number;
  TabID?:number;
  Note?:string;
  OrderStatusID:number;
  LastUpdate:Date;
}
export interface TabOrderStatusInstance extends Sequelize.Instance<TabOrderStatusAttribute>, TabOrderStatusAttribute { }
export interface TabOrderStatusModel extends Sequelize.Model<TabOrderStatusInstance, TabOrderStatusAttribute> { }

// table: TabPaymentGroups
export interface TabPaymentGroupsAttribute {
  ID:number;
  TenantID:number;
  TabID:number;
  PaymentInfoID:number;
  PaymentGroupNote?:string;
  IsArchived:number;
  LastUpdate:Date;
}
export interface TabPaymentGroupsInstance extends Sequelize.Instance<TabPaymentGroupsAttribute>, TabPaymentGroupsAttribute { }
export interface TabPaymentGroupsModel extends Sequelize.Model<TabPaymentGroupsInstance, TabPaymentGroupsAttribute> { }

// table: TableAreas
export interface TableAreasAttribute {
  ID:number;
  TenantID:number;
  Description?:string;
  IsArchived:number;
  LastUpdate:Date;
}
export interface TableAreasInstance extends Sequelize.Instance<TableAreasAttribute>, TableAreasAttribute { }
export interface TableAreasModel extends Sequelize.Model<TableAreasInstance, TableAreasAttribute> { }

// table: TableHelp
export interface TableHelpAttribute {
  ID:number;
  TableName:string;
  TableEnum:string;
  LangID?:number;
  ToolTip?:string;
  HelpText?:string;
  IsArchived:number;
  LastUpdate:Date;
}
export interface TableHelpInstance extends Sequelize.Instance<TableHelpAttribute>, TableHelpAttribute { }
export interface TableHelpModel extends Sequelize.Model<TableHelpInstance, TableHelpAttribute> { }

// table: Tables
export interface TablesAttribute {
  ID:number;
  TenantID:number;
  Description?:string;
  DefaultSize?:number;
  TableAreaID:number;
  HostTableID?:string;
  IsArchived:number;
  LastUpdate:Date;
  name?:string;
}
export interface TablesInstance extends Sequelize.Instance<TablesAttribute>, TablesAttribute { }
export interface TablesModel extends Sequelize.Model<TablesInstance, TablesAttribute> { }

// table: Tabs
export interface TabsAttribute {
  ID:number;
  StaffShift?:number;
  Origin?:string;
  TenantID:number;
  WaitronID?:number;
  WaitronServiceID?:number;
  CustomerID?:number;
  UserID?:number;
  LoyaltyProgRef?:string;
  TableID?:number;
  TabName?:string;
  Party?:string;
  GuestCount?:number;
  OpenTime?:Date;
  CloseTime?:Date;
  Voided?:number;
  NetOrderTotal?:number;
  Gratuity?:number;
  ChangeGiven?:number;
  TaxTotal?:number;
  InvoiceTotal?:number;
  CostTotal?:number;
  InvoiceNo?:string;
  HostProcessed?:number;
  HostTabID?:string;
  HostWaiterID?:string;
  HostWaiterName?:string;
  Note?:string;
  SubscribeType?:string;
  SubscribeID?:string;
  OrderStatus:number;
  IsFutureOrder:number;
  SaleType?:string;
  UserCode?:string;
  IsArchived:number;
  ArchivedDateTime?:Date;
  LastUpdate:Date;
  percentage_discount?:number;
  reportDate?:Date;
  CashOnCollection?:number;
  PrepTime?:number;
  EstimatedCompletionTime?:Date;
  NumPrepTimeUpdates?:number;
  PaymentConfirmationReceived?:number;
  ServingTime?:Date;
  TransportTypeID?:number;
}
export interface TabsInstance extends Sequelize.Instance<TabsAttribute>, TabsAttribute { }
export interface TabsModel extends Sequelize.Model<TabsInstance, TabsAttribute> { }

// table: Tag
export interface TagAttribute {
  ID:number;
  TenantID:number;
  TagDescription?:string;
  TagDescriptionStringID?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface TagInstance extends Sequelize.Instance<TagAttribute>, TagAttribute { }
export interface TagModel extends Sequelize.Model<TagInstance, TagAttribute> { }

// table: TagAttributes
export interface TagAttributesAttribute {
  ID:number;
  TenantID:number;
  TagID:number;
  AttributeID:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface TagAttributesInstance extends Sequelize.Instance<TagAttributesAttribute>, TagAttributesAttribute { }
export interface TagAttributesModel extends Sequelize.Model<TagAttributesInstance, TagAttributesAttribute> { }

// table: TaxGroups
export interface TaxGroupsAttribute {
  ID:number;
  TenantID?:number;
  TaxGroupName:string;
  DefaultGroup:number;
  LastUpdate:Date;
  IsArchived:number;
}
export interface TaxGroupsInstance extends Sequelize.Instance<TaxGroupsAttribute>, TaxGroupsAttribute { }
export interface TaxGroupsModel extends Sequelize.Model<TaxGroupsInstance, TaxGroupsAttribute> { }

// table: TaxLines
export interface TaxLinesAttribute {
  ID:number;
  TenantID:number;
  TabID:number;
  TaxTotal?:number;
  TaxID:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface TaxLinesInstance extends Sequelize.Instance<TaxLinesAttribute>, TaxLinesAttribute { }
export interface TaxLinesModel extends Sequelize.Model<TaxLinesInstance, TaxLinesAttribute> { }

// table: TaxRegimes
export interface TaxRegimesAttribute {
  ID:number;
  Regime?:string;
  LevyOnNonTaxables?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface TaxRegimesInstance extends Sequelize.Instance<TaxRegimesAttribute>, TaxRegimesAttribute { }
export interface TaxRegimesModel extends Sequelize.Model<TaxRegimesInstance, TaxRegimesAttribute> { }

// table: TaxTypeLinks
export interface TaxTypeLinksAttribute {
  ID:number;
  TenantID:number;
  TaxTypeID:number;
  TaxGroupID:number;
  LastUpdate:Date;
  IsArchived:number;
}
export interface TaxTypeLinksInstance extends Sequelize.Instance<TaxTypeLinksAttribute>, TaxTypeLinksAttribute { }
export interface TaxTypeLinksModel extends Sequelize.Model<TaxTypeLinksInstance, TaxTypeLinksAttribute> { }

// table: TaxTypes
export interface TaxTypesAttribute {
  ID:number;
  TenantID:number;
  TaxRegime?:number;
  TaxType?:string;
  Seq?:number;
  InclusiveTax?:number;
  TaxRate?:number;
  TaxReference?:string;
  HostTaxID?:string;
  TaxStartDate?:Date;
  TaxEndDate?:Date;
  IsArchived:number;
  LastUpdate:Date;
}
export interface TaxTypesInstance extends Sequelize.Instance<TaxTypesAttribute>, TaxTypesAttribute { }
export interface TaxTypesModel extends Sequelize.Model<TaxTypesInstance, TaxTypesAttribute> { }

// table: TenantAccountSystem
export interface TenantAccountSystemAttribute {
  ID:number;
  AccountSysID?:number;
  TenantID?:number;
  InitInvoiceNo:string;
  AppID?:string;
  AppSecret?:string;
  AppApiKey?:string;
  SystemCompanyID?:string;
  TaxTypeID?:string;
  BankID?:string;
  CashCustomerID?:string;
  GratuityAccountID?:string;
  PettyAccountID?:string;
  InventoryMovementAssets?:string;
  InventoryMovementCostOfSale?:string;
  InventoryMovementAssetsAdjustmentAccount?:string;
  TrackInventoryQuantity:number;
  InventoryCostType?:string;
  ItemReferenceField?:string;
  AnalysisCategory1?:string;
  AnalysisCategory2?:string;
  AnalysisCategory3?:string;
  IsArchived:number;
  AccountingOrganisationName?:string;
  Active:number;
  Syncing?:number;
  SyncProgress?:string;
}
export interface TenantAccountSystemInstance extends Sequelize.Instance<TenantAccountSystemAttribute>, TenantAccountSystemAttribute { }
export interface TenantAccountSystemModel extends Sequelize.Model<TenantAccountSystemInstance, TenantAccountSystemAttribute> { }

// table: TenantAndMenuPublishVersions
export interface TenantAndMenuPublishVersionsAttribute {
  TenantID:number;
  MenuID:number;
  Version:string;
  TenantAndMenuFileKey:string;
  BucketName:string;
  TenantAndMenuS3Url?:string;
  CreateTime?:Date;
}
export interface TenantAndMenuPublishVersionsInstance extends Sequelize.Instance<TenantAndMenuPublishVersionsAttribute>, TenantAndMenuPublishVersionsAttribute { }
export interface TenantAndMenuPublishVersionsModel extends Sequelize.Model<TenantAndMenuPublishVersionsInstance, TenantAndMenuPublishVersionsAttribute> { }

// table: TenantDiffs
export interface TenantDiffsAttribute {
  ID:number;
  TenantID:number;
  CurrentVersion:string;
  LatestVersion:string;
  Diff?:string;
  DiffKey?:string;
  CreateTime?:Date;
}
export interface TenantDiffsInstance extends Sequelize.Instance<TenantDiffsAttribute>, TenantDiffsAttribute { }
export interface TenantDiffsModel extends Sequelize.Model<TenantDiffsInstance, TenantDiffsAttribute> { }

// table: TenantLanguages
export interface TenantLanguagesAttribute {
  ID:number;
  TenantID:number;
  LangID:number;
  IsArchived?:number;
  LastUpdate:Date;
}
export interface TenantLanguagesInstance extends Sequelize.Instance<TenantLanguagesAttribute>, TenantLanguagesAttribute { }
export interface TenantLanguagesModel extends Sequelize.Model<TenantLanguagesInstance, TenantLanguagesAttribute> { }

// table: TenantLocationSettings
export interface TenantLocationSettingsAttribute {
  ID:number;
  TenantID:number;
  TenantLocationID:number;
  SettingID:number;
  SettingValue:string;
}
export interface TenantLocationSettingsInstance extends Sequelize.Instance<TenantLocationSettingsAttribute>, TenantLocationSettingsAttribute { }
export interface TenantLocationSettingsModel extends Sequelize.Model<TenantLocationSettingsInstance, TenantLocationSettingsAttribute> { }

// table: TenantLocations
export interface TenantLocationsAttribute {
  ID:number;
  LocationName:string;
  TenantID:number;
  AddressTableID?:number;
  IsArchived:number;
}
export interface TenantLocationsInstance extends Sequelize.Instance<TenantLocationsAttribute>, TenantLocationsAttribute { }
export interface TenantLocationsModel extends Sequelize.Model<TenantLocationsInstance, TenantLocationsAttribute> { }

// table: TenantLoyaltyPrograms
export interface TenantLoyaltyProgramsAttribute {
  ID:number;
  TenantID:number;
  ProgramID:number;
  LoyaltyNr:string;
  IsArchived?:number;
  LastUpdate:Date;
}
export interface TenantLoyaltyProgramsInstance extends Sequelize.Instance<TenantLoyaltyProgramsAttribute>, TenantLoyaltyProgramsAttribute { }
export interface TenantLoyaltyProgramsModel extends Sequelize.Model<TenantLoyaltyProgramsInstance, TenantLoyaltyProgramsAttribute> { }

// table: TenantOperatingTimes
export interface TenantOperatingTimesAttribute {
  ID:number;
  TenantID:number;
  WeekDay:number;
  StartTime?:any;
  EndTime?:any;
  OpArea:string;
  IsArchived:number;
}
export interface TenantOperatingTimesInstance extends Sequelize.Instance<TenantOperatingTimesAttribute>, TenantOperatingTimesAttribute { }
export interface TenantOperatingTimesModel extends Sequelize.Model<TenantOperatingTimesInstance, TenantOperatingTimesAttribute> { }

// table: TenantPaymentType
export interface TenantPaymentTypeAttribute {
  ID:number;
  TenantID:number;
  PaymentTypeID:number;
  DefaultPayment:number;
  DisplayName:string;
  PaymentUser?:string;
  PaymentPass?:string;
  PaymentNotifyKey?:string;
  DecryptionKey?:string;
  LevyType:any;
  LevyValue:number;
  Seq?:number;
  QRCode?:string;
  ChangeGiven:number;
  OpenCashDrawer:number;
  HostAllocate:number;
  AlterTypeAllowed:number;
  HostBankID?:string;
  ReferenceRequired:number;
  UserEnabled:number;
  EnabledPOS:number;
  EnabledToGo:number;
  EnabledApps:number;
  EnabledTA:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface TenantPaymentTypeInstance extends Sequelize.Instance<TenantPaymentTypeAttribute>, TenantPaymentTypeAttribute { }
export interface TenantPaymentTypeModel extends Sequelize.Model<TenantPaymentTypeInstance, TenantPaymentTypeAttribute> { }

// table: TenantPerpShifts
export interface TenantPerpShiftsAttribute {
  ID:number;
  TenantID:number;
  eDayNameID:number;
  ShiftName:string;
  StartTime:any;
  EndTime:any;
  IsArchived:number;
  LastUpdate:Date;
}
export interface TenantPerpShiftsInstance extends Sequelize.Instance<TenantPerpShiftsAttribute>, TenantPerpShiftsAttribute { }
export interface TenantPerpShiftsModel extends Sequelize.Model<TenantPerpShiftsInstance, TenantPerpShiftsAttribute> { }

// table: TenantPublishVersions
export interface TenantPublishVersionsAttribute {
  TenantID:number;
  Version:string;
  TenantFileKey:string;
  PatchFileKey?:string;
  BucketName:string;
  TenantObject:string;
  PatchObject?:string;
  PatchS3Url?:string;
  TenantS3Url?:string;
  CreateTime?:Date;
}
export interface TenantPublishVersionsInstance extends Sequelize.Instance<TenantPublishVersionsAttribute>, TenantPublishVersionsAttribute { }
export interface TenantPublishVersionsModel extends Sequelize.Model<TenantPublishVersionsInstance, TenantPublishVersionsAttribute> { }

// table: TenantServiceConfig
export interface TenantServiceConfigAttribute {
  ID:number;
  TenantID:number;
  Name:string;
  Type?:string;
  IAMKey:string;
  IAMSecret:string;
  AWSQueueURL:string;
  QueueARN?:string;
  Region:string;
  MessageCount?:number;
  Interval?:number;
  FetchDelaySeconds?:number;
  WorkerCount?:number;
  MaxWorkerCount?:number;
  Active?:number;
  RetryThreshold:number;
  QueueMonitorThreshold:number;
  LogEvent:number;
  LastUpdate?:Date;
  Synchronous?:number;
}
export interface TenantServiceConfigInstance extends Sequelize.Instance<TenantServiceConfigAttribute>, TenantServiceConfigAttribute { }
export interface TenantServiceConfigModel extends Sequelize.Model<TenantServiceConfigInstance, TenantServiceConfigAttribute> { }

// table: TenantServices
export interface TenantServicesAttribute {
  TenantId:number;
  ServiceId:number;
  LastUpdate?:Date;
}
export interface TenantServicesInstance extends Sequelize.Instance<TenantServicesAttribute>, TenantServicesAttribute { }
export interface TenantServicesModel extends Sequelize.Model<TenantServicesInstance, TenantServicesAttribute> { }

// table: TenantSettings
export interface TenantSettingsAttribute {
  ID:number;
  TenantID:number;
  SettingID:number;
  SettingValue:string;
}
export interface TenantSettingsInstance extends Sequelize.Instance<TenantSettingsAttribute>, TenantSettingsAttribute { }
export interface TenantSettingsModel extends Sequelize.Model<TenantSettingsInstance, TenantSettingsAttribute> { }

// table: TenantSuppliers
export interface TenantSuppliersAttribute {
  ID:number;
  TenantID?:number;
  GlobalSupplierID?:number;
  Name?:string;
  ContactPerson?:string;
  Phone?:string;
  Email?:string;
  Website?:string;
  TermsAndConditions?:string;
  CreditLimit?:any;
  AccountingSystemLink?:string;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface TenantSuppliersInstance extends Sequelize.Instance<TenantSuppliersAttribute>, TenantSuppliersAttribute { }
export interface TenantSuppliersModel extends Sequelize.Model<TenantSuppliersInstance, TenantSuppliersAttribute> { }

// table: TenantTOPIAddresses
export interface TenantTOPIAddressesAttribute {
  ID:number;
  TenantID:number;
  TopiID:string;
  Address:string;
  Description?:string;
  LastUpdate:Date;
}
export interface TenantTOPIAddressesInstance extends Sequelize.Instance<TenantTOPIAddressesAttribute>, TenantTOPIAddressesAttribute { }
export interface TenantTOPIAddressesModel extends Sequelize.Model<TenantTOPIAddressesInstance, TenantTOPIAddressesAttribute> { }

// table: TenantTagLinks
export interface TenantTagLinksAttribute {
  ID:number;
  TenantID:number;
  TagID:number;
  IsArchived?:number;
  LastUpdate:Date;
}
export interface TenantTagLinksInstance extends Sequelize.Instance<TenantTagLinksAttribute>, TenantTagLinksAttribute { }
export interface TenantTagLinksModel extends Sequelize.Model<TenantTagLinksInstance, TenantTagLinksAttribute> { }

// table: TenantTags
export interface TenantTagsAttribute {
  ID:number;
  TagName:string;
  IsArchived?:number;
  LastUpdate:Date;
}
export interface TenantTagsInstance extends Sequelize.Instance<TenantTagsAttribute>, TenantTagsAttribute { }
export interface TenantTagsModel extends Sequelize.Model<TenantTagsInstance, TenantTagsAttribute> { }

// table: TenantTargets
export interface TenantTargetsAttribute {
  ID:number;
  TenantID:number;
  Name:string;
  ePeriodID:number;
  StartDate?:Date;
  EndDate?:Date;
  TargetMoney?:number;
  BonusMoney?:number;
  BonusPercent?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface TenantTargetsInstance extends Sequelize.Instance<TenantTargetsAttribute>, TenantTargetsAttribute { }
export interface TenantTargetsModel extends Sequelize.Model<TenantTargetsInstance, TenantTargetsAttribute> { }

// table: TenantTemplates
export interface TenantTemplatesAttribute {
  ID:number;
  TenantID:number;
  Name:string;
  Body?:string;
  CreateTime?:Date;
  UpdateTime?:Date;
}
export interface TenantTemplatesInstance extends Sequelize.Instance<TenantTemplatesAttribute>, TenantTemplatesAttribute { }
export interface TenantTemplatesModel extends Sequelize.Model<TenantTemplatesInstance, TenantTemplatesAttribute> { }

// table: Tenants
export interface TenantsAttribute {
  ID:number;
  TenantName:string;
  TenantTypeID:number;
  BusinessTypeID:number;
  ProdTypeID:number;
  Address?:string;
  UserID:number;
  CountryID?:number;
  CurrencyID?:number;
  Base64Logo?:string;
  LangID?:number;
  MenuTenantID?:number;
  ManagerTenantID?:number;
  POSVendorID?:number;
  UseSupersetMenu:number;
  RefFranchiseeID?:number;
  TenantAuthCode?:string;
  TimeZone?:number;
  LocaleID?:string;
  LocLong:number;
  LocLat:number;
  ShowTimes:number;
  ShowPrepTimes:number;
  ShowDeliveryTimes:number;
  ProcessItemRatings:number;
  ProcessRestRatings:number;
  ShowItemRatings:number;
  ShowItemRatingComments:number;
  ShowRestRatings:number;
  ShowRestRatingComments:number;
  ShowThumbs:number;
  ShowCurrencySymbol:number;
  AllowLocalPrices:number;
  AllowSS:number;
  PublishSS:number;
  AllowVoid:number;
  ApplyVoidPINReq:number;
  ApplyDiscountPINReq:number;
  StaffPinSignIn:number;
  ShowShiftStats:number;
  OrderClosingMethod:number;
  ShowOrdConfirm:number;
  ShowQuickCashBtn:number;
  ShowTotalsInTab:number;
  MarkOrdDeliv:number;
  UseTallOrderBackupServer:number;
  DebugMode:number;
  ResumeLastTab:number;
  PrintServerURL?:string;
  POSLicense?:string;
  POSDeviceCount?:number;
  POSPostingURL?:string;
  POSIntegratorURL?:string;
  POSIntegratorParameters?:string;
  POSImportMenu:number;
  POSInactiveTimeout:number;
  POSTableNoReq:number;
  AddItemPromptNewOrder:number;
  CompletePaymentPromptPlaceOrder:number;
  GuestCountReq:number;
  UseGratuityMechanism:number;
  ContactWork?:string;
  ReceiptHeader?:string;
  ReceiptFooter?:string;
  MPOSPaymentConfig?:string;
  GTPaymentConfig?:string;
  ProcessGuaranteeTab:number;
  FromEmailAddress?:string;
  ServerURL?:string;
  MoneyFormat?:string;
  DateFormat?:string;
  FirstDayOfMonth:number;
  FirstDayOfWeek:number;
  DefaultWaitronCommPercent?:number;
  ExcludeTaxesFromCommCalc:number;
  UseWaitronScheduling:number;
  ThemeBUrl:string;
  ThemeBTile:number;
  ThemeBColor?:string;
  ThemeFColor?:string;
  HeadBColor?:string;
  HeadFColor?:string;
  ItemBColor?:string;
  ItemFColor?:string;
  WaiterTerm?:string;
  MenuTerm?:string;
  MenusTerm?:string;
  TabTerm?:string;
  TabsTerm?:string;
  GratuityTerm:string;
  GratuityPercentages:string;
  UserDefLabel1?:string;
  UserDefLabel2?:string;
  ListOrGridView:number;
  ReportCatWidth:number;
  UseGuestNumbering:number;
  FreeFormTblNmbrs:number;
  AllowDirectCloudSignIn:number;
  AllowCloseTabAmountChange:number;
  AllowPaymentDelete?:number;
  RestrictTableNumXWaiters:number;
  ShowiAppNotes:number;
  TwitterUrl?:string;
  FacebookUrl?:string;
  InvoicePrefix:string;
  InvNoMinLength:number;
  StorePayments:any;
  TGMenuAvail:number;
  ToGoFeaturedItemId?:number;
  TGShowThumbs:number;
  TGWaiterID?:number;
  TGMUDelivPcnt?:number;
  TGWSUris?:string;
  TGSubdomain?:string;
  TGFailoverEmailAddr?:string;
  TGLandingContent?:string;
  TGTsAndCs?:string;
  TGAbout?:string;
  TGRestaurantTerm?:string;
  TGRestaurantsTerm?:string;
  TGOrderTerm?:string;
  TGTabTerm?:string;
  TGReqPreAuthSSOrders:number;
  TGAllowSSPayments:number;
  TGStartTableNumber?:number;
  TGMaxTableNumber?:number;
  TGStartTimeMon?:any;
  TGEndTimeMon?:any;
  TGStartTimeTue?:any;
  TGEndTimeTue?:any;
  TGStartTimeWed?:any;
  TGEndTimeWed?:any;
  TGStartTimeThu?:any;
  TGEndTimeThu?:any;
  TGStartTimeFri?:any;
  TGEndTimeFri?:any;
  TGStartTimeSat?:any;
  TGEndTimeSat?:any;
  TGStartTimeSun?:any;
  TGEndTimeSun?:any;
  LastActiveMenuPub?:Date;
  LastPOSSync?:Date;
  DemoMode:number;
  IsArchived:number;
  LastUpdate:Date;
  TogoLogoURL?:string;
  AllowPOSAutoUpdates?:number;
  AllowMasterStationOverride?:number;
}
export interface TenantsInstance extends Sequelize.Instance<TenantsAttribute>, TenantsAttribute { }
export interface TenantsModel extends Sequelize.Model<TenantsInstance, TenantsAttribute> { }

// table: ToGoFeaturedItems
export interface ToGoFeaturedItemsAttribute {
  ID:number;
  TenantID:number;
  MenuItemID?:number;
  MediaID?:number;
  Seq:number;
  PriceText?:string;
  Title?:string;
  SubTitle?:string;
  Description?:string;
  Active?:number;
  StartDate?:Date;
  EndDate?:Date;
  LastUpdate:Date;
  CreateTime:Date;
}
export interface ToGoFeaturedItemsInstance extends Sequelize.Instance<ToGoFeaturedItemsAttribute>, ToGoFeaturedItemsAttribute { }
export interface ToGoFeaturedItemsModel extends Sequelize.Model<ToGoFeaturedItemsInstance, ToGoFeaturedItemsAttribute> { }

// table: TogoUserFeedback
export interface TogoUserFeedbackAttribute {
  Id:number;
  FirstName?:string;
  LastName?:string;
  MobileNumber?:string;
  Feedback?:string;
  UserId?:number;
  ContactPermission?:number;
  CreateTime?:Date;
  UpdateTime?:Date;
}
export interface TogoUserFeedbackInstance extends Sequelize.Instance<TogoUserFeedbackAttribute>, TogoUserFeedbackAttribute { }
export interface TogoUserFeedbackModel extends Sequelize.Model<TogoUserFeedbackInstance, TogoUserFeedbackAttribute> { }

// table: TopiLog
export interface TopiLogAttribute {
  ID:number;
  TenantID:number;
  DeviceName:string;
  LastUpdate:Date;
}
export interface TopiLogInstance extends Sequelize.Instance<TopiLogAttribute>, TopiLogAttribute { }
export interface TopiLogModel extends Sequelize.Model<TopiLogInstance, TopiLogAttribute> { }

// table: Translators
export interface TranslatorsAttribute {
  ID:number;
  TenantID:number;
  UserID?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface TranslatorsInstance extends Sequelize.Instance<TranslatorsAttribute>, TranslatorsAttribute { }
export interface TranslatorsModel extends Sequelize.Model<TranslatorsInstance, TranslatorsAttribute> { }

// table: User
export interface UserAttribute {
  ID:number;
  FirstName?:string;
  LastName?:string;
  Email?:string;
  pWord?:string;
  pWordResetToken?:string;
  Mobile?:string;
  PhoneNo?:string;
  Country?:number;
  City?:string;
  UserPrefLanguage?:number;
  MarketingPref?:number;
  DontAskInfo?:number;
  Gender?:number;
  YearOfBirth?:number;
  eEmailVerifyID?:number;
  AuthKey:string;
  IsArchived:number;
  IsAdmin:number;
  AdminLevel:number;
  IsParAdmin:number;
  LastUpdate:Date;
  UserFavItem?:string;
  PushId?:string;
  EndpointArn?:string;
}
export interface UserInstance extends Sequelize.Instance<UserAttribute>, UserAttribute { }
export interface UserModel extends Sequelize.Model<UserInstance, UserAttribute> { }

// table: TransportType
export interface TransportTypeAttribute {
  ID:number;
  Description?:string;
  CreateTime?:Date;
  UpdateTime?:Date;
}
export interface TransportTypeInstance extends Sequelize.Instance<TransportTypeAttribute>, TransportTypeAttribute { }
export interface TransportTypeModel extends Sequelize.Model<TransportTypeInstance, TransportTypeAttribute> { }

// table: UserDevices
export interface UserDevicesAttribute {
  ID:number;
  UserID?:number;
  DeviceID?:number;
  DeviceModelID?:number;
  DateFirstUse?:Date;
  DateLastUse?:Date;
  IsArchived:number;
  LastUpdate:Date;
}
export interface UserDevicesInstance extends Sequelize.Instance<UserDevicesAttribute>, UserDevicesAttribute { }
export interface UserDevicesModel extends Sequelize.Model<UserDevicesInstance, UserDevicesAttribute> { }

// table: UserPaymentMethods
export interface UserPaymentMethodsAttribute {
  ID:number;
  UserID?:number;
  PmtTypeID?:number;
  PmtNickName?:string;
  PmtSecureToken?:string;
  PmtLastDateTime?:Date;
  PmtLast?:number;
}
export interface UserPaymentMethodsInstance extends Sequelize.Instance<UserPaymentMethodsAttribute>, UserPaymentMethodsAttribute { }
export interface UserPaymentMethodsModel extends Sequelize.Model<UserPaymentMethodsInstance, UserPaymentMethodsAttribute> { }

// table: UserLoyaltyPrograms
export interface UserLoyaltyProgramsAttribute {
  ID:number;
  UserID?:number;
  ProgramID?:number;
  LoyaltyNumber?:string;
  DateLastUsed?:Date;
  IsArchived:number;
}
export interface UserLoyaltyProgramsInstance extends Sequelize.Instance<UserLoyaltyProgramsAttribute>, UserLoyaltyProgramsAttribute { }
export interface UserLoyaltyProgramsModel extends Sequelize.Model<UserLoyaltyProgramsInstance, UserLoyaltyProgramsAttribute> { }

// table: UserPreferences
export interface UserPreferencesAttribute {
  ID:number;
  UserID?:number;
  TenantID?:number;
  PrefName:string;
  PrefValue:string;
}
export interface UserPreferencesInstance extends Sequelize.Instance<UserPreferencesAttribute>, UserPreferencesAttribute { }
export interface UserPreferencesModel extends Sequelize.Model<UserPreferencesInstance, UserPreferencesAttribute> { }

// table: WaitronDevice
export interface WaitronDeviceAttribute {
  ID:number;
  TenantID?:number;
  WaitronID?:number;
  DeviceID?:number;
  DateAssigned?:Date;
  DateReturned?:Date;
  IsArchived:number;
  LastUpdate:Date;
}
export interface WaitronDeviceInstance extends Sequelize.Instance<WaitronDeviceAttribute>, WaitronDeviceAttribute { }
export interface WaitronDeviceModel extends Sequelize.Model<WaitronDeviceInstance, WaitronDeviceAttribute> { }

// table: WaitronRatings
export interface WaitronRatingsAttribute {
  ID:number;
  TenantID:number;
  TabID:number;
  WaitronID:number;
  RatingDateTime?:Date;
  AttitudeRatng?:number;
  SpeedRating?:number;
  OverallRating?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface WaitronRatingsInstance extends Sequelize.Instance<WaitronRatingsAttribute>, WaitronRatingsAttribute { }
export interface WaitronRatingsModel extends Sequelize.Model<WaitronRatingsInstance, WaitronRatingsAttribute> { }

// table: WaitronService
export interface WaitronServiceAttribute {
  ID:number;
  TenantID:number;
  WaitronID:number;
  SignIn?:Date;
  SignOut?:Date;
  AreaID?:number;
  DeviceID:number;
  ShiftID?:number;
  HostShiftID?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface WaitronServiceInstance extends Sequelize.Instance<WaitronServiceAttribute>, WaitronServiceAttribute { }
export interface WaitronServiceModel extends Sequelize.Model<WaitronServiceInstance, WaitronServiceAttribute> { }

// table: WaitronShiftMonies
export interface WaitronShiftMoniesAttribute {
  ID:number;
  TenantID:number;
  WaitronShiftID:number;
  TenantPaymentTypeID:number;
  Amount?:number;
  LastUpdate:Date;
}
export interface WaitronShiftMoniesInstance extends Sequelize.Instance<WaitronShiftMoniesAttribute>, WaitronShiftMoniesAttribute { }
export interface WaitronShiftMoniesModel extends Sequelize.Model<WaitronShiftMoniesInstance, WaitronShiftMoniesAttribute> { }

// table: WaitronTables
export interface WaitronTablesAttribute {
  ID:number;
  TenantID:number;
  WaitronID:number;
  TableID?:number;
  IsArchived:number;
  LastUpdate:Date;
}
export interface WaitronTablesInstance extends Sequelize.Instance<WaitronTablesAttribute>, WaitronTablesAttribute { }
export interface WaitronTablesModel extends Sequelize.Model<WaitronTablesInstance, WaitronTablesAttribute> { }

// table: WebSession
export interface WebSessionAttribute {
  id:string;
  data?:string;
  last_accessed:Date;
  expire?:number;
}
export interface WebSessionInstance extends Sequelize.Instance<WebSessionAttribute>, WebSessionAttribute { }
export interface WebSessionModel extends Sequelize.Model<WebSessionInstance, WebSessionAttribute> { }

// table: WebsocketEvent
export interface WebsocketEventAttribute {
  ID:number;
  EventSource?:string;
  EventDestination?:string;
  DeviceUid?:string;
  EventID?:string;
  Event?:string;
  Acknowledged?:number;
  AcknowledgedTime?:Date;
  RetryCount?:number;
  CreateTime?:Date;
  UpdateTime?:Date;
}
export interface WebsocketEventInstance extends Sequelize.Instance<WebsocketEventAttribute>, WebsocketEventAttribute { }
export interface WebsocketEventModel extends Sequelize.Model<WebsocketEventInstance, WebsocketEventAttribute> { }

// table: WebsocketEvents
export interface WebsocketEventsAttribute {
  ID:string;
  EventSource?:string;
  EventDestination?:string;
  DeviceUid?:string;
  Type?:string;
  Data?:string;
  RetryCount?:number;
  Status?:string;
  CreateTime?:Date;
  UpdateTime?:Date;
}
export interface WebsocketEventsInstance extends Sequelize.Instance<WebsocketEventsAttribute>, WebsocketEventsAttribute { }
export interface WebsocketEventsModel extends Sequelize.Model<WebsocketEventsInstance, WebsocketEventsAttribute> { }

// table: auth_consumer
export interface auth_consumerAttribute {
  id:number;
  consumer_key:string;
  consumer_secret:string;
  active:number;
  usage?:string;
}
export interface auth_consumerInstance extends Sequelize.Instance<auth_consumerAttribute>, auth_consumerAttribute { }
export interface auth_consumerModel extends Sequelize.Model<auth_consumerInstance, auth_consumerAttribute> { }

// table: auth_consumer_nonce
export interface auth_consumer_nonceAttribute {
  id:number;
  consumer_id:number;
  timestamp:number;
  nonce:string;
}
export interface auth_consumer_nonceInstance extends Sequelize.Instance<auth_consumer_nonceAttribute>, auth_consumer_nonceAttribute { }
export interface auth_consumer_nonceModel extends Sequelize.Model<auth_consumer_nonceInstance, auth_consumer_nonceAttribute> { }

// table: auth_token
export interface auth_tokenAttribute {
  id:number;
  type:number;
  consumer_id:number;
  user_id:number;
  token:string;
  token_secret:string;
  callback_url:string;
  verifier:string;
  created:Date;
}
export interface auth_tokenInstance extends Sequelize.Instance<auth_tokenAttribute>, auth_tokenAttribute { }
export interface auth_tokenModel extends Sequelize.Model<auth_tokenInstance, auth_tokenAttribute> { }

// table: faq
export interface faqAttribute {
  id:number;
  parent:number;
  title:string;
  detail:string;
  faq_id:number;
  order:number;
}
export interface faqInstance extends Sequelize.Instance<faqAttribute>, faqAttribute { }
export interface faqModel extends Sequelize.Model<faqInstance, faqAttribute> { }

// table: auth_token_type
export interface auth_token_typeAttribute {
  id:number;
  type:string;
}
export interface auth_token_typeInstance extends Sequelize.Instance<auth_token_typeAttribute>, auth_token_typeAttribute { }
export interface auth_token_typeModel extends Sequelize.Model<auth_token_typeInstance, auth_token_typeAttribute> { }

// table: faq_list
export interface faq_listAttribute {
  id:number;
  faq_name:string;
  order:number;
}
export interface faq_listInstance extends Sequelize.Instance<faq_listAttribute>, faq_listAttribute { }
export interface faq_listModel extends Sequelize.Model<faq_listInstance, faq_listAttribute> { }

// table: flyway_schema_history
export interface flyway_schema_historyAttribute {
  installed_rank:number;
  version?:string;
  description:string;
  type:string;
  script:string;
  checksum?:number;
  installed_by:string;
  installed_on:Date;
  execution_time:number;
  success:number;
}
export interface flyway_schema_historyInstance extends Sequelize.Instance<flyway_schema_historyAttribute>, flyway_schema_historyAttribute { }
export interface flyway_schema_historyModel extends Sequelize.Model<flyway_schema_historyInstance, flyway_schema_historyAttribute> { }

// table: migration
export interface migrationAttribute {
  version:string;
  apply_time?:number;
}
export interface migrationInstance extends Sequelize.Instance<migrationAttribute>, migrationAttribute { }
export interface migrationModel extends Sequelize.Model<migrationInstance, migrationAttribute> { }

// table: play_evolutions
export interface play_evolutionsAttribute {
  id:number;
  hash:string;
  applied_at:Date;
  apply_script?:string;
  revert_script?:string;
  state?:string;
  last_problem?:string;
}
export interface play_evolutionsInstance extends Sequelize.Instance<play_evolutionsAttribute>, play_evolutionsAttribute { }
export interface play_evolutionsModel extends Sequelize.Model<play_evolutionsInstance, play_evolutionsAttribute> { }

// table: suppliers
export interface suppliersAttribute {
  id:number;
  tenant?:number;
  company_name?:string;
  contact_person?:string;
  phone?:string;
  email?:string;
  website?:string;
  terms?:string;
  credit_limit?:any;
  is_archived?:number;
  create_time?:Date;
  update_time?:Date;
}
export interface suppliersInstance extends Sequelize.Instance<suppliersAttribute>, suppliersAttribute { }
export interface suppliersModel extends Sequelize.Model<suppliersInstance, suppliersAttribute> { }

// table: vPrintDevices
export interface vPrintDevicesAttribute {
  ID:number;
  TenantID:number;
  StationName?:string;
  DeviceName?:string;
  IsArchived:number;
}
export interface vPrintDevicesInstance extends Sequelize.Instance<vPrintDevicesAttribute>, vPrintDevicesAttribute { }
export interface vPrintDevicesModel extends Sequelize.Model<vPrintDevicesInstance, vPrintDevicesAttribute> { }

// table: vSessionActiveTabs
export interface vSessionActiveTabsAttribute {
  TabID:number;
  TenantID:number;
  WaitronID?:number;
  TableID?:number;
  TabName?:string;
  Party:string;
  GuestCount?:number;
  OpenTime?:Date;
  CloseTime?:Date;
  CustomerID?:number;
  NetOrderTotal:number;
  TaxTotal:number;
  Gratuity:number;
  InvoiceTotal:number;
}
export interface vSessionActiveTabsInstance extends Sequelize.Instance<vSessionActiveTabsAttribute>, vSessionActiveTabsAttribute { }
export interface vSessionActiveTabsModel extends Sequelize.Model<vSessionActiveTabsInstance, vSessionActiveTabsAttribute> { }

// table: vTabs
export interface vTabsAttribute {
  TabID:number;
  TenantID:number;
  TenantName:string;
  WaitronID?:number;
  WaitronName?:string;
  TableID?:number;
  TabName?:string;
  Party:string;
  GuestCount?:number;
  OpenDateTime?:Date;
  OpenDate?:Date;
  OpenTime?:any;
  OpenHour?:number;
  CloseDateTime?:Date;
  CloseDate?:Date;
  CloseTime?:any;
  CustomerID?:number;
  NetOrderTotal:number;
  TaxTotal:number;
  Gratuity:number;
  InvoiceTotal:number;
  InvoiceNo:string;
}
export interface vTabsInstance extends Sequelize.Instance<vTabsAttribute>, vTabsAttribute { }
export interface vTabsModel extends Sequelize.Model<vTabsInstance, vTabsAttribute> { }

// table: v_today
export interface v_todayAttribute {
  today:Date;
}
export interface v_todayInstance extends Sequelize.Instance<v_todayAttribute>, v_todayAttribute { }
export interface v_todayModel extends Sequelize.Model<v_todayInstance, v_todayAttribute> { }
