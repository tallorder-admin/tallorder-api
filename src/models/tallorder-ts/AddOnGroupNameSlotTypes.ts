/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {AddOnGroupNameSlotTypesInstance, AddOnGroupNameSlotTypesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<AddOnGroupNameSlotTypesInstance, AddOnGroupNameSlotTypesAttribute>('AddOnGroupNameSlotTypes', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    MenuID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Menus',
        key: 'ID'
      }
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'MenuItems',
        key: 'ID'
      }
    },
    AddOnGroupID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'AddOnGroups',
        key: 'ID'
      }
    },
    AddOnSlotType: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BotID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'AddOnGroupNameSlotTypes'
  });
};
