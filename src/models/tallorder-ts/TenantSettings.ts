/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantSettingsInstance, TenantSettingsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantSettingsInstance, TenantSettingsAttribute>('TenantSettings', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    SettingID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Settings',
        key: 'ID'
      }
    },
    SettingValue: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    tableName: 'TenantSettings'
  });
};
