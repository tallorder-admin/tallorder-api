/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {UserPaymentMethodsInstance, UserPaymentMethodsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<UserPaymentMethodsInstance, UserPaymentMethodsAttribute>('UserPaymentMethods', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PmtTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PmtNickName: {
      type: DataTypes.CHAR(30),
      allowNull: true
    },
    PmtSecureToken: {
      type: DataTypes.CHAR(64),
      allowNull: true
    },
    PmtLastDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    PmtLast: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, {
    tableName: 'UserPaymentMethods'
  });
};
