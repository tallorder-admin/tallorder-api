/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {auth_consumerInstance, auth_consumerAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<auth_consumerInstance, auth_consumerAttribute>('auth_consumer', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    consumer_key: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    consumer_secret: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    active: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    usage: {
      type: DataTypes.STRING(64),
      allowNull: true
    }
  }, {
    tableName: 'auth_consumer'
  });
};
