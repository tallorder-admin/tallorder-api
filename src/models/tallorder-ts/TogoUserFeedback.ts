/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TogoUserFeedbackInstance, TogoUserFeedbackAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TogoUserFeedbackInstance, TogoUserFeedbackAttribute>('TogoUserFeedback', {
    Id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    FirstName: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    LastName: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    MobileNumber: {
      type: DataTypes.STRING(14),
      allowNull: true
    },
    Feedback: {
      type: DataTypes.STRING(511),
      allowNull: true
    },
    UserId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ContactPermission: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    UpdateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TogoUserFeedback'
  });
};
