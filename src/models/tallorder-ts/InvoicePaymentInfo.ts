/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {InvoicePaymentInfoInstance, InvoicePaymentInfoAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<InvoicePaymentInfoInstance, InvoicePaymentInfoAttribute>('InvoicePaymentInfo', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    PaymentTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    PaymentRef: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    Amount: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    PayDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    InvoiceNo: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    AuthCode: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    TransID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    CustomerReceipt: {
      type: DataTypes.STRING(5000),
      allowNull: true
    },
    MerchantReceipt: {
      type: DataTypes.STRING(5000),
      allowNull: true
    },
    SignatureImage: {
      type: "BLOB",
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'InvoicePaymentInfo'
  });
};
