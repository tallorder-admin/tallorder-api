/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PurchaseOrdersInstance, PurchaseOrdersAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PurchaseOrdersInstance, PurchaseOrdersAttribute>('PurchaseOrders', {
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Supplier: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'TenantSuppliers',
        key: 'ID'
      }
    },
    SupplierRef: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    OrderDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DateExpected: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DateReceived: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    PurchaseOrderNumber: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Note: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Status: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    Total: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    DeliveryCost: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ReplenishmentDays: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, {
    tableName: 'PurchaseOrders'
  });
};
