/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PastelCSVUploadsInstance, PastelCSVUploadsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PastelCSVUploadsInstance, PastelCSVUploadsAttribute>('PastelCSVUploads', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    CustomerCode: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    UploadDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    S3_URL: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Error: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ScheduleType: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    DataPeriod: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    tableName: 'PastelCSVUploads'
  });
};
