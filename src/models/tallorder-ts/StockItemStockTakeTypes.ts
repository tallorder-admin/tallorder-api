/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StockItemStockTakeTypesInstance, StockItemStockTakeTypesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StockItemStockTakeTypesInstance, StockItemStockTakeTypesAttribute>('StockItemStockTakeTypes', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    StockItemID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'StockItems',
        key: 'ID'
      }
    },
    StockTakeTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'StockTakeTypes',
        key: 'ID'
      }
    }
  }, {
    tableName: 'StockItemStockTakeTypes'
  });
};
