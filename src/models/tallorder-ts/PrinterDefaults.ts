/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PrinterDefaultsInstance, PrinterDefaultsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PrinterDefaultsInstance, PrinterDefaultsAttribute>('PrinterDefaults', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Brand: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    ModelName: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    DefaultName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FontAWidth: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    FontBWidth: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    LineSpacing: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    UpdateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'PrinterDefaults'
  });
};
