/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {DevicePairingsInstance, DevicePairingsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<DevicePairingsInstance, DevicePairingsAttribute>('DevicePairings', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    DateTime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    StaffID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    HostID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    DeviceID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Adhoc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'DevicePairings'
  });
};
