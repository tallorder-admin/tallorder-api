/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {MenuDiffsInstance, MenuDiffsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<MenuDiffsInstance, MenuDiffsAttribute>('MenuDiffs', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    MenuID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Menus',
        key: 'ID'
      }
    },
    CurrentVersion: {
      type: DataTypes.STRING(255),
      allowNull: false,
      references: {
        model: 'MenuPublishVersions',
        key: 'Version'
      }
    },
    LatestVersion: {
      type: DataTypes.STRING(255),
      allowNull: false,
      references: {
        model: 'MenuPublishVersions',
        key: 'Version'
      }
    },
    Diff: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    DiffKey: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'MenuDiffs'
  });
};
