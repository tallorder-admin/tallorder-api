/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PastelTenantCustomersInstance, PastelTenantCustomersAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PastelTenantCustomersInstance, PastelTenantCustomersAttribute>('PastelTenantCustomers', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    CustomerId: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    CustomerName: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    CustomerCode: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    OrderNumber: {
      type: DataTypes.STRING(25),
      allowNull: false
    },
    IncExTax: {
      type: DataTypes.STRING(1),
      allowNull: false
    },
    InvoiceMessage: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    },
    DeliveryAddress: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    },
    SettlementTerms: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Telephone: {
      type: DataTypes.STRING(16),
      allowNull: false,
      defaultValue: ''
    },
    FaxNumber: {
      type: DataTypes.STRING(16),
      allowNull: false,
      defaultValue: ''
    },
    ContactPerson: {
      type: DataTypes.STRING(16),
      allowNull: false,
      defaultValue: ''
    },
    FreightMethod: {
      type: DataTypes.STRING(10),
      allowNull: false,
      defaultValue: ''
    },
    ShipDeliver: {
      type: DataTypes.STRING(10),
      allowNull: false,
      defaultValue: ''
    },
    Discount: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00'
    },
    AdditionalCosts: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      defaultValue: ''
    },
    IsDefault: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'PastelTenantCustomers'
  });
};
