/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {BrandItemsInstance, BrandItemsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<BrandItemsInstance, BrandItemsAttribute>('BrandItems', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ShortName: {
      type: DataTypes.STRING(30),
      allowNull: false,
      defaultValue: 'New item'
    },
    BarCode: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    PLU: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    Rating: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    CategoryID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    HaveImages: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ImageMediaID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'BrandItems'
  });
};
