/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {CurrencyInstance, CurrencyAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<CurrencyInstance, CurrencyAttribute>('Currency', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    CurrencyCode: {
      type: DataTypes.CHAR(3),
      allowNull: false,
      unique: true
    },
    Symbol: {
      type: DataTypes.CHAR(3),
      allowNull: true
    },
    SymbolPos: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      defaultValue: 'L'
    },
    Currency: {
      type: DataTypes.STRING(60),
      allowNull: false
    },
    UsageCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'Currency'
  });
};
