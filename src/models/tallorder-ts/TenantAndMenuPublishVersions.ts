/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantAndMenuPublishVersionsInstance, TenantAndMenuPublishVersionsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantAndMenuPublishVersionsInstance, TenantAndMenuPublishVersionsAttribute>('TenantAndMenuPublishVersions', {
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    MenuID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Menus',
        key: 'ID'
      }
    },
    Version: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true
    },
    TenantAndMenuFileKey: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    BucketName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    TenantAndMenuS3Url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TenantAndMenuPublishVersions'
  });
};
