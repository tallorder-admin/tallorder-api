/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {LoyaltyProgramsInstance, LoyaltyProgramsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<LoyaltyProgramsInstance, LoyaltyProgramsAttribute>('LoyaltyPrograms', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    SponsorID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ProgramName: {
      type: DataTypes.CHAR(60),
      allowNull: false,
      unique: true
    },
    SignupMsg: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    SignupURL: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Note: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(4),
      allowNull: true,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'LoyaltyPrograms'
  });
};
