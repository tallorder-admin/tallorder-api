/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantAccountSystemInstance, TenantAccountSystemAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantAccountSystemInstance, TenantAccountSystemAttribute>('TenantAccountSystem', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    AccountSysID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'AccountSystems',
        key: 'ID'
      }
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    InitInvoiceNo: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    },
    AppID: {
      type: DataTypes.STRING(75),
      allowNull: true
    },
    AppSecret: {
      type: DataTypes.STRING(75),
      allowNull: true
    },
    AppApiKey: {
      type: DataTypes.STRING(75),
      allowNull: true
    },
    SystemCompanyID: {
      type: DataTypes.STRING(75),
      allowNull: true
    },
    TaxTypeID: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    BankID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CashCustomerID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    GratuityAccountID: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    PettyAccountID: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    InventoryMovementAssets: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    InventoryMovementCostOfSale: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    InventoryMovementAssetsAdjustmentAccount: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    TrackInventoryQuantity: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    InventoryCostType: {
      type: DataTypes.STRING(10),
      allowNull: true,
      defaultValue: 'AVG'
    },
    ItemReferenceField: {
      type: DataTypes.STRING(20),
      allowNull: true,
      defaultValue: 'TextUserField1'
    },
    AnalysisCategory1: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    AnalysisCategory2: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    AnalysisCategory3: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AccountingOrganisationName: {
      type: DataTypes.STRING(512),
      allowNull: true
    },
    Active: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    Syncing: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    SyncProgress: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'TenantAccountSystem'
  });
};
