/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {RestaurantRatingsInstance, RestaurantRatingsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<RestaurantRatingsInstance, RestaurantRatingsAttribute>('RestaurantRatings', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TabID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    RatingDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LocationRating: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ViewRating: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    AblutionsRating: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OverallRating: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    WineListRating: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'RestaurantRatings'
  });
};
