/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {MenuItemIntentsInstance, MenuItemIntentsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<MenuItemIntentsInstance, MenuItemIntentsAttribute>('MenuItemIntents', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'MenuItems',
        key: 'ID'
      }
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    MenuID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Menus',
        key: 'ID'
      }
    },
    IntentName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BotID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Bots',
        key: 'ID'
      }
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'MenuItemIntents'
  });
};
