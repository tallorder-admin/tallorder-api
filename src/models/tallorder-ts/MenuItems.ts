/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {MenuItemsInstance, MenuItemsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<MenuItemsInstance, MenuItemsAttribute>('MenuItems', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ItemType: {
      type: DataTypes.CHAR(255),
      allowNull: false,
      defaultValue: 'product'
    },
    MenuID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ParentID: {
      type: DataTypes.CHAR(15),
      allowNull: true
    },
    ItemID: {
      type: DataTypes.CHAR(15),
      allowNull: true
    },
    ShortName: {
      type: DataTypes.STRING(100),
      allowNull: true,
      defaultValue: 'New item'
    },
    PrintSlipName: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    CategoryID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    StockItemID: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    Seq: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    IsModOnly: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    IsIngredOnly: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    IsProduced: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    Req86Countdown: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    BrandItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ShortcutID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Unit: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    UnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '10'
    },
    Cost: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    Price: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    PriceVar: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    AllowOpenPrice: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    OpenPriceAuthReq: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '1'
    },
    AllowLocalOpenPrice: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    TaxGroupID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'TaxGroups',
        key: 'ID'
      }
    },
    IsTaxable: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    AllowLocalPrice: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ReqMgrSignOff: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    Notes: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    PrepTime: {
      type: DataTypes.TIME,
      allowNull: true
    },
    ServeFrom: {
      type: DataTypes.TIME,
      allowNull: true
    },
    ServeUntil: {
      type: DataTypes.TIME,
      allowNull: true
    },
    SrvSu: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvMo: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvTu: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvWe: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvTh: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvFr: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvSa: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    HostPLU: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    HostedAccountID: {
      type: DataTypes.STRING(75),
      allowNull: true
    },
    HostedAccountCode: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    BarCode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    HostSalesDept: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    ImageMediaID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Media',
        key: 'ID'
      }
    },
    PromoImageMediaID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    HaveImages: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    HasPromoImage: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    PromoServiceGroupID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    WaiterOnly: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AvailableToGo: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AvailableTA: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AvailableSelfService: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AutoAcceptToGo: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ReturnPeriodDays: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true
    },
    WarrantyPeriodMnths: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    WarrantyNote: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'MenuItems'
  });
};
