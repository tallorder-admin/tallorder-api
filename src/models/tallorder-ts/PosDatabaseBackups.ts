/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PosDatabaseBackupsInstance, PosDatabaseBackupsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PosDatabaseBackupsInstance, PosDatabaseBackupsAttribute>('PosDatabaseBackups', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    DeviceUID: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    TimeStamp: {
      type: DataTypes.DATE,
      allowNull: false
    },
    S3_URL: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Error: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'PosDatabaseBackups'
  });
};
