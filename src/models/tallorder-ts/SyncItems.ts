/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {SyncItemsInstance, SyncItemsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<SyncItemsInstance, SyncItemsAttribute>('SyncItems', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ItemType: {
      type: DataTypes.CHAR(7),
      allowNull: false,
      defaultValue: 'product'
    },
    MenuID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ParentID: {
      type: DataTypes.CHAR(15),
      allowNull: true
    },
    ItemID: {
      type: DataTypes.CHAR(15),
      allowNull: true
    },
    ShortName: {
      type: DataTypes.STRING(30),
      allowNull: true,
      defaultValue: 'New item'
    },
    PrintSlipName: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    CategoryID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    StockItemID: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    Seq: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    IsModOnly: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    IsIngredOnly: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    BrandItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ShortcutID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Cost: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    Price: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    PriceVar: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    TaxGroupID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    IsTaxable: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    AllowLocalPrice: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ReqMgrSignOff: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    Notes: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    PrepTime: {
      type: DataTypes.TIME,
      allowNull: true
    },
    ServeFrom: {
      type: DataTypes.TIME,
      allowNull: true
    },
    ServeUntil: {
      type: DataTypes.TIME,
      allowNull: true
    },
    SrvSu: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvMo: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvTu: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvWe: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvTh: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvFr: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    SrvSa: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    HostPLU: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    HostedAccountID: {
      type: DataTypes.STRING(75),
      allowNull: true
    },
    BarCode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    HostSalesDept: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    ImageMediaID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PromoImageMediaID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    HaveImages: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    HasPromoImage: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    PromoServiceGroupID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    WaiterOnly: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AvailableToGo: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AvailableTA: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AutoAcceptToGo: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    ReturnPeriodDays: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true
    },
    WarrantyPeriodMnths: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    WarrantyNote: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'SyncItems'
  });
};
