/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StockUnitConversionsInstance, StockUnitConversionsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StockUnitConversionsInstance, StockUnitConversionsAttribute>('StockUnitConversions', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    FromUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ToUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ConversionFactor: {
      type: "DOUBLE",
      allowNull: true,
      defaultValue: '1'
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'StockUnitConversions'
  });
};
