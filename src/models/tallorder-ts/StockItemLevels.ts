/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StockItemLevelsInstance, StockItemLevelsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StockItemLevelsInstance, StockItemLevelsAttribute>('StockItemLevels', {
    StockItem: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'StockItems',
        key: 'ID'
      }
    },
    BackOrder: {
      type: "DOUBLE",
      allowNull: true
    },
    Available: {
      type: "DOUBLE",
      allowNull: true
    },
    Reserved: {
      type: "DOUBLE",
      allowNull: true
    },
    RunningTotal: {
      type: "DOUBLE",
      allowNull: true
    }
  }, {
    tableName: 'StockItemLevels'
  });
};
