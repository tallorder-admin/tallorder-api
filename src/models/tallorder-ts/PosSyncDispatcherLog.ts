/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PosSyncDispatcherLogInstance, PosSyncDispatcherLogAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PosSyncDispatcherLogInstance, PosSyncDispatcherLogAttribute>('PosSyncDispatcherLog', {
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true
    },
    TenantID: {
      type: DataTypes.INTEGER(20),
      allowNull: true
    },
    Type: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TopicArn: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Subscriptions: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    RequiredSubscriptions: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    NotSubscribed: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Success: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    Status: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Error: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    RetryCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    Data: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    DispatchTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'PosSyncDispatcherLog'
  });
};
