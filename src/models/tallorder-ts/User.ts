/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {UserInstance, UserAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<UserInstance, UserAttribute>('User', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    FirstName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    LastName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    Email: {
      type: DataTypes.CHAR(60),
      allowNull: true,
      unique: true
    },
    pWord: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    pWordResetToken: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Mobile: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    PhoneNo: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    Country: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    City: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    UserPrefLanguage: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    MarketingPref: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    DontAskInfo: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '1'
    },
    Gender: {
      type: DataTypes.INTEGER(4),
      allowNull: true,
      defaultValue: '0'
    },
    YearOfBirth: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    eEmailVerifyID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    AuthKey: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: ''
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    IsAdmin: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    AdminLevel: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '0'
    },
    IsParAdmin: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    UserFavItem: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    PushId: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    EndpointArn: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'User'
  });
};
