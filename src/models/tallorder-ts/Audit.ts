/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {AuditInstance, AuditAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<AuditInstance, AuditAttribute>('Audit', {
    ID: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    WhenStamp: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    SourceID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    AdminID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    Verb: {
      type: DataTypes.STRING(75),
      allowNull: false,
      defaultValue: ''
    },
    Noun: {
      type: DataTypes.STRING(75),
      allowNull: true,
      defaultValue: ''
    },
    JsonBefore: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    JsonAfter: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'Audit'
  });
};
