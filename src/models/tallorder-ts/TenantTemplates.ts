/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {TenantTemplatesInstance, TenantTemplatesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<TenantTemplatesInstance, TenantTemplatesAttribute>('TenantTemplates', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    Name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Body: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    UpdateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'TenantTemplates'
  });
};
