/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {SagePettyCashAccountsInstance, SagePettyCashAccountsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<SagePettyCashAccountsInstance, SagePettyCashAccountsAttribute>('SagePettyCashAccounts', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    SageAccountID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Type: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    Description: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'SagePettyCashAccounts'
  });
};
