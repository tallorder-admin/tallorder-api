/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {BotLexTabUsersInstance, BotLexTabUsersAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<BotLexTabUsersInstance, BotLexTabUsersAttribute>('BotLexTabUsers', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    MenuID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Menus',
        key: 'ID'
      }
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Tenants',
        key: 'ID'
      }
    },
    TabID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Tabs',
        key: 'ID'
      }
    },
    BotID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Bots',
        key: 'ID'
      }
    },
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'User',
        key: 'ID'
      }
    },
    LexChannelUserID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LexChannelType: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    LexChannelName: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    LexChannelID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LexPageAccessToken: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LexTabSnapScanURL: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LexTabSnapScanURLLink: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LexTabZapperURL: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LexTabZapperURLLink: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LexTabSnapScanURLS3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LexTabZapperURLS3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'BotLexTabUsers'
  });
};
