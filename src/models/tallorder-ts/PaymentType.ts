/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PaymentTypeInstance, PaymentTypeAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PaymentTypeInstance, PaymentTypeAttribute>('PaymentType', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TypeCode: {
      type: DataTypes.CHAR(4),
      allowNull: false
    },
    PaymentDescr: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    PaymentDescrStringID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PaymentThirdPartyInfo: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AuthUserPrompt: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    AuthPassPrompt: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    AuthNotifyKeyPrompt: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    DecryptionKeyPrompt: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    LangID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    EnabledPOS: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    EnabledToGo: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    EnabledApps: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    EnabledTA: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    IsArchived: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'PaymentType'
  });
};
