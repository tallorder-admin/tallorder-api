/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {migrationInstance, migrationAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<migrationInstance, migrationAttribute>('migration', {
    version: {
      type: DataTypes.STRING(180),
      allowNull: false,
      primaryKey: true
    },
    apply_time: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'migration'
  });
};
