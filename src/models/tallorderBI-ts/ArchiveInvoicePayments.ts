/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ArchiveInvoicePaymentsInstance, ArchiveInvoicePaymentsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ArchiveInvoicePaymentsInstance, ArchiveInvoicePaymentsAttribute>('ArchiveInvoicePayments', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TypeCode: {
      type: DataTypes.CHAR(4),
      allowNull: false
    },
    PaymentDescr: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    PaymentRef: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    Amount: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    PayDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    InvoiceNo: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    FirstName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    LastName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    HostID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AuthCode: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    TransID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    CustomerReceipt: {
      type: DataTypes.STRING(5000),
      allowNull: true
    },
    MerchantReceipt: {
      type: DataTypes.STRING(5000),
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: '0000-00-00 00:00:00'
    }
  }, {
    tableName: 'ArchiveInvoicePayments'
  });
};
