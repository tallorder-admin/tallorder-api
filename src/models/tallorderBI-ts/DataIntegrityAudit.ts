/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {DataIntegrityAuditInstance, DataIntegrityAuditAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<DataIntegrityAuditInstance, DataIntegrityAuditAttribute>('DataIntegrityAudit', {
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TestOf: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    TestOfID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TestRelativeOf: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    TestRelativeID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TestUUID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ParentDescription: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Result: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    Expected: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Actual: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TestTime: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'DataIntegrityAudit'
  });
};
