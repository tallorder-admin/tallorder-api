/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {MonthlyWaiterPaymentsInstance, MonthlyWaiterPaymentsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<MonthlyWaiterPaymentsInstance, MonthlyWaiterPaymentsAttribute>('MonthlyWaiterPayments', {
    ID: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    PaymentType: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: ''
    },
    StaffRole: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: ''
    },
    StaffName: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: ''
    },
    RollupDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Month: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PaymentTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PaymentCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Total: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    Gratuity: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    ChangeGiven: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    MaxPayment: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    MinPayment: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    TimeZone: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'MonthlyWaiterPayments'
  });
};
