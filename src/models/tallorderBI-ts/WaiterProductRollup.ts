/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {WaiterProductRollupInstance, WaiterProductRollupAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<WaiterProductRollupInstance, WaiterProductRollupAttribute>('WaiterProductRollup', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ProductID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    HostOrderID: {
      type: DataTypes.STRING(255),
      allowNull: false,
      unique: true
    },
    HostTabId: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    HourlyRollupId: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    DailyRollupId: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    MonthlyRollupId: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    RollupDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    TimeZone: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'WaiterProductRollup'
  });
};
