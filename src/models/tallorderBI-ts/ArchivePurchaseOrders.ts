/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ArchivePurchaseOrdersInstance, ArchivePurchaseOrdersAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ArchivePurchaseOrdersInstance, ArchivePurchaseOrdersAttribute>('ArchivePurchaseOrders', {
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: '',
      primaryKey: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    StaffID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    StaffName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PurchaseOrderNumber: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SupplierID: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    SupplierName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SupplierRef: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SupplierContactPerson: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SupplierPhone: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SupplierEmail: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SupplierAddressLine1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SupplierAddressLine2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SupplierAddressLine3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SupplierAddressCity: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SupplierAddressCountry: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SupplierAddressZipCode: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TotalExclDeliveryCost: {
      type: "DOUBLE",
      allowNull: true
    },
    DeliveryCost: {
      type: "DOUBLE",
      allowNull: true
    },
    TotalInclDeliveryCost: {
      type: "DOUBLE",
      allowNull: true
    },
    ExpenseAccountID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ExpenseAccountName: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    DateOrdered: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DateExpected: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DateReceived: {
      type: DataTypes.DATE,
      allowNull: true
    },
    IsReceived: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'ArchivePurchaseOrders'
  });
};
