/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StockTakeInstance, StockTakeAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StockTakeInstance, StockTakeAttribute>('StockTake', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    StaffID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    StaffName: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    Date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    Type: {
      type: DataTypes.STRING(60),
      allowNull: false
    },
    HostStockTakeID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Note: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'StockTake'
  });
};
