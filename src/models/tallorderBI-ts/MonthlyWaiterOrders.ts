/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {MonthlyWaiterOrdersInstance, MonthlyWaiterOrdersAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<MonthlyWaiterOrdersInstance, MonthlyWaiterOrdersAttribute>('MonthlyWaiterOrders', {
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TenantName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    StaffName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    StaffRole: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RollupDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Month: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ItemShortName: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    ItemName: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    CategoryID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    CategoryName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    BarCode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    BrandItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    BrandTenant: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PortionQuantity: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    UnitQuantity: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    InvoiceTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    NetTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    TaxTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    CostTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    Discounted: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    Voided: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    VoidedPortionQuantity: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    VoidedUnitQuantity: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    VoidedInvoiceTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    VoidedNetTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    VoidedTaxTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    VoidedCostTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    TimeZone: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'MonthlyWaiterOrders'
  });
};
