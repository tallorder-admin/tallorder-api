/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ArchiveShiftsInstance, ArchiveShiftsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ArchiveShiftsInstance, ArchiveShiftsAttribute>('ArchiveShifts', {
    HostShiftID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    DatetimeSignIn: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DatetimeSignOut: {
      type: DataTypes.DATE,
      allowNull: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    StaffId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    StaffName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    InitialCashFloat: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    FinalCashFloat: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    TotalTablesServed: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TotalTips: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    CashDue: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    VoidsCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    VoidsValue: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    TurnOver: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    NoOfTabs: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    GrossProfit: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    HighestTabValue: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    LowestTabValue: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    TimeZone: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    Verified: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    Status: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Error: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ShiftNumber: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'ArchiveShifts'
  });
};
