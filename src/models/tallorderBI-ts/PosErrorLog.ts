/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PosErrorLogInstance, PosErrorLogAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PosErrorLogInstance, PosErrorLogAttribute>('PosErrorLog', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Type: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    DeviceID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    AdditionalInfo: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    OriginalError: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    CustomError: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ErrorID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Version: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    ErrorTime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    TimeZone: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'PosErrorLog'
  });
};
