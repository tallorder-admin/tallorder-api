/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ArchiveTenantsInstance, ArchiveTenantsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ArchiveTenantsInstance, ArchiveTenantsAttribute>('ArchiveTenants', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    TenantName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    TenantType: {
      type: DataTypes.CHAR(30),
      allowNull: false
    },
    ProdType: {
      type: DataTypes.CHAR(30),
      allowNull: false
    },
    Email: {
      type: DataTypes.CHAR(60),
      allowNull: false,
      defaultValue: ''
    },
    CountryCode: {
      type: DataTypes.CHAR(2),
      allowNull: false
    },
    CurrencyCode: {
      type: DataTypes.CHAR(3),
      allowNull: false
    },
    Symbol: {
      type: DataTypes.CHAR(3),
      allowNull: true
    },
    LangCode: {
      type: DataTypes.CHAR(2),
      allowNull: false
    },
    LocaleID: {
      type: DataTypes.CHAR(12),
      allowNull: true,
      defaultValue: 'en_US_POSIX'
    },
    TimeZone: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    MoneyFormat: {
      type: DataTypes.CHAR(32),
      allowNull: true,
      defaultValue: '%1.2f'
    },
    DateFormat: {
      type: DataTypes.CHAR(32),
      allowNull: true,
      defaultValue: 'yyyy-MM-DD HH:MM'
    },
    ThemeBColor: {
      type: DataTypes.CHAR(10),
      allowNull: true,
      defaultValue: '59B211'
    },
    ThemeFColor: {
      type: DataTypes.CHAR(10),
      allowNull: true,
      defaultValue: 'FFFFFF'
    },
    WaiterTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true,
      defaultValue: 'Waiter'
    },
    MenuTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true,
      defaultValue: 'Menu'
    },
    MenusTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true,
      defaultValue: 'Menus'
    },
    TabTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true,
      defaultValue: 'Tab'
    },
    TabsTerm: {
      type: DataTypes.CHAR(16),
      allowNull: true,
      defaultValue: 'Tabs'
    },
    MenuTenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    POSVendor: {
      type: DataTypes.STRING(30),
      allowNull: false,
      defaultValue: ''
    }
  }, {
    tableName: 'ArchiveTenants'
  });
};
