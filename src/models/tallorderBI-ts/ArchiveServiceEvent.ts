/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ArchiveServiceEventInstance, ArchiveServiceEventAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ArchiveServiceEventInstance, ArchiveServiceEventAttribute>('ArchiveServiceEvent', {
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true
    },
    TenantID: {
      type: DataTypes.INTEGER(20),
      allowNull: true
    },
    ServiceName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Type: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MasterDataId: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    JobStarted: {
      type: DataTypes.DATE,
      allowNull: false
    },
    JobCompleted: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Completed: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    Status: {
      type: DataTypes.STRING(20),
      allowNull: true,
      defaultValue: 'NEW'
    },
    Error: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    RetryCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    Data: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'ArchiveServiceEvent'
  });
};
