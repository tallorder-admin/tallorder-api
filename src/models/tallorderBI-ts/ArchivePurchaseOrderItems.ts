/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ArchivePurchaseOrderItemsInstance, ArchivePurchaseOrderItemsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ArchivePurchaseOrderItemsInstance, ArchivePurchaseOrderItemsAttribute>('ArchivePurchaseOrderItems', {
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    PurchaseOrderID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SupplierID: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    SupplierName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ItemName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HostPLU: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    SelectedUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SelectedUnitDescription: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BaseUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    BaseUnitDescription: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SelectedUnitQuantity: {
      type: "DOUBLE",
      allowNull: true
    },
    ConversionFactor: {
      type: "DOUBLE",
      allowNull: true
    },
    BaseUnitQuantity: {
      type: "DOUBLE",
      allowNull: true
    },
    BaseUnitCost: {
      type: "DOUBLE",
      allowNull: true
    },
    ProportionalDeliveryCost: {
      type: "DOUBLE",
      allowNull: true
    },
    TotalExclDeliveryCost: {
      type: "DOUBLE",
      allowNull: true
    },
    TotalInclDeliveryCost: {
      type: "DOUBLE",
      allowNull: true
    },
    ReplenishmentCycleDays: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    LastReplenishmentCycleDays: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    AverageReplenishmentCycleDays: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SupplierLastReplenishmentCycleDays: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SupplierAverageReplenishmentCycleDays: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    DateOrdered: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DateExpected: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DateReceived: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'ArchivePurchaseOrderItems'
  });
};
