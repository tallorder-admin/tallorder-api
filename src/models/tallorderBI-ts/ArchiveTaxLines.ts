/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ArchiveTaxLinesInstance, ArchiveTaxLinesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ArchiveTaxLinesInstance, ArchiveTaxLinesAttribute>('ArchiveTaxLines', {
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TabID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    TaxTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    TaxID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TaxType: {
      type: DataTypes.CHAR(50),
      allowNull: true
    },
    InclusiveTax: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '1'
    },
    TaxReference: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    Regime: {
      type: DataTypes.CHAR(60),
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: '0000-00-00 00:00:00'
    }
  }, {
    tableName: 'ArchiveTaxLines'
  });
};
