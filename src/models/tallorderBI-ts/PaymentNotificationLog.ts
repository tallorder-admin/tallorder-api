/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {PaymentNotificationLogInstance, PaymentNotificationLogAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<PaymentNotificationLogInstance, PaymentNotificationLogAttribute>('PaymentNotificationLog', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Environment: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Method: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    Ref: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Payload: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'PaymentNotificationLog'
  });
};
