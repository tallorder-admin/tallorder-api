/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ArchiveSessionsInstance, ArchiveSessionsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ArchiveSessionsInstance, ArchiveSessionsAttribute>('ArchiveSessions', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TenantName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    StaffType: {
      type: DataTypes.CHAR(30),
      allowNull: false
    },
    FirstName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    LastName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    PhoneNo: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    Email: {
      type: DataTypes.CHAR(60),
      allowNull: false,
      defaultValue: ''
    },
    SignIn: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SignOut: {
      type: DataTypes.DATE,
      allowNull: true
    },
    AreaID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Area: {
      type: DataTypes.CHAR(50),
      allowNull: true
    },
    DeviceID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Device: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DeviceType: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    ShiftID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TotalTablesServer: {
      type: DataTypes.INTEGER(3),
      allowNull: true
    },
    TotalRestaurantRevenue: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    TotalTips: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, {
    tableName: 'ArchiveSessions'
  });
};
