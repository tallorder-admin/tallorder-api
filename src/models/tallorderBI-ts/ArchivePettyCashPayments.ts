/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ArchivePettyCashPaymentsInstance, ArchivePettyCashPaymentsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ArchivePettyCashPaymentsInstance, ArchivePettyCashPaymentsAttribute>('ArchivePettyCashPayments', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TenantName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Reason: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Note: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Amount: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    Source: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    AccountHostID: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    StaffID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ManagerID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    HostShiftID: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    StaffFirstName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    StaffLastName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    StaffEmail: {
      type: DataTypes.CHAR(60),
      allowNull: true
    },
    StaffMobile: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    ManagerFirstName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    ManagerLastName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    ManagerEmail: {
      type: DataTypes.CHAR(60),
      allowNull: true
    },
    ManagerMobile: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    OriginDeviceUID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HostPettyCashPaymentID: {
      type: DataTypes.STRING(255),
      allowNull: true,
      unique: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'ArchivePettyCashPayments'
  });
};
