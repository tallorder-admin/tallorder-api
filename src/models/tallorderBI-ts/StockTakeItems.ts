/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StockTakeItemsInstance, StockTakeItemsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StockTakeItemsInstance, StockTakeItemsAttribute>('StockTakeItems', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    StaffID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ItemName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    StaffName: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    Date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    MeasurementUnit: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    ExpectedQuantity: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    ActualQuantity: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    Discrepancy: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    HostStockTakeID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    HostStockTakeItemID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Note: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'StockTakeItems'
  });
};
