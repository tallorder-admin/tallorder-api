/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ArchiveTabsInstance, ArchiveTabsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ArchiveTabsInstance, ArchiveTabsAttribute>('ArchiveTabs', {
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TenantName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Origin: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: 'POS'
    },
    OriginDeviceUID: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    TabName: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    HostID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FirstName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    LastName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    Role: {
      type: DataTypes.CHAR(40),
      allowNull: true,
      defaultValue: ''
    },
    Email: {
      type: DataTypes.CHAR(60),
      allowNull: true
    },
    Mobile: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    PhoneNo: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    CustomerID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    LoyaltyProgRef: {
      type: DataTypes.CHAR(64),
      allowNull: true
    },
    MarketingPermissionFlag: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    CustFirstName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    CustLastName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    CustEmail: {
      type: DataTypes.CHAR(60),
      allowNull: true,
      defaultValue: ''
    },
    CustPhoneNo: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    TableID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Table: {
      type: DataTypes.CHAR(50),
      allowNull: true
    },
    HostTableID: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    TableAreaID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TableArea: {
      type: DataTypes.CHAR(50),
      allowNull: true
    },
    Party: {
      type: DataTypes.CHAR(64),
      allowNull: true
    },
    GuestCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    Voided: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    TabOpenTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    TabCloseTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ReportDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    NetOrderTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    Gratuity: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    ChangeGiven: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    TaxTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    InvoiceTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    CostTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    InvoiceNo: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HostTabID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HostWaiterID: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    HostWaiterName: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    Note: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SubscribeType: {
      type: DataTypes.CHAR(3),
      allowNull: true
    },
    SubscribeID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: '0000-00-00 00:00:00'
    },
    SyncFileLocation: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    DayOfWeek: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IsPublicHoliday: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    IsSchoolHoliday: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    Season: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Weather: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PosGPS: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FirstWeekAfterPayDay: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    FirstWeekendAfterPayDay: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    LastWeekBeforePayDay: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    LastWeekendBeforePayDay: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    TimeZone: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    HostShiftID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    AccountingTabSyncHistoryID: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    AccountingSyncLocked: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    IntegrityCheck: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    LastIntegrityCheck: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'ArchiveTabs'
  });
};
