/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {play_evolutionsInstance, play_evolutionsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<play_evolutionsInstance, play_evolutionsAttribute>('play_evolutions', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    hash: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    applied_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    apply_script: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    revert_script: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    state: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    last_problem: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'play_evolutions'
  });
};
