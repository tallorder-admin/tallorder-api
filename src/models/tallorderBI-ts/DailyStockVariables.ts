/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {DailyStockVariablesInstance, DailyStockVariablesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<DailyStockVariablesInstance, DailyStockVariablesAttribute>('DailyStockVariables', {
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    LineItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    LineItemType: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    LineItemDescription: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BaseUnitDescription: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    DaysSalesAvailable: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    BaseUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SevenDayStockDay: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ThirtyDayStockDay: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SixtyDayStockDay: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    StockOut: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    SevenDayQuantityMA: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ThirtyDayQuantityMA: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SixtyDayQuantityMA: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SevenDaySalesMA: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ThirtyDaySalesMA: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SixtyDaySalesMA: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SevenDayProfitMA: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ThirtyDayProfitMA: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SixtyDayProfitMA: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SevenDayMarginMA: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ThirtyDayMarginMA: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SixtyDayMarginMA: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    AnnualisedNetSalesStockTurn: {
      type: "DOUBLE",
      allowNull: true
    },
    MonthlyNetSalesStockTurn: {
      type: "DOUBLE",
      allowNull: true
    },
    ThirtyDayNetSalesStockTurn: {
      type: "DOUBLE",
      allowNull: true
    },
    AnnualNetSalesStockTurn: {
      type: "DOUBLE",
      allowNull: true
    },
    AnnualisedCOSStockTurn: {
      type: "DOUBLE",
      allowNull: true
    },
    MonthlyCOSStockTurn: {
      type: "DOUBLE",
      allowNull: true
    },
    ThirtyDayCOSStockTurn: {
      type: "DOUBLE",
      allowNull: true
    },
    AnnualCOSStockTurn: {
      type: "DOUBLE",
      allowNull: true
    },
    OpeningStock: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ClosingStock: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    Purchases: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    OtherStockReceived: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    TotalStockReceived: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    NetStockMovement: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ActualUnitsUsed: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    TheoreticalUnitsUsed: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    UnitsUsedVariance: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    OpeningUnitCost: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    OpeningAverageUnitCost: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    OpeningStockValue: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ClosingUnitCost: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ClosingAverageUnitCost: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ClosingStockValue: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    LogDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    EndDayLogDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SevenDayStartDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ThirtyDayStartDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SixtyDayStartDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    MonthStartDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    YearToDateStartDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'DailyStockVariables'
  });
};
