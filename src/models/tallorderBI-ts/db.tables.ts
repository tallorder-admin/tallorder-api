// tslint:disable
import * as path from 'path';
import * as sequelize from 'sequelize';
import * as def from './db';

export interface ITables {
  AccountingTabSyncHistory:def.AccountingTabSyncHistoryModel;
  ArchiveInvoicePayments:def.ArchiveInvoicePaymentsModel;
  ArchiveAuditTrail:def.ArchiveAuditTrailModel;
  ArchiveOrderTaxLines:def.ArchiveOrderTaxLinesModel;
  ArchiveOrderLines:def.ArchiveOrderLinesModel;
  ArchivePettyCashPayments:def.ArchivePettyCashPaymentsModel;
  ArchivePettyPayments:def.ArchivePettyPaymentsModel;
  ArchivePurchaseOrderItems:def.ArchivePurchaseOrderItemsModel;
  ArchiveServiceEvent:def.ArchiveServiceEventModel;
  ArchivePurchaseOrders:def.ArchivePurchaseOrdersModel;
  ArchiveSessions:def.ArchiveSessionsModel;
  ArchiveShifts:def.ArchiveShiftsModel;
  ArchiveTabPayments:def.ArchiveTabPaymentsModel;
  ArchiveTenants:def.ArchiveTenantsModel;
  DailyOrders:def.DailyOrdersModel;
  DailyStockVariables:def.DailyStockVariablesModel;
  ArchiveTabs:def.ArchiveTabsModel;
  ArchiveTaxLines:def.ArchiveTaxLinesModel;
  DailyTabs:def.DailyTabsModel;
  DailyTabsPerWaiter:def.DailyTabsPerWaiterModel;
  DailyWaiterOrders:def.DailyWaiterOrdersModel;
  DailyWaiterPayments:def.DailyWaiterPaymentsModel;
  DataIntegrityAdditionalInfo:def.DataIntegrityAdditionalInfoModel;
  DataIntegrityAudit:def.DataIntegrityAuditModel;
  HourlyOrders:def.HourlyOrdersModel;
  HourlySales:def.HourlySalesModel;
  HourlyWaiterOrders:def.HourlyWaiterOrdersModel;
  HourlyTabsPerWaiter:def.HourlyTabsPerWaiterModel;
  HourlyWaiterPayments:def.HourlyWaiterPaymentsModel;
  MonthlyOrders:def.MonthlyOrdersModel;
  MonthlyTabs:def.MonthlyTabsModel;
  MonthlyTabsPerWaiter:def.MonthlyTabsPerWaiterModel;
  MonthlyWaiterOrders:def.MonthlyWaiterOrdersModel;
  MonthlyWaiterPayments:def.MonthlyWaiterPaymentsModel;
  PaymentNotificationLog:def.PaymentNotificationLogModel;
  PosErrorLog:def.PosErrorLogModel;
  SequelizeMeta:def.SequelizeMetaModel;
  StockAdjustments:def.StockAdjustmentsModel;
  StockTake:def.StockTakeModel;
  StockTakeItems:def.StockTakeItemsModel;
  TenantProductRollup:def.TenantProductRollupModel;
  WaiterProductRollup:def.WaiterProductRollupModel;
  WaiterPaymentsRollup:def.WaiterPaymentsRollupModel;
  TenantTabsRollup:def.TenantTabsRollupModel;
  WaiterTabsRollup:def.WaiterTabsRollupModel;
  play_evolutions:def.play_evolutionsModel;
}

export const getModels = function(seq:sequelize.Sequelize):ITables {
  const tables:ITables = {
    AccountingTabSyncHistory: seq.import(path.join(__dirname, './AccountingTabSyncHistory')),
    ArchiveInvoicePayments: seq.import(path.join(__dirname, './ArchiveInvoicePayments')),
    ArchiveAuditTrail: seq.import(path.join(__dirname, './ArchiveAuditTrail')),
    ArchiveOrderTaxLines: seq.import(path.join(__dirname, './ArchiveOrderTaxLines')),
    ArchiveOrderLines: seq.import(path.join(__dirname, './ArchiveOrderLines')),
    ArchivePettyCashPayments: seq.import(path.join(__dirname, './ArchivePettyCashPayments')),
    ArchivePettyPayments: seq.import(path.join(__dirname, './ArchivePettyPayments')),
    ArchivePurchaseOrderItems: seq.import(path.join(__dirname, './ArchivePurchaseOrderItems')),
    ArchiveServiceEvent: seq.import(path.join(__dirname, './ArchiveServiceEvent')),
    ArchivePurchaseOrders: seq.import(path.join(__dirname, './ArchivePurchaseOrders')),
    ArchiveSessions: seq.import(path.join(__dirname, './ArchiveSessions')),
    ArchiveShifts: seq.import(path.join(__dirname, './ArchiveShifts')),
    ArchiveTabPayments: seq.import(path.join(__dirname, './ArchiveTabPayments')),
    ArchiveTenants: seq.import(path.join(__dirname, './ArchiveTenants')),
    DailyOrders: seq.import(path.join(__dirname, './DailyOrders')),
    DailyStockVariables: seq.import(path.join(__dirname, './DailyStockVariables')),
    ArchiveTabs: seq.import(path.join(__dirname, './ArchiveTabs')),
    ArchiveTaxLines: seq.import(path.join(__dirname, './ArchiveTaxLines')),
    DailyTabs: seq.import(path.join(__dirname, './DailyTabs')),
    DailyTabsPerWaiter: seq.import(path.join(__dirname, './DailyTabsPerWaiter')),
    DailyWaiterOrders: seq.import(path.join(__dirname, './DailyWaiterOrders')),
    DailyWaiterPayments: seq.import(path.join(__dirname, './DailyWaiterPayments')),
    DataIntegrityAdditionalInfo: seq.import(path.join(__dirname, './DataIntegrityAdditionalInfo')),
    DataIntegrityAudit: seq.import(path.join(__dirname, './DataIntegrityAudit')),
    HourlyOrders: seq.import(path.join(__dirname, './HourlyOrders')),
    HourlySales: seq.import(path.join(__dirname, './HourlySales')),
    HourlyWaiterOrders: seq.import(path.join(__dirname, './HourlyWaiterOrders')),
    HourlyTabsPerWaiter: seq.import(path.join(__dirname, './HourlyTabsPerWaiter')),
    HourlyWaiterPayments: seq.import(path.join(__dirname, './HourlyWaiterPayments')),
    MonthlyOrders: seq.import(path.join(__dirname, './MonthlyOrders')),
    MonthlyTabs: seq.import(path.join(__dirname, './MonthlyTabs')),
    MonthlyTabsPerWaiter: seq.import(path.join(__dirname, './MonthlyTabsPerWaiter')),
    MonthlyWaiterOrders: seq.import(path.join(__dirname, './MonthlyWaiterOrders')),
    MonthlyWaiterPayments: seq.import(path.join(__dirname, './MonthlyWaiterPayments')),
    PaymentNotificationLog: seq.import(path.join(__dirname, './PaymentNotificationLog')),
    PosErrorLog: seq.import(path.join(__dirname, './PosErrorLog')),
    SequelizeMeta: seq.import(path.join(__dirname, './SequelizeMeta')),
    StockAdjustments: seq.import(path.join(__dirname, './StockAdjustments')),
    StockTake: seq.import(path.join(__dirname, './StockTake')),
    StockTakeItems: seq.import(path.join(__dirname, './StockTakeItems')),
    TenantProductRollup: seq.import(path.join(__dirname, './TenantProductRollup')),
    WaiterProductRollup: seq.import(path.join(__dirname, './WaiterProductRollup')),
    WaiterPaymentsRollup: seq.import(path.join(__dirname, './WaiterPaymentsRollup')),
    TenantTabsRollup: seq.import(path.join(__dirname, './TenantTabsRollup')),
    WaiterTabsRollup: seq.import(path.join(__dirname, './WaiterTabsRollup')),
    play_evolutions: seq.import(path.join(__dirname, './play_evolutions')),
  };
  return tables;
};
