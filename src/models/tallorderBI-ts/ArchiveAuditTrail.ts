/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ArchiveAuditTrailInstance, ArchiveAuditTrailAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ArchiveAuditTrailInstance, ArchiveAuditTrailAttribute>('ArchiveAuditTrail', {
    ID: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false
    },
    WhenStamp: {
      type: DataTypes.DATE,
      allowNull: false
    },
    SourceName: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    TabID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    InvoiceNo: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    UserName: {
      type: DataTypes.STRING(75),
      allowNull: false
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    TenantName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Verb: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: ''
    },
    Description: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    }
  }, {
    tableName: 'ArchiveAuditTrail'
  });
};
