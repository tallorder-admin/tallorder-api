/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {StockAdjustmentsInstance, StockAdjustmentsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<StockAdjustmentsInstance, StockAdjustmentsAttribute>('StockAdjustments', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    StaffID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    StaffName: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    Date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    Type: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ItemName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MasterID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HostAdjustmentID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    MeasurementUnit: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    Quantity: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    QuantityBefore: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    QuantityAfter: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    StockValueChange: {
      type: "DOUBLE",
      allowNull: true,
      defaultValue: '0'
    },
    Note: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    LastCostPerBaseUnit: {
      type: "DOUBLE",
      allowNull: true,
      defaultValue: '0'
    },
    AverageCostPerBaseUnit: {
      type: "DOUBLE",
      allowNull: true,
      defaultValue: '0'
    },
    RunningStockValue: {
      type: "DOUBLE",
      allowNull: true,
      defaultValue: '0'
    }
  }, {
    tableName: 'StockAdjustments'
  });
};
