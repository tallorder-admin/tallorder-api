// tslint:disable
import * as Sequelize from 'sequelize';


// table: AccountingTabSyncHistory
export interface AccountingTabSyncHistoryAttribute {
  ID:number;
  TenantID:number;
  HostTabID:string;
  InvoiceNumber:string;
  AccountingID:string;
  LocalTabData?:string;
  AccountingTabData?:string;
  AccountingPaymentData?:string;
  AdditionalInformation?:string;
  SyncDateTime?:Date;
  AccountingAllocationData?:string;
}
export interface AccountingTabSyncHistoryInstance extends Sequelize.Instance<AccountingTabSyncHistoryAttribute>, AccountingTabSyncHistoryAttribute { }
export interface AccountingTabSyncHistoryModel extends Sequelize.Model<AccountingTabSyncHistoryInstance, AccountingTabSyncHistoryAttribute> { }

// table: ArchiveInvoicePayments
export interface ArchiveInvoicePaymentsAttribute {
  ID:number;
  TenantID:number;
  TypeCode:string;
  PaymentDescr?:string;
  PaymentRef?:string;
  Amount?:number;
  PayDate?:Date;
  InvoiceNo?:string;
  WaitronID?:number;
  FirstName?:string;
  LastName?:string;
  HostID?:string;
  AuthCode?:string;
  TransID?:number;
  CustomerReceipt?:string;
  MerchantReceipt?:string;
  LastUpdate:Date;
}
export interface ArchiveInvoicePaymentsInstance extends Sequelize.Instance<ArchiveInvoicePaymentsAttribute>, ArchiveInvoicePaymentsAttribute { }
export interface ArchiveInvoicePaymentsModel extends Sequelize.Model<ArchiveInvoicePaymentsInstance, ArchiveInvoicePaymentsAttribute> { }

// table: ArchiveAuditTrail
export interface ArchiveAuditTrailAttribute {
  ID:number;
  WhenStamp:Date;
  SourceName:string;
  TabID?:number;
  InvoiceNo?:string;
  UserID:number;
  UserName:string;
  TenantID:number;
  TenantName:string;
  Verb:string;
  Description:string;
}
export interface ArchiveAuditTrailInstance extends Sequelize.Instance<ArchiveAuditTrailAttribute>, ArchiveAuditTrailAttribute { }
export interface ArchiveAuditTrailModel extends Sequelize.Model<ArchiveAuditTrailInstance, ArchiveAuditTrailAttribute> { }

// table: ArchiveOrderTaxLines
export interface ArchiveOrderTaxLinesAttribute {
  ID:string;
  TenantID:number;
  TabID:string;
  OrderLineID:string;
  TaxTotal?:number;
  TaxID:number;
  TaxType?:string;
  InclusiveTax?:number;
  TaxReference?:string;
  Regime?:string;
  LastUpdate:Date;
}
export interface ArchiveOrderTaxLinesInstance extends Sequelize.Instance<ArchiveOrderTaxLinesAttribute>, ArchiveOrderTaxLinesAttribute { }
export interface ArchiveOrderTaxLinesModel extends Sequelize.Model<ArchiveOrderTaxLinesInstance, ArchiveOrderTaxLinesAttribute> { }

// table: ArchiveOrderLines
export interface ArchiveOrderLinesAttribute {
  ID:string;
  TenantID:number;
  TenantName:string;
  OriginDeviceUID?:string;
  MenuItemID:number;
  AddOnID?:number;
  ItemShortName:string;
  ItemName?:string;
  CategoryID?:number;
  CategoryName?:string;
  BarCode?:string;
  BrandItemID?:number;
  BrandTenant?:string;
  LineNo:number;
  Price?:number;
  Qty?:number;
  InvQty?:number;
  InvoiceNumber?:string;
  GuestNum?:number;
  OrderDateTime?:Date;
  DeliveredDateTime?:Date;
  PrimaryMenuItemID?:number;
  PrintCount?:number;
  Note?:string;
  HostOrderID?:string;
  LineTotal?:number;
  NetTotal?:number;
  TaxTotal?:number;
  CostTotal?:number;
  TabID:string;
  HostTabID?:string;
  HostWaiterID?:string;
  HostWaiterName?:string;
  TabName?:string;
  WaitronID?:number;
  HostID?:string;
  FirstName?:string;
  LastName?:string;
  Role?:string;
  Email?:string;
  Mobile?:string;
  PhoneNo?:string;
  HostPLU?:string;
  HostSalesDept?:string;
  SaleType?:string;
  LastUpdate:Date;
  Discounted?:number;
  Voided?:number;
  UnplacedVoid?:number;
  UserCode?:string;
  Returned?:number;
  ParentOrderId?:number;
  IntegrityCheck?:number;
  HostShiftID?:string;
}
export interface ArchiveOrderLinesInstance extends Sequelize.Instance<ArchiveOrderLinesAttribute>, ArchiveOrderLinesAttribute { }
export interface ArchiveOrderLinesModel extends Sequelize.Model<ArchiveOrderLinesInstance, ArchiveOrderLinesAttribute> { }

// table: ArchivePettyCashPayments
export interface ArchivePettyCashPaymentsAttribute {
  ID:number;
  TenantID:number;
  TenantName:string;
  Reason?:string;
  Note?:string;
  Amount?:number;
  Source?:string;
  AccountHostID?:string;
  StaffID?:number;
  ManagerID?:number;
  HostShiftID?:string;
  StaffFirstName?:string;
  StaffLastName?:string;
  StaffEmail?:string;
  StaffMobile?:string;
  ManagerFirstName?:string;
  ManagerLastName?:string;
  ManagerEmail?:string;
  ManagerMobile?:string;
  OriginDeviceUID?:string;
  HostPettyCashPaymentID?:string;
  DateCreated:Date;
}
export interface ArchivePettyCashPaymentsInstance extends Sequelize.Instance<ArchivePettyCashPaymentsAttribute>, ArchivePettyCashPaymentsAttribute { }
export interface ArchivePettyCashPaymentsModel extends Sequelize.Model<ArchivePettyCashPaymentsInstance, ArchivePettyCashPaymentsAttribute> { }

// table: ArchivePettyPayments
export interface ArchivePettyPaymentsAttribute {
  ID:number;
  TenantID:number;
  TenantName:string;
  Amount?:number;
  Notes?:string;
  WaitronID?:number;
  FirstName?:string;
  LastName?:string;
  Email?:string;
  Mobile?:string;
  HostID?:number;
  DateCreated:Date;
}
export interface ArchivePettyPaymentsInstance extends Sequelize.Instance<ArchivePettyPaymentsAttribute>, ArchivePettyPaymentsAttribute { }
export interface ArchivePettyPaymentsModel extends Sequelize.Model<ArchivePettyPaymentsInstance, ArchivePettyPaymentsAttribute> { }

// table: ArchivePurchaseOrderItems
export interface ArchivePurchaseOrderItemsAttribute {
  ID:string;
  TenantID:number;
  PurchaseOrderID?:string;
  SupplierID?:number;
  SupplierName?:string;
  MenuItemID?:number;
  ItemName?:string;
  HostPLU?:string;
  SelectedUnitID?:number;
  SelectedUnitDescription?:string;
  BaseUnitID?:number;
  BaseUnitDescription?:string;
  SelectedUnitQuantity?:any;
  ConversionFactor?:any;
  BaseUnitQuantity?:any;
  BaseUnitCost?:any;
  ProportionalDeliveryCost?:any;
  TotalExclDeliveryCost?:any;
  TotalInclDeliveryCost?:any;
  ReplenishmentCycleDays?:number;
  LastReplenishmentCycleDays?:number;
  AverageReplenishmentCycleDays?:number;
  SupplierLastReplenishmentCycleDays?:number;
  SupplierAverageReplenishmentCycleDays?:number;
  DateOrdered?:Date;
  DateExpected?:Date;
  DateReceived?:Date;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface ArchivePurchaseOrderItemsInstance extends Sequelize.Instance<ArchivePurchaseOrderItemsAttribute>, ArchivePurchaseOrderItemsAttribute { }
export interface ArchivePurchaseOrderItemsModel extends Sequelize.Model<ArchivePurchaseOrderItemsInstance, ArchivePurchaseOrderItemsAttribute> { }

// table: ArchiveServiceEvent
export interface ArchiveServiceEventAttribute {
  ID:string;
  TenantID?:number;
  ServiceName:string;
  Type?:string;
  MasterDataId?:string;
  JobStarted:Date;
  JobCompleted?:Date;
  Completed?:number;
  Status?:string;
  Error?:string;
  RetryCount?:number;
  Data?:string;
  LastUpdate?:Date;
}
export interface ArchiveServiceEventInstance extends Sequelize.Instance<ArchiveServiceEventAttribute>, ArchiveServiceEventAttribute { }
export interface ArchiveServiceEventModel extends Sequelize.Model<ArchiveServiceEventInstance, ArchiveServiceEventAttribute> { }

// table: ArchivePurchaseOrders
export interface ArchivePurchaseOrdersAttribute {
  ID:string;
  TenantID?:number;
  StaffID?:number;
  StaffName?:string;
  PurchaseOrderNumber?:string;
  SupplierID?:number;
  SupplierName?:string;
  SupplierRef?:string;
  SupplierContactPerson?:string;
  SupplierPhone?:string;
  SupplierEmail?:string;
  SupplierAddressLine1?:string;
  SupplierAddressLine2?:string;
  SupplierAddressLine3?:string;
  SupplierAddressCity?:string;
  SupplierAddressCountry?:string;
  SupplierAddressZipCode?:string;
  TotalExclDeliveryCost?:any;
  DeliveryCost?:any;
  TotalInclDeliveryCost?:any;
  ExpenseAccountID?:number;
  ExpenseAccountName?:string;
  DateOrdered?:Date;
  DateExpected?:Date;
  DateReceived?:Date;
  IsReceived?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface ArchivePurchaseOrdersInstance extends Sequelize.Instance<ArchivePurchaseOrdersAttribute>, ArchivePurchaseOrdersAttribute { }
export interface ArchivePurchaseOrdersModel extends Sequelize.Model<ArchivePurchaseOrdersInstance, ArchivePurchaseOrdersAttribute> { }

// table: ArchiveSessions
export interface ArchiveSessionsAttribute {
  ID:number;
  TenantID:number;
  TenantName:string;
  WaitronID:number;
  StaffType:string;
  FirstName?:string;
  LastName?:string;
  PhoneNo?:string;
  Email:string;
  SignIn?:Date;
  SignOut?:Date;
  AreaID?:number;
  Area?:string;
  DeviceID:number;
  Device?:string;
  DeviceType?:string;
  ShiftID?:number;
  TotalTablesServer?:number;
  TotalRestaurantRevenue?:number;
  TotalTips?:number;
}
export interface ArchiveSessionsInstance extends Sequelize.Instance<ArchiveSessionsAttribute>, ArchiveSessionsAttribute { }
export interface ArchiveSessionsModel extends Sequelize.Model<ArchiveSessionsInstance, ArchiveSessionsAttribute> { }

// table: ArchiveShifts
export interface ArchiveShiftsAttribute {
  HostShiftID:string;
  DatetimeSignIn?:Date;
  DatetimeSignOut?:Date;
  TenantID?:number;
  StaffId?:number;
  StaffName?:string;
  InitialCashFloat?:number;
  FinalCashFloat?:number;
  TotalTablesServed?:number;
  TotalTips?:number;
  CashDue?:number;
  VoidsCount?:number;
  VoidsValue?:number;
  TurnOver?:number;
  NoOfTabs?:number;
  GrossProfit?:number;
  HighestTabValue?:number;
  LowestTabValue?:number;
  TimeZone?:number;
  create_time?:Date;
  update_time?:Date;
  Verified?:number;
  Status?:string;
  Error?:string;
  ShiftNumber?:string;
}
export interface ArchiveShiftsInstance extends Sequelize.Instance<ArchiveShiftsAttribute>, ArchiveShiftsAttribute { }
export interface ArchiveShiftsModel extends Sequelize.Model<ArchiveShiftsInstance, ArchiveShiftsAttribute> { }

// table: ArchiveTabPayments
export interface ArchiveTabPaymentsAttribute {
  ID:string;
  TenantID:number;
  TenantName:string;
  PaymentTypeID:number;
  PaymentDescr?:string;
  PaymentRef?:string;
  Amount?:number;
  Balance?:number;
  Gratuity?:number;
  ChangeGiven?:number;
  WaitronID?:number;
  FirstName?:string;
  LastName?:string;
  Role?:string;
  Email?:string;
  Mobile?:string;
  PhoneNo?:string;
  TabID:string;
  CustomerID?:number;
  LoyaltyProgRef?:string;
  MarketingPermissionFlag?:number;
  CustFirstName?:string;
  CustLastName?:string;
  CustEmail?:string;
  CustPhoneNo?:string;
  Table?:string;
  HostTableID?:string;
  TableAreaID?:number;
  TableArea?:string;
  TabName?:string;
  Party?:string;
  GuestCount?:number;
  TabOpenTime?:Date;
  TabCloseTime?:Date;
  InvoiceNo?:string;
  HostTabID?:string;
  HostWaiterID?:string;
  HostWaiterName?:string;
  TxID?:string;
  TxNotes?:string;
  LastUpdate:Date;
  HostPaymentID?:string;
  HostShiftID?:string;
}
export interface ArchiveTabPaymentsInstance extends Sequelize.Instance<ArchiveTabPaymentsAttribute>, ArchiveTabPaymentsAttribute { }
export interface ArchiveTabPaymentsModel extends Sequelize.Model<ArchiveTabPaymentsInstance, ArchiveTabPaymentsAttribute> { }

// table: ArchiveTenants
export interface ArchiveTenantsAttribute {
  ID:number;
  TenantName:string;
  TenantType:string;
  ProdType:string;
  Email:string;
  CountryCode:string;
  CurrencyCode:string;
  Symbol?:string;
  LangCode:string;
  LocaleID?:string;
  TimeZone?:number;
  MoneyFormat?:string;
  DateFormat?:string;
  ThemeBColor?:string;
  ThemeFColor?:string;
  WaiterTerm?:string;
  MenuTerm?:string;
  MenusTerm?:string;
  TabTerm?:string;
  TabsTerm?:string;
  MenuTenantID?:number;
  POSVendor:string;
}
export interface ArchiveTenantsInstance extends Sequelize.Instance<ArchiveTenantsAttribute>, ArchiveTenantsAttribute { }
export interface ArchiveTenantsModel extends Sequelize.Model<ArchiveTenantsInstance, ArchiveTenantsAttribute> { }

// table: DailyOrders
export interface DailyOrdersAttribute {
  ID:string;
  TenantID:number;
  TenantName:string;
  RollupDate?:Date;
  Day?:number;
  MenuItemID:number;
  ItemShortName?:string;
  ItemName?:string;
  CategoryID?:number;
  CategoryName?:string;
  BarCode?:string;
  BrandItemID?:number;
  BrandTenant?:string;
  PortionQuantity?:number;
  UnitQuantity?:number;
  InvoiceTotal?:number;
  NetTotal?:number;
  TaxTotal?:number;
  CostTotal?:number;
  Discounted?:number;
  Voided?:number;
  VoidedPortionQuantity?:number;
  VoidedUnitQuantity?:number;
  VoidedInvoiceTotal?:number;
  VoidedNetTotal?:number;
  VoidedTaxTotal?:number;
  VoidedCostTotal?:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface DailyOrdersInstance extends Sequelize.Instance<DailyOrdersAttribute>, DailyOrdersAttribute { }
export interface DailyOrdersModel extends Sequelize.Model<DailyOrdersInstance, DailyOrdersAttribute> { }

// table: DailyStockVariables
export interface DailyStockVariablesAttribute {
  ID:string;
  TenantID:number;
  LineItemID:number;
  LineItemType:string;
  LineItemDescription?:string;
  BaseUnitDescription?:string;
  DaysSalesAvailable?:number;
  BaseUnitID?:number;
  SevenDayStockDay?:number;
  ThirtyDayStockDay?:number;
  SixtyDayStockDay?:number;
  StockOut?:number;
  SevenDayQuantityMA?:number;
  ThirtyDayQuantityMA?:number;
  SixtyDayQuantityMA?:number;
  SevenDaySalesMA?:number;
  ThirtyDaySalesMA?:number;
  SixtyDaySalesMA?:number;
  SevenDayProfitMA?:number;
  ThirtyDayProfitMA?:number;
  SixtyDayProfitMA?:number;
  SevenDayMarginMA?:number;
  ThirtyDayMarginMA?:number;
  SixtyDayMarginMA?:number;
  AnnualisedNetSalesStockTurn?:any;
  MonthlyNetSalesStockTurn?:any;
  ThirtyDayNetSalesStockTurn?:any;
  AnnualNetSalesStockTurn?:any;
  AnnualisedCOSStockTurn?:any;
  MonthlyCOSStockTurn?:any;
  ThirtyDayCOSStockTurn?:any;
  AnnualCOSStockTurn?:any;
  OpeningStock?:number;
  ClosingStock?:number;
  Purchases?:number;
  OtherStockReceived?:number;
  TotalStockReceived?:number;
  NetStockMovement?:number;
  ActualUnitsUsed?:number;
  TheoreticalUnitsUsed?:number;
  UnitsUsedVariance?:number;
  OpeningUnitCost?:number;
  OpeningAverageUnitCost?:number;
  OpeningStockValue?:number;
  ClosingUnitCost?:number;
  ClosingAverageUnitCost?:number;
  ClosingStockValue?:number;
  LogDate:Date;
  EndDayLogDate?:Date;
  SevenDayStartDate?:Date;
  ThirtyDayStartDate?:Date;
  SixtyDayStartDate?:Date;
  MonthStartDate?:Date;
  YearToDateStartDate?:Date;
  LastUpdate?:Date;
  CreateTime?:Date;
}
export interface DailyStockVariablesInstance extends Sequelize.Instance<DailyStockVariablesAttribute>, DailyStockVariablesAttribute { }
export interface DailyStockVariablesModel extends Sequelize.Model<DailyStockVariablesInstance, DailyStockVariablesAttribute> { }

// table: ArchiveTabs
export interface ArchiveTabsAttribute {
  TenantID:number;
  TenantName:string;
  ID:string;
  Origin?:string;
  OriginDeviceUID?:string;
  TabName?:string;
  WaitronID?:number;
  HostID?:string;
  FirstName?:string;
  LastName?:string;
  Role?:string;
  Email?:string;
  Mobile?:string;
  PhoneNo?:string;
  CustomerID?:number;
  LoyaltyProgRef?:string;
  MarketingPermissionFlag?:number;
  CustFirstName?:string;
  CustLastName?:string;
  CustEmail?:string;
  CustPhoneNo?:string;
  TableID?:number;
  Table?:string;
  HostTableID?:string;
  TableAreaID?:number;
  TableArea?:string;
  Party?:string;
  GuestCount?:number;
  Voided?:number;
  TabOpenTime?:Date;
  TabCloseTime?:Date;
  ReportDate?:Date;
  NetOrderTotal?:number;
  Gratuity?:number;
  ChangeGiven?:number;
  TaxTotal?:number;
  InvoiceTotal?:number;
  CostTotal?:number;
  InvoiceNo?:string;
  HostTabID?:string;
  HostWaiterID?:string;
  HostWaiterName?:string;
  Note?:string;
  SubscribeType?:string;
  SubscribeID?:string;
  LastUpdate:Date;
  SyncFileLocation?:string;
  DayOfWeek?:string;
  IsPublicHoliday?:boolean;
  IsSchoolHoliday?:boolean;
  Season?:string;
  Weather?:string;
  PosGPS?:string;
  FirstWeekAfterPayDay?:boolean;
  FirstWeekendAfterPayDay?:boolean;
  LastWeekBeforePayDay?:boolean;
  LastWeekendBeforePayDay?:boolean;
  TimeZone?:number;
  HostShiftID?:string;
  AccountingTabSyncHistoryID?:number;
  AccountingSyncLocked?:number;
  IntegrityCheck?:number;
  LastIntegrityCheck?:Date;
}
export interface ArchiveTabsInstance extends Sequelize.Instance<ArchiveTabsAttribute>, ArchiveTabsAttribute { }
export interface ArchiveTabsModel extends Sequelize.Model<ArchiveTabsInstance, ArchiveTabsAttribute> { }

// table: ArchiveTaxLines
export interface ArchiveTaxLinesAttribute {
  ID:string;
  TenantID:number;
  TabID:string;
  TaxTotal?:number;
  TaxID:number;
  TaxType?:string;
  InclusiveTax?:number;
  TaxReference?:string;
  Regime?:string;
  LastUpdate:Date;
}
export interface ArchiveTaxLinesInstance extends Sequelize.Instance<ArchiveTaxLinesAttribute>, ArchiveTaxLinesAttribute { }
export interface ArchiveTaxLinesModel extends Sequelize.Model<ArchiveTaxLinesInstance, ArchiveTaxLinesAttribute> { }

// table: DailyTabs
export interface DailyTabsAttribute {
  ID:string;
  TenantID:number;
  TenantName:string;
  RollupDate?:Date;
  Day?:number;
  NetOrderTotal?:number;
  Gratuity?:number;
  ChangeGiven?:number;
  TaxTotal?:number;
  CostTotal?:number;
  InvoiceTotal?:number;
  MaxTotal?:number;
  MinTotal?:number;
  AverageTotal?:number;
  TabCount?:number;
  Voided?:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface DailyTabsInstance extends Sequelize.Instance<DailyTabsAttribute>, DailyTabsAttribute { }
export interface DailyTabsModel extends Sequelize.Model<DailyTabsInstance, DailyTabsAttribute> { }

// table: DailyTabsPerWaiter
export interface DailyTabsPerWaiterAttribute {
  ID:string;
  TenantID:number;
  WaitronID:number;
  StaffRole?:string;
  StaffName?:string;
  RollupDate?:Date;
  Day?:number;
  NetOrderTotal?:number;
  Gratuity?:number;
  ChangeGiven?:number;
  TaxTotal?:number;
  CostTotal?:number;
  InvoiceTotal?:number;
  MaxTotal?:number;
  MinTotal?:number;
  AverageTotal?:number;
  TabCount?:number;
  Voided?:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface DailyTabsPerWaiterInstance extends Sequelize.Instance<DailyTabsPerWaiterAttribute>, DailyTabsPerWaiterAttribute { }
export interface DailyTabsPerWaiterModel extends Sequelize.Model<DailyTabsPerWaiterInstance, DailyTabsPerWaiterAttribute> { }

// table: DailyWaiterOrders
export interface DailyWaiterOrdersAttribute {
  ID:string;
  TenantID:number;
  TenantName:string;
  WaitronID:number;
  StaffName?:string;
  StaffRole?:string;
  RollupDate?:Date;
  Day?:number;
  MenuItemID:number;
  ItemShortName?:string;
  ItemName?:string;
  CategoryID?:number;
  CategoryName?:string;
  BarCode?:string;
  BrandItemID?:number;
  BrandTenant?:string;
  PortionQuantity?:number;
  UnitQuantity?:number;
  InvoiceTotal?:number;
  NetTotal?:number;
  TaxTotal?:number;
  CostTotal?:number;
  Discounted?:number;
  Voided?:number;
  VoidedPortionQuantity?:number;
  VoidedUnitQuantity?:number;
  VoidedInvoiceTotal?:number;
  VoidedNetTotal?:number;
  VoidedTaxTotal?:number;
  VoidedCostTotal?:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface DailyWaiterOrdersInstance extends Sequelize.Instance<DailyWaiterOrdersAttribute>, DailyWaiterOrdersAttribute { }
export interface DailyWaiterOrdersModel extends Sequelize.Model<DailyWaiterOrdersInstance, DailyWaiterOrdersAttribute> { }

// table: DailyWaiterPayments
export interface DailyWaiterPaymentsAttribute {
  ID:string;
  TenantID:number;
  WaitronID:number;
  PaymentType?:string;
  StaffRole?:string;
  StaffName?:string;
  RollupDate?:Date;
  Day?:number;
  PaymentTypeID?:number;
  PaymentCount?:number;
  Total?:number;
  Gratuity?:number;
  ChangeGiven?:number;
  MaxPayment:number;
  MinPayment:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface DailyWaiterPaymentsInstance extends Sequelize.Instance<DailyWaiterPaymentsAttribute>, DailyWaiterPaymentsAttribute { }
export interface DailyWaiterPaymentsModel extends Sequelize.Model<DailyWaiterPaymentsInstance, DailyWaiterPaymentsAttribute> { }

// table: DataIntegrityAdditionalInfo
export interface DataIntegrityAdditionalInfoAttribute {
  ID:number;
  AuditID:string;
  Key?:string;
  Value?:string;
}
export interface DataIntegrityAdditionalInfoInstance extends Sequelize.Instance<DataIntegrityAdditionalInfoAttribute>, DataIntegrityAdditionalInfoAttribute { }
export interface DataIntegrityAdditionalInfoModel extends Sequelize.Model<DataIntegrityAdditionalInfoInstance, DataIntegrityAdditionalInfoAttribute> { }

// table: DataIntegrityAudit
export interface DataIntegrityAuditAttribute {
  ID:string;
  TenantID?:number;
  TestOf?:string;
  TestOfID?:string;
  TestRelativeOf?:string;
  TestRelativeID?:string;
  TestUUID?:string;
  ParentDescription?:string;
  Description?:string;
  Result?:string;
  Expected?:string;
  Actual?:string;
  TestTime?:Date;
}
export interface DataIntegrityAuditInstance extends Sequelize.Instance<DataIntegrityAuditAttribute>, DataIntegrityAuditAttribute { }
export interface DataIntegrityAuditModel extends Sequelize.Model<DataIntegrityAuditInstance, DataIntegrityAuditAttribute> { }

// table: HourlyOrders
export interface HourlyOrdersAttribute {
  ID:string;
  TenantID:number;
  TenantName:string;
  RollupDate?:Date;
  Hour?:number;
  MenuItemID:number;
  ItemShortName?:string;
  ItemName?:string;
  CategoryID?:number;
  CategoryName?:string;
  BarCode?:string;
  BrandItemID?:number;
  BrandTenant?:string;
  PortionQuantity?:number;
  UnitQuantity?:number;
  InvoiceTotal?:number;
  NetTotal?:number;
  TaxTotal?:number;
  CostTotal?:number;
  Discounted?:number;
  Voided?:number;
  VoidedPortionQuantity?:number;
  VoidedUnitQuantity?:number;
  VoidedInvoiceTotal?:number;
  VoidedNetTotal?:number;
  VoidedTaxTotal?:number;
  VoidedCostTotal?:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface HourlyOrdersInstance extends Sequelize.Instance<HourlyOrdersAttribute>, HourlyOrdersAttribute { }
export interface HourlyOrdersModel extends Sequelize.Model<HourlyOrdersInstance, HourlyOrdersAttribute> { }

// table: HourlySales
export interface HourlySalesAttribute {
  ID:string;
  TenantID:number;
  TenantName:string;
  RollupDate?:Date;
  Hour?:number;
  NetOrderTotal?:number;
  Gratuity?:number;
  ChangeGiven?:number;
  TaxTotal?:number;
  CostTotal?:number;
  InvoiceTotal?:number;
  MaxTotal?:number;
  MinTotal?:number;
  AverageTotal?:number;
  TabCount?:number;
  Voided?:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface HourlySalesInstance extends Sequelize.Instance<HourlySalesAttribute>, HourlySalesAttribute { }
export interface HourlySalesModel extends Sequelize.Model<HourlySalesInstance, HourlySalesAttribute> { }

// table: HourlyWaiterOrders
export interface HourlyWaiterOrdersAttribute {
  ID:string;
  TenantID:number;
  TenantName:string;
  WaitronID:number;
  StaffName?:string;
  StaffRole?:string;
  RollupDate?:Date;
  Hour?:number;
  MenuItemID:number;
  ItemShortName?:string;
  ItemName?:string;
  CategoryID?:number;
  CategoryName?:string;
  BarCode?:string;
  BrandItemID?:number;
  BrandTenant?:string;
  PortionQuantity?:number;
  UnitQuantity?:number;
  InvoiceTotal?:number;
  NetTotal?:number;
  TaxTotal?:number;
  CostTotal?:number;
  Discounted?:number;
  Voided?:number;
  VoidedPortionQuantity?:number;
  VoidedUnitQuantity?:number;
  VoidedInvoiceTotal?:number;
  VoidedNetTotal?:number;
  VoidedTaxTotal?:number;
  VoidedCostTotal?:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface HourlyWaiterOrdersInstance extends Sequelize.Instance<HourlyWaiterOrdersAttribute>, HourlyWaiterOrdersAttribute { }
export interface HourlyWaiterOrdersModel extends Sequelize.Model<HourlyWaiterOrdersInstance, HourlyWaiterOrdersAttribute> { }

// table: HourlyTabsPerWaiter
export interface HourlyTabsPerWaiterAttribute {
  ID:string;
  TenantID:number;
  WaitronID:number;
  StaffRole?:string;
  StaffName?:string;
  RollupDate?:Date;
  Hour?:number;
  NetOrderTotal?:number;
  Gratuity?:number;
  ChangeGiven?:number;
  TaxTotal?:number;
  CostTotal?:number;
  InvoiceTotal?:number;
  MaxTotal?:number;
  MinTotal?:number;
  AverageTotal?:number;
  TabCount?:number;
  Voided?:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface HourlyTabsPerWaiterInstance extends Sequelize.Instance<HourlyTabsPerWaiterAttribute>, HourlyTabsPerWaiterAttribute { }
export interface HourlyTabsPerWaiterModel extends Sequelize.Model<HourlyTabsPerWaiterInstance, HourlyTabsPerWaiterAttribute> { }

// table: HourlyWaiterPayments
export interface HourlyWaiterPaymentsAttribute {
  ID:string;
  TenantID:number;
  WaitronID:number;
  PaymentType?:string;
  StaffRole?:string;
  StaffName?:string;
  RollupDate?:Date;
  Hour?:number;
  PaymentTypeID?:number;
  PaymentCount?:number;
  Total?:number;
  Gratuity?:number;
  ChangeGiven?:number;
  MaxPayment:number;
  MinPayment:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface HourlyWaiterPaymentsInstance extends Sequelize.Instance<HourlyWaiterPaymentsAttribute>, HourlyWaiterPaymentsAttribute { }
export interface HourlyWaiterPaymentsModel extends Sequelize.Model<HourlyWaiterPaymentsInstance, HourlyWaiterPaymentsAttribute> { }

// table: MonthlyOrders
export interface MonthlyOrdersAttribute {
  ID:string;
  TenantID:number;
  TenantName:string;
  RollupDate?:Date;
  Month?:number;
  MenuItemID:number;
  ItemShortName?:string;
  ItemName?:string;
  CategoryID?:number;
  CategoryName?:string;
  BarCode?:string;
  BrandItemID?:number;
  BrandTenant?:string;
  PortionQuantity?:number;
  UnitQuantity?:number;
  InvoiceTotal?:number;
  NetTotal?:number;
  TaxTotal?:number;
  CostTotal?:number;
  Discounted?:number;
  Voided?:number;
  VoidedPortionQuantity?:number;
  VoidedUnitQuantity?:number;
  VoidedInvoiceTotal?:number;
  VoidedNetTotal?:number;
  VoidedTaxTotal?:number;
  VoidedCostTotal?:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface MonthlyOrdersInstance extends Sequelize.Instance<MonthlyOrdersAttribute>, MonthlyOrdersAttribute { }
export interface MonthlyOrdersModel extends Sequelize.Model<MonthlyOrdersInstance, MonthlyOrdersAttribute> { }

// table: MonthlyTabs
export interface MonthlyTabsAttribute {
  ID:string;
  TenantID:number;
  TenantName:string;
  RollupDate?:Date;
  Month?:number;
  NetOrderTotal?:number;
  Gratuity?:number;
  ChangeGiven?:number;
  TaxTotal?:number;
  CostTotal?:number;
  InvoiceTotal?:number;
  MaxTotal?:number;
  MinTotal?:number;
  AverageTotal?:number;
  TabCount?:number;
  Voided?:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface MonthlyTabsInstance extends Sequelize.Instance<MonthlyTabsAttribute>, MonthlyTabsAttribute { }
export interface MonthlyTabsModel extends Sequelize.Model<MonthlyTabsInstance, MonthlyTabsAttribute> { }

// table: MonthlyTabsPerWaiter
export interface MonthlyTabsPerWaiterAttribute {
  ID:string;
  TenantID:number;
  WaitronID:number;
  StaffRole?:string;
  StaffName?:string;
  RollupDate?:Date;
  Month?:number;
  NetOrderTotal?:number;
  Gratuity?:number;
  ChangeGiven?:number;
  TaxTotal?:number;
  CostTotal?:number;
  InvoiceTotal?:number;
  MaxTotal?:number;
  MinTotal?:number;
  AverageTotal?:number;
  TabCount?:number;
  Voided?:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface MonthlyTabsPerWaiterInstance extends Sequelize.Instance<MonthlyTabsPerWaiterAttribute>, MonthlyTabsPerWaiterAttribute { }
export interface MonthlyTabsPerWaiterModel extends Sequelize.Model<MonthlyTabsPerWaiterInstance, MonthlyTabsPerWaiterAttribute> { }

// table: MonthlyWaiterOrders
export interface MonthlyWaiterOrdersAttribute {
  ID:string;
  TenantID:number;
  TenantName:string;
  WaitronID:number;
  StaffName?:string;
  StaffRole?:string;
  RollupDate?:Date;
  Month?:number;
  MenuItemID:number;
  ItemShortName?:string;
  ItemName?:string;
  CategoryID?:number;
  CategoryName?:string;
  BarCode?:string;
  BrandItemID?:number;
  BrandTenant?:string;
  PortionQuantity?:number;
  UnitQuantity?:number;
  InvoiceTotal?:number;
  NetTotal?:number;
  TaxTotal?:number;
  CostTotal?:number;
  Discounted?:number;
  Voided?:number;
  VoidedPortionQuantity?:number;
  VoidedUnitQuantity?:number;
  VoidedInvoiceTotal?:number;
  VoidedNetTotal?:number;
  VoidedTaxTotal?:number;
  VoidedCostTotal?:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface MonthlyWaiterOrdersInstance extends Sequelize.Instance<MonthlyWaiterOrdersAttribute>, MonthlyWaiterOrdersAttribute { }
export interface MonthlyWaiterOrdersModel extends Sequelize.Model<MonthlyWaiterOrdersInstance, MonthlyWaiterOrdersAttribute> { }

// table: MonthlyWaiterPayments
export interface MonthlyWaiterPaymentsAttribute {
  ID:string;
  TenantID:number;
  WaitronID:number;
  PaymentType?:string;
  StaffRole?:string;
  StaffName?:string;
  RollupDate?:Date;
  Month?:number;
  PaymentTypeID?:number;
  PaymentCount?:number;
  Total?:number;
  Gratuity?:number;
  ChangeGiven?:number;
  MaxPayment:number;
  MinPayment:number;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface MonthlyWaiterPaymentsInstance extends Sequelize.Instance<MonthlyWaiterPaymentsAttribute>, MonthlyWaiterPaymentsAttribute { }
export interface MonthlyWaiterPaymentsModel extends Sequelize.Model<MonthlyWaiterPaymentsInstance, MonthlyWaiterPaymentsAttribute> { }

// table: PaymentNotificationLog
export interface PaymentNotificationLogAttribute {
  ID:number;
  Environment?:string;
  Method?:string;
  Ref?:string;
  Payload?:string;
  DateCreated:Date;
}
export interface PaymentNotificationLogInstance extends Sequelize.Instance<PaymentNotificationLogAttribute>, PaymentNotificationLogAttribute { }
export interface PaymentNotificationLogModel extends Sequelize.Model<PaymentNotificationLogInstance, PaymentNotificationLogAttribute> { }

// table: PosErrorLog
export interface PosErrorLogAttribute {
  ID:number;
  TenantID:number;
  Type?:string;
  DeviceID:string;
  AdditionalInfo?:string;
  OriginalError?:string;
  CustomError?:string;
  ErrorID:string;
  Version:string;
  ErrorTime:Date;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface PosErrorLogInstance extends Sequelize.Instance<PosErrorLogAttribute>, PosErrorLogAttribute { }
export interface PosErrorLogModel extends Sequelize.Model<PosErrorLogInstance, PosErrorLogAttribute> { }

// table: SequelizeMeta
export interface SequelizeMetaAttribute {
  name:string;
}
export interface SequelizeMetaInstance extends Sequelize.Instance<SequelizeMetaAttribute>, SequelizeMetaAttribute { }
export interface SequelizeMetaModel extends Sequelize.Model<SequelizeMetaInstance, SequelizeMetaAttribute> { }

// table: StockAdjustments
export interface StockAdjustmentsAttribute {
  ID:number;
  TenantID:number;
  StaffID?:number;
  StaffName?:string;
  Date:Date;
  Type:string;
  MenuItemID:number;
  ItemName?:string;
  MasterID?:string;
  HostAdjustmentID?:string;
  MeasurementUnit?:string;
  Quantity?:number;
  QuantityBefore?:number;
  QuantityAfter?:number;
  StockValueChange?:any;
  Note?:string;
  CreateTime?:Date;
  LastUpdate?:Date;
  LastCostPerBaseUnit?:any;
  AverageCostPerBaseUnit?:any;
  RunningStockValue?:any;
}
export interface StockAdjustmentsInstance extends Sequelize.Instance<StockAdjustmentsAttribute>, StockAdjustmentsAttribute { }
export interface StockAdjustmentsModel extends Sequelize.Model<StockAdjustmentsInstance, StockAdjustmentsAttribute> { }

// table: StockTake
export interface StockTakeAttribute {
  ID:number;
  TenantID:number;
  StaffID:number;
  StaffName?:string;
  Date:Date;
  Type:string;
  HostStockTakeID:string;
  Note?:string;
  LastUpdate:Date;
}
export interface StockTakeInstance extends Sequelize.Instance<StockTakeAttribute>, StockTakeAttribute { }
export interface StockTakeModel extends Sequelize.Model<StockTakeInstance, StockTakeAttribute> { }

// table: StockTakeItems
export interface StockTakeItemsAttribute {
  ID:number;
  TenantID:number;
  StaffID:number;
  MenuItemID:number;
  ItemName?:string;
  StaffName?:string;
  Date:Date;
  MeasurementUnit?:string;
  ExpectedQuantity?:number;
  ActualQuantity?:number;
  Discrepancy?:number;
  HostStockTakeID:string;
  HostStockTakeItemID:string;
  Note?:string;
  LastUpdate:Date;
}
export interface StockTakeItemsInstance extends Sequelize.Instance<StockTakeItemsAttribute>, StockTakeItemsAttribute { }
export interface StockTakeItemsModel extends Sequelize.Model<StockTakeItemsInstance, StockTakeItemsAttribute> { }

// table: TenantProductRollup
export interface TenantProductRollupAttribute {
  ID:number;
  TenantID:number;
  ProductID:number;
  HostTabId:string;
  HostOrderID:string;
  HourlyRollupId:string;
  DailyRollupId:string;
  MonthlyRollupId:string;
  RollupDate:Date;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface TenantProductRollupInstance extends Sequelize.Instance<TenantProductRollupAttribute>, TenantProductRollupAttribute { }
export interface TenantProductRollupModel extends Sequelize.Model<TenantProductRollupInstance, TenantProductRollupAttribute> { }

// table: WaiterProductRollup
export interface WaiterProductRollupAttribute {
  ID:number;
  TenantID:number;
  WaitronID:number;
  ProductID:number;
  HostOrderID:string;
  HostTabId:string;
  HourlyRollupId:string;
  DailyRollupId:string;
  MonthlyRollupId:string;
  RollupDate:Date;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface WaiterProductRollupInstance extends Sequelize.Instance<WaiterProductRollupAttribute>, WaiterProductRollupAttribute { }
export interface WaiterProductRollupModel extends Sequelize.Model<WaiterProductRollupInstance, WaiterProductRollupAttribute> { }

// table: WaiterPaymentsRollup
export interface WaiterPaymentsRollupAttribute {
  ID:number;
  TenantID:number;
  WaitronID:number;
  PaymentTypeID:number;
  HostTabId:string;
  HostPaymentId:string;
  HourlyRollupId:string;
  DailyRollupId:string;
  MonthlyRollupId:string;
  RollupDate:Date;
  TabCloseTime:Date;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface WaiterPaymentsRollupInstance extends Sequelize.Instance<WaiterPaymentsRollupAttribute>, WaiterPaymentsRollupAttribute { }
export interface WaiterPaymentsRollupModel extends Sequelize.Model<WaiterPaymentsRollupInstance, WaiterPaymentsRollupAttribute> { }

// table: TenantTabsRollup
export interface TenantTabsRollupAttribute {
  ID:number;
  TenantID:number;
  HostTabId:string;
  HourlyRollupId:string;
  DailyRollupId:string;
  MonthlyRollupId:string;
  RollupDate:Date;
  TabCloseTime:Date;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface TenantTabsRollupInstance extends Sequelize.Instance<TenantTabsRollupAttribute>, TenantTabsRollupAttribute { }
export interface TenantTabsRollupModel extends Sequelize.Model<TenantTabsRollupInstance, TenantTabsRollupAttribute> { }

// table: WaiterTabsRollup
export interface WaiterTabsRollupAttribute {
  ID:number;
  TenantID:number;
  WaitronID:number;
  HostTabId:string;
  HourlyRollupId:string;
  DailyRollupId:string;
  MonthlyRollupId:string;
  RollupDate:Date;
  TabCloseTime:Date;
  TimeZone?:number;
  CreateTime?:Date;
  LastUpdate?:Date;
}
export interface WaiterTabsRollupInstance extends Sequelize.Instance<WaiterTabsRollupAttribute>, WaiterTabsRollupAttribute { }
export interface WaiterTabsRollupModel extends Sequelize.Model<WaiterTabsRollupInstance, WaiterTabsRollupAttribute> { }

// table: play_evolutions
export interface play_evolutionsAttribute {
  id:number;
  hash:string;
  applied_at:Date;
  apply_script?:string;
  revert_script?:string;
  state?:string;
  last_problem?:string;
}
export interface play_evolutionsInstance extends Sequelize.Instance<play_evolutionsAttribute>, play_evolutionsAttribute { }
export interface play_evolutionsModel extends Sequelize.Model<play_evolutionsInstance, play_evolutionsAttribute> { }
