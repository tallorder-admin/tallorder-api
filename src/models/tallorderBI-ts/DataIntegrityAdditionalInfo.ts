/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {DataIntegrityAdditionalInfoInstance, DataIntegrityAdditionalInfoAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<DataIntegrityAdditionalInfoInstance, DataIntegrityAdditionalInfoAttribute>('DataIntegrityAdditionalInfo', {
    ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    AuditID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Key: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Value: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'DataIntegrityAdditionalInfo'
  });
};
