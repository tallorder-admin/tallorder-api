/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {AccountingTabSyncHistoryInstance, AccountingTabSyncHistoryAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<AccountingTabSyncHistoryInstance, AccountingTabSyncHistoryAttribute>('AccountingTabSyncHistory', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    HostTabID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    InvoiceNumber: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    AccountingID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    LocalTabData: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    AccountingTabData: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    AccountingPaymentData: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    AdditionalInformation: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    SyncDateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    AccountingAllocationData: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'AccountingTabSyncHistory'
  });
};
