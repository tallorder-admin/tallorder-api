/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ArchiveTabPaymentsInstance, ArchiveTabPaymentsAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ArchiveTabPaymentsInstance, ArchiveTabPaymentsAttribute>('ArchiveTabPayments', {
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TenantName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    PaymentTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    PaymentDescr: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    PaymentRef: {
      type: DataTypes.CHAR(64),
      allowNull: true
    },
    Amount: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    Balance: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    Gratuity: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    ChangeGiven: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    FirstName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    LastName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    Role: {
      type: DataTypes.CHAR(40),
      allowNull: true,
      defaultValue: ''
    },
    Email: {
      type: DataTypes.CHAR(60),
      allowNull: true
    },
    Mobile: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    PhoneNo: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    TabID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    CustomerID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    LoyaltyProgRef: {
      type: DataTypes.CHAR(64),
      allowNull: true
    },
    MarketingPermissionFlag: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    CustFirstName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    CustLastName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    CustEmail: {
      type: DataTypes.CHAR(60),
      allowNull: true,
      defaultValue: ''
    },
    CustPhoneNo: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    Table: {
      type: DataTypes.CHAR(50),
      allowNull: true
    },
    HostTableID: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    TableAreaID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TableArea: {
      type: DataTypes.CHAR(50),
      allowNull: true
    },
    TabName: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    Party: {
      type: DataTypes.CHAR(64),
      allowNull: true
    },
    GuestCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    TabOpenTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    TabCloseTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    InvoiceNo: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: ''
    },
    HostTabID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HostWaiterID: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    HostWaiterName: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    TxID: {
      type: DataTypes.STRING(75),
      allowNull: true
    },
    TxNotes: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: '0000-00-00 00:00:00'
    },
    HostPaymentID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HostShiftID: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'ArchiveTabPayments'
  });
};
