/* jshint indent: 2 */
// tslint:disable
import * as sequelize from 'sequelize';
import {DataTypes} from 'sequelize';
import {ArchiveOrderLinesInstance, ArchiveOrderLinesAttribute} from './db';

module.exports = function(sequelize: sequelize.Sequelize, DataTypes: DataTypes) {
  return sequelize.define<ArchiveOrderLinesInstance, ArchiveOrderLinesAttribute>('ArchiveOrderLines', {
    ID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    TenantID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    TenantName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    OriginDeviceUID: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    MenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    AddOnID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ItemShortName: {
      type: DataTypes.STRING(30),
      allowNull: false,
      defaultValue: 'New item'
    },
    ItemName: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    CategoryID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    CategoryName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    BarCode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    BrandItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    BrandTenant: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LineNo: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: '0'
    },
    Price: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    Qty: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '1.00'
    },
    InvQty: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '1.00'
    },
    InvoiceNumber: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    GuestNum: {
      type: DataTypes.INTEGER(6),
      allowNull: true,
      defaultValue: '0'
    },
    OrderDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DeliveredDateTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    PrimaryMenuItemID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PrintCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    Note: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HostOrderID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LineTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    NetTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    TaxTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00000'
    },
    CostTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0.00'
    },
    TabID: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    HostTabID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HostWaiterID: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    HostWaiterName: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    TabName: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    WaitronID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    HostID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FirstName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    LastName: {
      type: DataTypes.CHAR(40),
      allowNull: true
    },
    Role: {
      type: DataTypes.CHAR(40),
      allowNull: true,
      defaultValue: ''
    },
    Email: {
      type: DataTypes.CHAR(60),
      allowNull: true
    },
    Mobile: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    PhoneNo: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    HostPLU: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    HostSalesDept: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    SaleType: {
      type: DataTypes.STRING(10),
      allowNull: true,
      defaultValue: 'Sale'
    },
    LastUpdate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: '0000-00-00 00:00:00'
    },
    Discounted: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    Voided: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    UnplacedVoid: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    UserCode: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    Returned: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    ParentOrderId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    IntegrityCheck: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    HostShiftID: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'ArchiveOrderLines'
  });
};
