import {HttpError} from "./util/Error";
import {Response} from "./util/Response";
import {Context} from "./util/Context";

exports.getAllDevicesHttp = (event, context, callback) => {
    const logger = Context.logger;
    logger.info('-------- deviceRegister function --------');
    logger.info(JSON.stringify(event, null, 2));
    logger.info(JSON.stringify(context, null, 2));
    context.callbackWaitsForEmptyEventLoop = false;

    try {

        if (!event.queryStringParameters || !event.queryStringParameters.tid) {
            throw new HttpError(400, 'Input', "tid not specified");
        }


        callback(null, Response.format("test"));

    } catch (error) {
        logger.info(`Error in execution ${error.toString()}`);
        callback(null, Response.error(error));
    }
};