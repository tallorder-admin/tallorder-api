import {TenantAccountSystem} from "./TenantAccountSystem";
import {GenericActions} from "../Generic";
import {Context} from "../../util/Context";
import {HttpError} from "../../util/Error";
import {Response} from "../../util/Response";


export const getTenantAccountSystem = (event, context, callback) => {
    const logger = Context.logger;
    logger.info('-------- tenantAccountSystem function --------');
    logger.info(JSON.stringify(event, null, 2));
    logger.info(JSON.stringify(context, null, 2));
    context.callbackWaitsForEmptyEventLoop = false;

    try {

        if (!event.pathParameters|| !event.pathParameters.tid) {
            throw new HttpError(400, 'Input', "tid not specified");
        }
        const tenantId: number = event.pathParameters.tid;

        GenericActions.bootstrap().then(() => {
            TenantAccountSystem.getAll(tenantId).then(tenantAccountSystem => {
                callback(null, Response.format(tenantAccountSystem));
            });
        });
    } catch (error) {
        logger.info(`Error in execution ${error.toString()}`);
        callback(null, Response.error(error));
    }
};