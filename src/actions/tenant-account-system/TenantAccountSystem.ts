import {GenericActions} from "../Generic";
import {Context} from "../../util/Context";

export class TenantAccountSystem {


    public static async getAll(tenantId: number): Promise<Array<TenantAccountSystem>> {
        Context.logger.debug(`Retrieving the TenantAccountSystem entries for tenant ${tenantId}`);
        return Context.tallorder.TenantAccountSystem.findAll({where: {TenantID: tenantId}});
    }


    public static async tenantAccountSystemView(tenantId: number, id: number, event, context, callback) {

        try {
            await GenericActions.bootstrap();

            const tenantAccountSystem = await Context.tallorder.TenantAccountSystem.findOne({where: {TenantID: tenantId, ID: id}});
            return callback(null, {
                statusCode: 200,
                body: JSON.stringify({
                    result: tenantAccountSystem,
                    type: "success",
                    reason: null
                }),
            });

        } catch (error) {
            return callback(null, {
                statusCode: 400,
                body: JSON.stringify({
                    result: null,
                    type: "error",
                    reason: error
                }),
            });
        }

    }

}

