import {Context} from "../util/Context";
import {HttpError} from "../util/Error";
import {GenericActions} from "./Generic";
import {DeviceRegister} from "./device-register/DeviceRegister";
import {Response} from "../util/Response";

export class MainRouter {

    logger = Context.logger;

    public static async getAllDevicesHttp(event, context, callback) {
        const logger = Context.logger;
        logger.info('-------- deviceRegister function --------');
        logger.info(JSON.stringify(event, null, 2));
        logger.info(JSON.stringify(context, null, 2));
        context.callbackWaitsForEmptyEventLoop = false;

        try {

            if (!event.queryStringParameters || !event.queryStringParameters.tid) {
                throw new HttpError(400, 'Input', "tid not specified");
            }
            const tenantId: number = event.queryStringParameters.tid;

            await GenericActions.bootstrap();

            const devices = await DeviceRegister.getAll(tenantId);

            callback(null, Response.format(devices));

        } catch (error) {
            logger.info(`Error in execution ${error.toString()}`);
            callback(null, Response.error(error));
        }
    }

}

exports.getAllDevicesHttp = MainRouter.getAllDevicesHttp;
