import {Context} from "../../util/Context";
import {ArrayHelper} from "../../util/ArrayHelper";

export class AccessController {

    public static async isPartnerAdmin(userId: number): Promise<boolean> {
        const user = Context.tallorder.User.findOne({where: {ID: userId, IsArchived: 0}});
        return ((user !== null) && (user['IsParAdmin'] == 1));
    }

    /**
     * Check what tenant access a non-admin user has access to
     * @param {number} userId
     * @param {number} tenantId
     * @returns {Promise<Array<string>>}
     */
    public static async userTenantAllowedActions(userId: number, tenantId: number): Promise<Array<string>> {

        Context.logger.debug(`Retrieving all Tenant allowed actions for user ${userId} and tenant ${tenantId}`);

        const tenant = await Context.tallorder.Tenants.findOne({where: {ID: tenantId}});
        if (tenant === null) {
            return [];
        }

        const user = await Context.tallorder.User.findOne({attributes: ['IsAdmin'], where: {ID: userId}});
        if(user.IsAdmin === 1) {
            return ['*'];
        }

        // 1. Check Tenant Type allowed
        let att: Array<string>;
        const accessTenantPermissions = await Context.tallorder.AccessPermissions.findAll({attributes: [[Context.sequelize.fn('concat', Context.sequelize.col('Controller'), '-', Context.sequelize.col('Action')),'ca']], where: {Allowed: 1, eType: 'eTenantType', eTypeID: tenant.TenantTypeID}, raw: true});
        if (accessTenantPermissions.length > 0) {
            att = ArrayHelper.getColumn(accessTenantPermissions, 'ca');
            // console.log('accessTenantPermissions',JSON.stringify(att));
        } else {
            return [];
        }

        // 2. Check Product Type allowed
        let apt: Array<string>;
        const accessProductPermissions = await Context.tallorder.AccessPermissions.findAll({attributes: [[Context.sequelize.fn('concat', Context.sequelize.col('Controller'), '-', Context.sequelize.col('Action')),'ca']], where: {Allowed: 1, eType: 'eProdType', eTypeID: tenant.ProdTypeID}, raw: true});
        if (accessProductPermissions.length > 0) {
            apt = (ArrayHelper.getColumn(accessProductPermissions, 'ca'));
            // console.log('accessProductPermissions',JSON.stringify(apt));
        } else {
            return [];
        }

        // 3. Check if Owner or Staff Type allowed
        let aut: Array<string>;
        const isPartnerAdmin = await this.isPartnerAdmin(userId);
        let eTypeID: string = '';
        if (isPartnerAdmin === true) {
            eTypeID = '48'; // Partner Admin
        } else if (tenant.UserID === userId) {
            eTypeID = '30'; // Owner
        } else {
            const staff = await Context.tallorder.Staff.findAll({where: {TenantID: tenantId, UserID: userId}, raw: true});
            eTypeID = ArrayHelper.getColumn(staff, 'eTypeID').toString();
        }
        const accessUserPermissions = await Context.tallorder.AccessPermissions.findAll({attributes: [[Context.sequelize.fn('concat', Context.sequelize.col('Controller'), '-', Context.sequelize.col('Action')),'ca']], where: {Allowed: 1, eType: 'eStaffType', eTypeID: [eTypeID]}, raw: true});
        if (accessUserPermissions.length > 0) {
            aut = (ArrayHelper.getColumn(accessUserPermissions, 'ca'));
            // console.log('accessUserPermissions',JSON.stringify(aut));
        } else {
            return [];
        }

        // Actions are allowed if common in all 3 types
        return ArrayHelper.containsAll(aut, apt, att);
    }

    /**
     * Check if user and tenant allowed access to specific action
     * @param {number} userId
     * @param {number} tenantId
     * @param {string} controller
     * @param {string} action
     */
    public static async isAllowedAccess(userId: number, tenantId: number, controller: string, action: string): Promise<boolean> {

        Context.logger.debug(`Checking access for user ${userId} and tenant ${tenantId} to controller ${controller} with action ${action}`);

        // If tenant not required then tenant access control not applicable
        const req = await Context.tallorder.AccessControllers.findOne({attributes: ['TenantRequired'], where: {Controller: controller}});
        if(req.TenantRequired < 1) {
            return true;
        }

        const tenant = await Context.tallorder.Tenants.findOne({where: {ID: tenantId}});
        if (tenant === null) {
            return false;
        }

        // Administrators have access to everything
        const user = await Context.tallorder.User.findOne({attributes: ['IsAdmin'], where: {ID: userId}});
        if(user.IsAdmin === 1) {
            return true;
        }

        // 1. Check Tenant Type allowed
        const accessTenantPermissions = await Context.tallorder.AccessPermissions.findOne({where: {Controller: controller, Action: action, Allowed: 1, eType: 'eTenantType', eTypeID: tenant.TenantTypeID}});
        if (accessTenantPermissions === null) {
            return false;
        }

        // 2. Check Product Type allowed
        const accessProductPermissions = await Context.tallorder.AccessPermissions.findOne({where: {Controller: controller, Action: action, Allowed: 1, eType: 'eProdType', eTypeID: tenant.ProdTypeID}});
        if (accessProductPermissions === null) {
            return false;
        }

        // 3. Check if Owner or Staff Type allowed
        const isPartnerAdmin = await this.isPartnerAdmin(userId);
        let eTypeID: string = '';
        if (isPartnerAdmin === true) {
            eTypeID = '48'; // Partner Admin
        } else if (tenant.UserID === userId) {
            eTypeID = '30'; // Owner
        } else {
            const staff = await Context.tallorder.Staff.findAll({where: {TenantID: tenantId, UserID: userId}, raw: true});
            eTypeID = ArrayHelper.getColumn(staff, 'eTypeID').toString();
            console.log('eTypeID',eTypeID);
        }
        const accessUserPermissions = await Context.tallorder.AccessPermissions.findOne({where: {Controller: controller, Action: action, Allowed: 1, eType: 'eStaffType', eTypeID: [eTypeID]}});
        return (accessUserPermissions !== null);
    }

}