import jwt = require("jsonwebtoken");
import bcrypt = require("bcryptjs");
import {Context} from "../../util/Context";

export class AuthController {

    private static _algorithm = 'HS384';
    private static _expiresIn = 86400; // expires in 24 hours

    private static async userFromBasicAuthString(authString : string): Promise<object> {
        const tmp = authString.split(' '); // Split on a space, the original auth looks like  "Basic Y2hhcmxlczoxMjM0NQ==" and we need the 2nd part
        const plain_auth = new Buffer(tmp[1], 'base64').toString(); // create a buffer and tell it the data coming in is base64, read it back out as a string

        console.log('plain_auth', plain_auth);

        // At this point plain_auth = "username:password"
        const creds = plain_auth.split(':'); // split on a ':'

        return {user: creds[0], pass: creds[1]};
    }

    /**
     * Encrypt a string
     * @param {number} saltRounds
     * @param {string} plainStr
     * @returns {Promise<string>}
     */
    public static encrypt(saltRounds : number, plainStr : string): Promise<string> {
        return bcrypt.hash(plainStr, saltRounds).then(function(hash) {
            return hash;
        });
    }

    /**
     * Get a User model by Email
     * @param {string} email
     * @param {string} passHash
     * @returns {Promise<object>}
     */
    public static async getUserByCreds(email: string, passHash: string): Promise<object> {
        Context.logger.debug(`Retrieving User entry for email ${email}`);
        const where = {Email: email, IsArchived: 0};
        if(passHash !==null) {
            where['pWord'] = passHash;
        }
        return Context.tallorder.User.findOne({where: where});
    }

    /**
     * Generate a password hash
     * @param {string} pWord
     * @returns {Promise<any>}
     */
    public static async generatePassHash(pWord: string): Promise<any> {

        const data = await Context.sequelize.query(`SELECT password('${pWord}') AS pHash`, { type: Context.sequelize.QueryTypes.SELECT});
        return data[0]['pHash'];
    }

    /**
     * Get a JWT Access Token
     * @param {string} app (Admin, POS etc.)
     * @param {string} auth (basic auth i.e. "Basic Y2hhcmxlczoxMjM0NQ==")
     * @returns {Promise<object>}
     */
    public static async getToken(app: string, auth: string): Promise<object> {

        Context.logger.debug(`Getting token for user with auth ${auth}`);

        const creds = await AuthController.userFromBasicAuthString(auth);
        if (creds['user'] === undefined || creds['pass'] === undefined) {
            throw new Error('Email or password undefined');
        }
        const email : string = creds['user'];
        const pass : string = creds['pass'];

        const passHash = await AuthController.generatePassHash(pass);
        const user = await AuthController.getUserByCreds(email,passHash);
        if(user===null) {
            throw new Error('Invalid user credentials');
        }

        // create a token
        const payload = { email: user['Email'], app: app };
        const jwtidString : string = user['ID']+user['Email']+app;
        const issuer : string = 'ID'+user['ID'];

        return new Promise(function (resolve, reject) {
            AuthController.encrypt(6,jwtidString)
                .then(function (jwtid) {
                    jwt.sign(payload, Context.auth['jwtKey'], {
                        expiresIn: AuthController._expiresIn, // expires in 24 hours
                        algorithm: AuthController._algorithm,
                        issuer: issuer, // User.ID
                        subject: app, // App
                        audience: user['Email'], // User.Email
                        jwtid: jwtid // JWT ID claim that provides a unique identifier for the JWT
                    }, function(err, token) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve({ auth: true, token: token });
                        }
                    });
                })
                .catch((err) => {
                    reject(err);
                });
        });

    }

    /**
     * Verify of JWT token
     * @param {string} bearerToken
     * @returns {Promise<object>}
     * TODO: You can use the context map to return cached credentials from the authorizer to the backend, using an integration request mapping template (event.requestContext.authorizer.key).
     * This enables the backend to provide an improved user experience by using the cached credentials to reduce the need to access the secret keys and open the authorization tokens for every request.
     */
    public static async verifyToken(bearerToken: string): Promise<any> {

        Context.logger.debug(`Verifying bearer token ${bearerToken}`);

        const tmp = bearerToken.split(' '); // Split on a space, the original auth looks like  "Bearer xxx" and we need the 2nd part
        const token = tmp[1];

        return new Promise(function (resolve, reject) {
            jwt.verify(token, Context.auth['jwtKey'], {
                algorithms: [AuthController._algorithm],
            }, async function(err, decoded) {
                if (err) {
                    reject(err);
                } else {
                    // Test if token is valid
                    const user = await AuthController.getUserByCreds(decoded['email'],null);
                    if(user !== null) {
                        const jwtidString : string = user['ID']+decoded['email']+decoded['app'];
                        bcrypt.compare(jwtidString, decoded['jti'], function(err, res) {
                            if (err) {
                                reject(err);
                            } else {
                                if(res === true) {
                                    resolve({valid: true, token: decoded });
                                } else {
                                    reject('Invalid token content');
                                }
                            }
                        });
                    } else {
                        reject('Invalid token');
                    }
                }
            });
        });
    }

    /**
     * Returns an IAM policy document for a given user and resource.
     *
     * @method buildIAMPolicy
     * @param {String} userId - user id
     * @param {String} effect  - Allow / Deny
     * @param {String} resource - resource ARN
     * @param {String} custom - response context
     * @returns {Object} policyDocument
     */
    public static async generatePolicy(userId:string, effect:string, resource:string, custom:string): Promise<object> {
        const logger = Context.logger;
        logger.info(`generating policy for ${userId}`);
        const policy = {
            principalId: userId,
            policyDocument: {
                Version: '2012-10-17',
                Statement: [
                    {
                        Action: 'execute-api:Invoke',
                        Effect: effect,
                        Resource: resource,
                    },
                ],
            },
            "context" : custom
        };

        logger.info('Policy ' + JSON.stringify(policy));
        return policy;
    }



}