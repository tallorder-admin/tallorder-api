import {AuthController} from "./AuthController";
import {Context} from "../../util/Context";
import {HttpError} from "../../util/Error";
import {Response} from "../../util/Response";
import {GenericActions} from "../Generic";
import {AccessController} from "./AccessController";

/**
 * Generate a new token
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<void>}
 */
exports.getTokenHttp = async (event, context, callback) => {
    const logger = Context.logger;
    logger.info('-------- AuthController getTokenHttp function --------');
    logger.info(JSON.stringify(event, null, 2));
    logger.info(JSON.stringify(context, null, 2));
    context.callbackWaitsForEmptyEventLoop = false;

    try {

        if (!event.headers.Authorization || !event.queryStringParameters.app) {
            throw new HttpError(400, 'Input', "credentials not specified");
        }
        const app: string = event.queryStringParameters.app;
        const auth: string = event.headers.Authorization;

        await GenericActions.bootstrap(['tallorder']);
        const token = await AuthController.getToken(app,auth);
        callback(null, Response.format(token,{'x-auth-token': token['token']}));

    } catch (error) {
        logger.info(`Error in execution ${error.toString()}`);
        callback(null, Response.error(error));
    }
};

/**
 * Verify an existing token
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<void>}
 */
exports.verifyTokenHttp = async (event, context, callback) => {
    const logger = Context.logger;
    logger.info('-------- AuthController verifyTokenHttp function --------');
    logger.info(JSON.stringify(event, null, 2));
    logger.info(JSON.stringify(context, null, 2));
    context.callbackWaitsForEmptyEventLoop = false;

    try {

        if (!event.headers.Authorization) {
            throw new HttpError(400, 'Input', "Token not set");
        }

        const token: string = event.headers.Authorization;

        await GenericActions.bootstrap(['tallorder']);

        const verify = await AuthController.verifyToken(token);
        callback(null, Response.format(verify));

    } catch (error) {
        logger.info('Error in execution ' + JSON.stringify(error));
        callback(null, Response.error(error));
    }

};

/**
 * AWS Custom Authorizer
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<void>}
 */
exports.authorizer = async (event, context, callback) => {
    const logger = Context.logger;
    logger.info('-------- AuthController authorizer function --------');
    logger.info(JSON.stringify(event, null, 2));
    logger.info(JSON.stringify(context, null, 2));
    context.callbackWaitsForEmptyEventLoop = false;

    try {

        if (!event.authorizationToken) {
            throw new HttpError(400, 'Input', "Token not set");
        }

        const token: string = event.authorizationToken;

        await GenericActions.bootstrap(['tallorder']);

        const verify = await AuthController.verifyToken(token);
        const userId : string = verify['token']['sub']+verify['token']['email'];
        logger.info(`token valid generating policy for ${userId}`);
        // const policy = await AuthController.generatePolicy(userId, 'Allow', event.methodArn, context);
        const policy = await AuthController.generatePolicy(userId, 'Allow', "arn:aws:execute-api:*", verify['token']);
        callback(null, policy);

    } catch (error) {
        logger.info(`Error in execution ${error.toString()}`);
        // callback(null, Response.error(error));
        context.fail(error.message);
    }

};

/**
 * Generate a password hash
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<void>}
 */
exports.generatePassHashHttp = async (event, context, callback) => {
    const logger = Context.logger;
    logger.info('-------- AuthController generatePassHashHttp function --------');
    logger.info(JSON.stringify(event, null, 2));
    logger.info(JSON.stringify(context, null, 2));
    context.callbackWaitsForEmptyEventLoop = false;

    try {

        if (!event.queryStringParameters || !event.queryStringParameters.pass) {
            throw new HttpError(400, 'Input', "Password not specified");
        }
        const pass: string = event.queryStringParameters.pass;

        await GenericActions.bootstrap(['tallorder']);
        const hash = await AuthController.generatePassHash(pass);
        callback(null, Response.format({hash : hash}));

    } catch (error) {
        logger.info(`Error in execution ${error.toString()}`);
        callback(null, Response.error(error));
    }

};

/**
 * Check what tenant access a non-admin user has access to
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<void>}
 */
exports.getUserTenantAccessHttp = async (event, context, callback) => {
    const logger = Context.logger;
    logger.info('-------- AuthController generatePassHashHttp function --------');
    logger.info(JSON.stringify(event, null, 2));
    logger.info(JSON.stringify(context, null, 2));
    context.callbackWaitsForEmptyEventLoop = false;

    try {
        if (!event.requestContext.authorizer.iss) {
            throw new HttpError(400, 'Input', "This function requires authorizer to be set");
        }
        Context.userId = event.requestContext.authorizer.iss.slice(2);

        if (!event.queryStringParameters || !event.queryStringParameters.tid) {
            throw new HttpError(400, 'Input', "tid not specified");
        }

        const tenantId: number = event.queryStringParameters.tid;

        await GenericActions.bootstrap(['tallorder']);
        const access = await AccessController.userTenantAllowedActions(Context.userId,tenantId);
        callback(null, Response.format(access));

    } catch (error) {
        logger.info(`Error in execution ${error.toString()}`);
        callback(null, Response.error(error));
    }

};
