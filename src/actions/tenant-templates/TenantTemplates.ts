import {GenericActions} from "../Generic";
import {Context} from "../../util/Context";

export class TenantTemplates {


    public static async getAll(tenantId: number): Promise<Array<TenantTemplates>> {
        Context.logger.debug(`Retrieving the tenantTemplates entries for tenant ${tenantId}`);
        return Context.tallorder.TenantTemplates.findAll({where: {TenantID: tenantId}});
    }

    public static async tenantTemplatesView(tenantId: number, id: number, event, context) {

        try {
            await GenericActions.bootstrap();
            Context.logger.debug(`Retrieving the tenantTemplates entries with ID ${id} for tenant ${tenantId}`);

            return await Context.tallorder.TenantTemplates.findOne({where: {TenantID: tenantId, ID: id}});

        } catch (error) {
            return error;
        }

    }

}

