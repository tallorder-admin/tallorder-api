import {TenantTemplates} from "./TenantTemplates";
import {GenericActions} from "../Generic";
import {Context} from "../../util/Context";
import {HttpError} from "../../util/Error";
import {Response} from "../../util/Response";


export const getTenantTemplates = (event, context, callback) => {
    const logger = Context.logger;
    logger.info('-------- tenantTemplates function --------');
    logger.info(JSON.stringify(event, null, 2));
    logger.info(JSON.stringify(context, null, 2));
    context.callbackWaitsForEmptyEventLoop = false;

    try {

        if (!event.pathParameters|| !event.pathParameters.tid) {
            throw new HttpError(400, 'Input', "tid not specified");
        }
        const tenantId: number = event.pathParameters.tid;

        switch(event.httpMethod) {
            case 'POST': {
                if (!event.pathParameters|| !event.pathParameters.id) {
                    throw new HttpError(400, 'Input', "id not specified");
                }
                const rowId: number = event.pathParameters.id;
                GenericActions.bootstrap().then(() => {
                    TenantTemplates.tenantTemplatesView(tenantId, rowId, event, context).then(tenantTemplates => {
                        callback(null, Response.format(tenantTemplates));
                    });
                });
                break;
            }
            case 'GET': {
                Context.logger.debug(`Get function for Tenant ${tenantId}`);
                GenericActions.bootstrap().then(() => {
                    TenantTemplates.getAll(tenantId).then(tenantTemplates => {
                        callback(null, Response.format(tenantTemplates));
                    });
                });
                break;
            }
            default: {
                break;
            }

        }

    } catch (error) {
        logger.info(`Error in execution ${error.toString()}`);
        callback(null, Response.error(error));
    }
};