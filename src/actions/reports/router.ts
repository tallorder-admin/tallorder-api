import {Reports} from "./Reports";
import {GenericActions} from "../Generic";
import {Context} from "../../util/Context";
import {HttpError} from "../../util/Error";
import {Response} from "../../util/Response";

export const getSales = async (event, context, callback) => {
    const logger = Context.logger;
    logger.info('-------- getSales function --------');
    logger.info(JSON.stringify(event, null, 2));
    logger.info(JSON.stringify(context, null, 2));
    context.callbackWaitsForEmptyEventLoop = false;

    try {

        if (!event.queryStringParameters || !event.queryStringParameters.tid) {
            throw new HttpError(400, 'Input', "tid not specified");
        }

        if (!event.queryStringParameters || !event.queryStringParameters.start) {
            throw new HttpError(400, 'Input', "start date not specified");
        }

        if (!event.queryStringParameters || !event.queryStringParameters.end) {
            throw new HttpError(400, 'Input', "end date not specified");
        }

        const tenantId: number = event.queryStringParameters.tid;
        const start: string = event.queryStringParameters.start;
        const end: string = event.queryStringParameters.end;
        const period: string = event.queryStringParameters.period;

        await GenericActions.bootstrap();
        const sales = await Reports.getProductSales(tenantId, period, start, end);
        callback(null, Response.format(sales));

    } catch (error) {
        logger.info(`Error in execution ${error.toString()}`);
        callback(null, Response.error(error));
    }
};
