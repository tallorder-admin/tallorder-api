import {GenericActions} from "../Generic";
import {Context} from "../../util/Context";

export class Reports {

    public static async getProductSales(tenant: number, period: string, start: string, end: string) {

        let data = [];

        switch(period) {
            case "hourly":
                data = await Context.tallorderBI.HourlyOrders.findAll({
                    where: {
                        TenantID: tenant,
                        RollupDate: {
                            $between: [start, end]
                        }
                    }
                });
                break;
            case "daily":
                data = await Context.tallorderBI.DailyOrders.findAll({
                    where: {
                        TenantID: tenant,
                        RollupDate: {
                            $between: [start, end]
                        }
                    }
                });
                break;
            case "monthly":
                data = await Context.tallorderBI.MonthlyOrders.findAll({
                    where: {
                        TenantID: tenant,
                        RollupDate: {
                            $between: [start, end]
                        }
                    }
                });
                break;
            default:
                data = await Context.tallorderBI.ArchiveOrderLines.findAll({
                    where: {
                        TenantID: tenant,
                        RollupDate: {
                            $between: [start, end]
                        }
                    }
                });
                break;
        }
        return data;
    }

}