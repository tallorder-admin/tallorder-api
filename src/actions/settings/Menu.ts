import {Context} from "../../util/Context";
import {MenuSettings} from "./menu_settings/MenuSettings";
import {MenuItem} from "./menu_settings/MenuItem";
import {StockLevel} from "./menu_settings/StockLevel";
import {ItemLocal} from "./menu_settings/ItemLocal";
import {TaxGroup} from "./menu_settings/TaxGroup";
import {StockItem} from "./menu_settings/StockItem";

const logger = Context.logger;

const snakeCaseKeys: any = require('snakecase-keys');

export class Menu {


    public static async publishMenu(tenantId: number, menuId: number) {
        Context.logger.debug(`Publishing menu ${menuId} - tid=${tenantId}`);

        const menuTreeSequalizeModelRef: object = {
            model: Context.tallorder.MenuTree,
            include: [{
                model: Context.tallorder.MenuItems,
                include: [{
                    model: Context.tallorder.MenuAddOns,
                }, {
                    model: Context.tallorder.MenuOptions,
                }, {
                    model: Context.tallorder.TaxGroups,
                    include: [{
                        model: Context.tallorder.TaxTypeLinks,
                        include: [{
                            model: Context.tallorder.TaxTypes
                        }]
                    }]
                }, {
                    model: Context.tallorder.StockItems,
                    include: [{
                        model: Context.tallorder.StockItemSuppliers,
                        include: [{
                            model: Context.tallorder.TenantSuppliers
                        }, {
                            model: Context.tallorder.StockItemSupplierQuantities,
                            include: [{
                                model: Context.tallorder.StockItemUnits,
                                include: [{
                                    model: Context.tallorder.StockUnits
                                }]
                            }]
                        }, {
                            model: Context.tallorder.StockItemUnits,
                            include: [{
                                model: Context.tallorder.StockUnits
                            }]
                        }]
                    }, {
                        model: Context.tallorder.StockItemStockTakeTypes,
                        include: [{
                            model: Context.tallorder.StockTakeTypes
                        }]
                    }, {
                        model: Context.tallorder.StockUnits
                    }]
                }, {
                    model: Context.tallorder.StockLevels,
                }, {
                    model: Context.tallorder.MenuItemCategory,
                }, {
                    model: Context.tallorder.Descriptions,
                }, {
                    model: Context.tallorder.ItemLocal,
                }, {
                    model: Context.tallorder.MenuItemTags,
                    include: [{
                        model: Context.tallorder.Tag
                    }]
                }, {
                    model: Context.tallorder.MenuPortions,
                }]
            }]
        };

        const menu = await Context.tallorder.Menus.findOne({
            where: {
                TenantID: tenantId,
                ID: menuId
            },
            include: [
                menuTreeSequalizeModelRef
            ]
        });

        const inventoryTrees = await Context.tallorder.MenuTree.findAll({
            where: {
                TenantID: tenantId,
                MenuID: {
                    $eq: null
                }
            },
            menuTreeSequalizeModelRef

        });


        const menuSettings = await this.buildMenuSettings(menu, inventoryTrees);

        const snaked_menu_settings = snakeCaseKeys(menuSettings);

        return true;

    }


    private static async buildMenuSettings(menu: any, inventoryTrees: any) {

        logger.debug("buildMenuSettings", "building Menu Settings");
        const menuSettings: MenuSettings = new MenuSettings();
        menuSettings.id = menu.dataValues.ID;
        menuSettings.lastMenuPublish = menu.dataValues.LastMenuChange.toISOString();
        menuSettings.latestVersion = menu.dataValues.LatestVersion;
        menuSettings.name = menu.dataValues.Name;
        menuSettings.tenantId = menu.dataValues.TenantID;


        const menuTrees = menu.MenuTrees;
        menuTrees.concat(inventoryTrees);


        const taxGroupDB = await Context.tallorder.TaxGroups.findOne({
            where: {
                TenantID: menu.dataValues.TenantID
            },
            include: [{
                model: Context.tallorder.TaxTypeLinks,
                include: [{
                    model: Context.tallorder.TaxTypes
                }]
            }]
        });

        const taxGroup: TaxGroup = new TaxGroup(taxGroupDB);

        const menuItemMap: object = {};
        const hashItemMap: object = {};
        const recipeItemMap: object = {};
        const addOnMenuItemMap: object = {};


        menuTrees.forEach(menuTree => {

            let menuItem: MenuItem = this.buildMenuItem(menuTree);

            menuItem.itemId = menuTree.dataValues.ID;
            menuItem.parentId = menuTree.dataValues.MenuID == null ? null : 0;
            menuItem.inventoryParentId = menuItem.type == "inv-cat" ? 0 : null;
            menuItem.sequence = menuTree.dataValues.Seq;


            if (menuItem.type === "cat" || menuItem.type === "inv-cat")
                menuItem.price = 0.0;


            if (!menuItem.itemLocal) {
                menuItem.itemLocal = new ItemLocal({});

                menuItem.itemLocal.id = menuItem.id;
                menuItem.itemLocal.isActive = true;
                menuItem.itemLocal.mgrAuthRequired = false;
                menuItem.itemLocal.tabAuthRequired = false;
                menuItem.itemLocal.isAvailable = true;
                menuItem.itemLocal.waitronAlert = "";
                menuItem.itemLocal.guestAlert = "";
                menuItem.itemLocal.specialFlag = false;
                menuItem.itemLocal.sqFlag = false;
            }


            /*
             * Check if menuItem has a TaxGroup, if not then allocate a default
             */

            if (!menuItem.taxGroup) {
                menuItem.taxGroup = taxGroup;
            }

            /*
             * Now add all the add-ons to MenuItems as well
             */

            menuTree.MenuItem.MenuAddOns.forEach(menuAddOn => {
                menuAddOn.AddOnGroup.AddOns.forEach(addOn => {

                    const addOnMenuItem = this.buildMenuItem(addOn.Item);
                    addOnMenuItemMap[addOnMenuItem.id] = addOnMenuItem;

                });
            });

            const recipes = this.getRecipeItems(menuTree.MenuItem);
            Object.keys(recipes).forEach(key => {
                recipeItemMap[key] = recipes[key];
            });


            if (menuTree.dataValues.ParentID) {


                if (menuTree.dataValues.ParentID != 0) {
                    let treeExists: boolean = false;

                    menuTrees.forEach(possibleParentMenuTree => {
                        if (possibleParentMenuTree.dataValues.ID === menuTree.dataValues.ParentID) {
                            treeExists = true;
                        }
                    });

                    if (treeExists) {
                        if (menuTree.dataValues.MenuID) {

                            // this is a normal menu parent
                            menuItem.parentId = menuTree.dataValues.ParentID;

                        } else {
                            // this is an inventory menu parent
                            menuItem.inventoryParentId = menuTree.dataValues.ParentID;
                        }
                    } else {
                        logger.error(`NO VALID PARENT FOUND FOR MENUTREE ${menuTree.dataValues.ParentID}`);
                    }


                }
            } else {
                /*
                 * Parent ID is 0
                 * - The item is in the main menu on the main level OR
                 * - The item is on the inventory menu on the main level
                 */
                if (!menuTree.dataValues.MenuID) {

                    const previous: MenuItem = menuItemMap[menuTree.dataValues.ID + "" + menuItem.id];
                    if (previous) {
                        // this item has already been processed, so it appears in the menu and inventory menu
                        // rather use the processed one and retain both parent values
                        menuItem = previous;
                    } else {
                        // Item has not been processed before so sofar this is only an Inventory item
                        // set the normal menu parent to null.
                        menuItem.parentId = null;
                    }

                    // this is an inventory item on the main level
                    menuItem.inventoryParentId = 0;

                }


            }


            menuItemMap[menuTree.dataValues.ID + "" + menuItem.id] = menuItem;
            hashItemMap[menuItem.id] = menuItem;

        });


        Object.keys(recipeItemMap).forEach(key => {
            if (!hashItemMap[key]) {
                if (!menuItemMap[key]) {
                    menuItemMap[key] = recipeItemMap[key];
                }
            }
        });


        Object.keys(addOnMenuItemMap).forEach(key => {
            if (!hashItemMap[key]) {
                if (!menuItemMap[key]) {
                    menuItemMap[key] = addOnMenuItemMap[key];
                }
            }
        });


        const menuItems = new Array<MenuItem>();
        Object.values(menuItemMap).forEach(item => {
            menuItems.push(item);
        });

        menuSettings.menuItems = menuItems.sort((item1, item2) => item1.id - item2.id);

        return menuSettings;
    }

    private static buildMenuItem(menuTree: any): MenuItem {

        const menuItemDB = menuTree.MenuItem;
        logger.debug("buildMenuSettings", `building Menu Item ${menuItemDB.ID}`);

        const menuItem: MenuItem = new MenuItem();


        menuItem.id = menuItemDB.dataValues.ID;
        menuItem.tenant = menuItemDB.dataValues.TenantID;

        // these all need to be arrays
        menuItem.recipeIngredients = [];
        menuItem.addOnGroups = [];
        menuItem.optionGroups = [];
        menuItem.portionGroups = [];

        //
        if (menuItemDB.dataValues.StockLevel)
            menuItem.stockLevel = new StockLevel(menuItemDB.dataValues.StockLevel.dataValues);

        //
        if (menuItemDB.dataValues.StockItem)
            menuItem.stockItem = new StockItem(menuItemDB.dataValues.StockItem.dataValues);

        menuItem.shortName = menuItemDB.dataValues.ShortName;
        menuItem.tags = menuItemDB.dataValues.Tags;
        menuItem.printSlipName = menuItemDB.dataValues.PrintSlipName;
        menuItem.parentId = menuItemDB.dataValues.ParentID;
        menuItem.itemId = menuItemDB.dataValues.ItemID;

        // set from type inv.
        menuItem.isInventoryHeading = menuItemDB.dataValues.IsInventoryHeading;

        //
        if (menuItemDB.dataValues.ItemLocal)
            menuItem.itemLocal = new ItemLocal(menuItemDB.dataValues.ItemLocal.dataValues);


        //
        if (menuItemDB.dataValues.Description) {

            menuItem.description = {
                id: menuItemDB.dataValues.Description.dataValues.ID,
                brandItemId: menuItemDB.dataValues.Description.dataValues.BrandItemID,
                language: menuItemDB.dataValues.Description.dataValues.LangID,
                itemName: menuItemDB.dataValues.Description.dataValues.ItemName,
                description: menuItemDB.dataValues.Description.dataValues.Description,
                nutrition: menuItemDB.dataValues.Description.dataValues.Nutrition,
                url: menuItemDB.dataValues.Description.dataValues.URL
            };
        }

        //
        if (menuItemDB.dataValues.MenuItemCategory) {
            menuItem.category = {
                id: menuItemDB.dataValues.MenuItemCategory.dataValues.ID,
                name: menuItemDB.dataValues.MenuItemCategory.dataValues.CategoryName,
                sequence: menuItemDB.dataValues.MenuItemCategory.dataValues.Seq
            };
        }

        menuItem.sequence = menuItemDB.dataValues.Seq;
        menuItem.isModOnly = menuItemDB.dataValues.IsModOnly;
        menuItem.isIngredientOnly = menuItemDB.dataValues.IsIngredOnly;
        menuItem.isProduced = menuItemDB.dataValues.IsProduced;
        menuItem.unit = menuItemDB.dataValues.Unit;
        menuItem.brandItemId = menuItemDB.dataValues.BrandItemID;
        menuItem.shortCutId = menuItemDB.dataValues.ShortcutID;
        menuItem.cost = menuItemDB.dataValues.Cost;
        menuItem.price = menuItemDB.dataValues.Price;
        menuItem.priceVar = menuItemDB.dataValues.PriceVar;
        menuItem.isTaxable = menuItemDB.dataValues.IsTaxable;

        //
        if (menuItemDB.dataValues.TaxGroup) {
            menuItem.taxGroup = new TaxGroup(menuItemDB.dataValues.TaxGroup.dataValues);
        }


        menuItem.allowLocalPrice = menuItemDB.dataValues.AllowLocalPrice;
        menuItem.reqMgrSignOff = menuItemDB.dataValues.ReqMgrSignOff;
        menuItem.notes = menuItemDB.dataValues.Notes;
        menuItem.type = menuItemDB.dataValues.ItemType;
        menuItem.prepTime = menuItemDB.dataValues.PrepTime;
        menuItem.serveFrom = menuItemDB.dataValues.ServeFrom;
        menuItem.serveUntil = menuItemDB.dataValues.ServeUntil;
        menuItem.serveSunday = menuItemDB.dataValues.SrvSu;
        menuItem.serveMonday = menuItemDB.dataValues.SrvMo;
        menuItem.serveTuesday = menuItemDB.dataValues.SrvTu;
        menuItem.serveWednesday = menuItemDB.dataValues.SrvWe;
        menuItem.serveThursday = menuItemDB.dataValues.SrvTh;
        menuItem.serveFriday = menuItemDB.dataValues.SrvFr;
        menuItem.serveSaturday = menuItemDB.dataValues.SrvSa;
        menuItem.hostPlu = menuItemDB.dataValues.HostPLU;
        menuItem.barCode = menuItemDB.dataValues.BarCode;
        menuItem.hostSalesDept = menuItemDB.dataValues.HostSalesDept;


        menuItem.promoServiceGroupId = menuItemDB.dataValues.PromoServiceGroupID;
        menuItem.waiterOnly = menuItemDB.dataValues.WaiterOnly;
        menuItem.availableTogo = menuItemDB.dataValues.AvailableToGo;
        menuItem.availableTa = menuItemDB.dataValues.AvailableTA;
        menuItem.availableSelfService = menuItemDB.dataValues.AvailableSelfService;
        menuItem.autoAcceptTogo = menuItemDB.dataValues.AutoAcceptToGo;
        menuItem.req86Countdown = menuItemDB.dataValues.Req86Countdown;

        logger.debug("buildMenuSettings", `completed building Menu Item ${menuItemDB.ID}`);

        return menuItem;

    }


    private static getRecipeItems(menuItem: any): object {

        const recipeMap = {};

        if (menuItem.RecipeIngredients)
            menuItem.RecipeIngredients.forEach(recipeIngredientDB => {
                if (recipeIngredientDB.IngredientMenuItem) {
                    const recipeIngredient: MenuItem = this.buildMenuItem(recipeIngredientDB.IngredientMenuItem);
                    recipeMap[recipeIngredient.id] = recipeIngredient;
                    if (recipeIngredientDB.IngredientMenuItem.RecipeIngredients &&
                        recipeIngredientDB.IngredientMenuItem.RecipeIngredients.length > 0) {
                        const nextRecipes = this.getRecipeItems(recipeIngredientDB.IngredientMenuItem);
                        Object.keys(nextRecipes).forEach(key => {
                            recipeMap[key] = nextRecipes[key];
                        });
                    }
                }
            });

        return recipeMap;
    }


}

