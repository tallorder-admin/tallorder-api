import {GenericActions} from "../Generic";
import {Context} from "../../util/Context";
import {HttpError} from "../../util/Error";
import {Response} from "../../util/Response";
import {Menu} from "./Menu";


exports.getSettingsHttp = async (event, context, callback) => {
    const logger = Context.logger;
    logger.info('-------- getSettingsHttp function --------');
    logger.info(JSON.stringify(event, null, 2));
    logger.info(JSON.stringify(context, null, 2));
    context.callbackWaitsForEmptyEventLoop = false;

    try {

        if (!event.queryStringParameters || !event.queryStringParameters.tid) {
            throw new HttpError(400, 'Input', "tid not specified");
        }
        const tenantId: number = event.queryStringParameters.tid;

        await GenericActions.bootstrap();

        Menu.publishMenu(31, 84);

        callback(null, Response.format(''));

    } catch (error) {
        logger.info(`Error in execution ${error.toString()}`);
        callback(null, Response.error(error));
    }
};

exports.getSettingsHttp({
    queryStringParameters:{
        tid:21
    }
}, {}, () => {
    console.log('ok');
});
