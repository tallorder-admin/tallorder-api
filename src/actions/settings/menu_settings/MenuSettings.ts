import {Context} from "../../../util/Context";

import {MenuItem} from './MenuItem';


export class MenuSettings {


    id: number;

    tenantId: number;

    name: string;

    latestVersion: string;

    menuItems: Array<MenuItem>;

    lastMenuPublish: string;

    addOnGroups: Array<{id:number, menuItemId: number, addOnId: number}>;

}