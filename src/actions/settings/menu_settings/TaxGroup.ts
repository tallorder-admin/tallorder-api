export class TaxGroup {


    constructor(obj: any) {

        this.id = obj.ID;
        this.taxGroupName = obj.TaxGroupName;
        obj.TaxTypeLinks.forEach(taxTypeLink => {

            this.taxTypes = new Array();
            this.taxTypes.push({

                id: taxTypeLink.TaxType.dataValues.ID,
                regime: taxTypeLink.TaxType.dataValues.TaxRegime,
                taxType: taxTypeLink.TaxType.dataValues.TaxType,
                seq: taxTypeLink.TaxType.dataValues.Seq,
                inclusive: taxTypeLink.TaxType.dataValues.InclusiveTax,
                rate: taxTypeLink.TaxType.dataValues.TaxRate,
                reference: taxTypeLink.TaxType.dataValues.TaxReference,
                hostTaxId: taxTypeLink.TaxType.dataValues.HostTaxID,
                startDate: taxTypeLink.TaxType.dataValues.TaxStartDate,
                endDate: taxTypeLink.TaxType.dataValues.TaxEndDate

            });
        });
        this.defaultGroup = obj.DefaultGroup;
    }

    id: number;
    taxGroupName: string;
    taxTypes: Array<{
        id: number,
        regime: any,
        taxType: string,
        seq: number,
        inclusive: boolean,
        rate: number,
        reference: string,
        hostTaxId: string,
        startDate: string,
        endDate: string
    }>;
    defaultGroup: boolean;
}