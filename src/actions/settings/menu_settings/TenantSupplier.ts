import {Address} from "./Address";
import {GlobalSupplier} from "./GlobalSupplier";

export class TenantSupplier {

    constructor(obj: any) {
        this.id = obj.ID;

        this.globalSupplier = new GlobalSupplier(obj.GlobalSupplier.dataValues);

        this.name = obj.Name;
        this.contactPerson = obj.ContactPerson;
        this.phone = obj.Phone;
        this.email = obj.Email;
        this.website = obj.Website;
        this.termsAndConditions = obj.TermsAndConditions;
        this.creditLimit = obj.CreditLimit;
        this.accountingSystemLink = obj.AccountingSystemLink;

        this.address = new Address(obj.Address.dataValues);
    }

    id: number;

    globalSupplier: GlobalSupplier;

    name: string;

    contactPerson: string;

    phone: string;

    email: string;

    website: string;

    termsAndConditions: string;

    creditLimit: number;

    accountingSystemLink: string;

    address: Address;

}