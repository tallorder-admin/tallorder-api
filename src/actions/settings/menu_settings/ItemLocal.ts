
export class ItemLocal {


    constructor(obj: any) {
        this.id = obj.ID;
        this.isActive = obj.IsActive;
        this.mgrAuthRequired = obj.MgrAuthRequired;
        this.tabAuthRequired = obj.TabAuthRequired;
        this.isAvailable = obj.IsAvailable;
        this.suspendToGo = obj.SuspendToGo;
        this.waitronAlert = obj.WaitronAlert;
        this.guestAlert = obj.GuestAlert;
        this.localPrice = obj.LocalPrice;
        this.specialFlag = obj.SpecialFlag;
        this.sqFlag = obj.SQFlag;
        this.specialPrice = obj.SpecialPrice;
        this.sqPrice = obj.SQPrice;
        this.stationId = obj.StationID;
        this.stationDeviceId = obj.StationDeviceID;
        this.wtrnPoints = obj.WtrnPoints;
        this.wtrnPointsEndDate = obj.WtrnPointsEndDate;
        this.wtrnComPercent = obj.WtrnComPercent;
        this.wtrnComValue = obj.WtrnComValue;
        this.wtrnComEndDate = obj.WtrnComEndDate;
    }

    id: number;
    isActive: boolean;
    mgrAuthRequired: boolean;

    tabAuthRequired: boolean;

    isAvailable: boolean;

    suspendToGo: boolean;


    waitronAlert: string;

    guestAlert: string;


    localPrice: number;

    specialFlag: boolean;

    sqFlag: boolean;

    specialPrice: number;
    sqPrice: number;
    stationId: number;
    stationDeviceId: number;
    wtrnPoints: number;
    wtrnPointsEndDate: string;
    wtrnComPercent: number;
    wtrnComValue: number;
    wtrnComEndDate: string;
}