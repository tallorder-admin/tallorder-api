

export class AppEnum {

    id: number;

    eGroup: string;

    eValue: string;

    eText: string;

    seq: number;

}