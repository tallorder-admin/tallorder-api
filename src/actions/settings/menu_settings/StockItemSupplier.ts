import {TenantSupplier} from "./TenantSupplier";
import {StockUnit} from "./StockUnit";
import {StockItemSupplierQuantities} from "./StockItemSupplierQuantities";

export class StockItemSupplier {


    constructor(obj: any) {
        this.id = obj.ID;

        this.tenantSupplier = new TenantSupplier(obj.TenantSupplier);

        this.preferredSupplier = obj.PreferredSupplier;
        this.economicOrderQuantity = obj.EconomicOrderQuantity;

        this.stockItemUnit = {

            id: obj.StockItemUnit.ID,
            stockUnit: new StockUnit(obj.StockItemUnit.dataValues),
            stockItemSupplierQuantities: obj.StockItemUnit.StockItemSupplierQuantities.dataValues

        };


        this.rank = obj.Rank;

        this.quantities = [];

            obj.StockItemSupplierQuantities.forEach(stockItemSupplierQuantities=> {

                this.quantities.push(new StockItemSupplierQuantities(stockItemSupplierQuantities.dataValues));

            });


        this.averageReplenishmentCycleDays = obj.AverageReplenishmentCycleDays;
        this.lastCostPerBaseUnit = obj.LastCostPerBaseUnit;
    }

    id: number;

    tenantSupplier: TenantSupplier;
    preferredSupplier: number;
    economicOrderQuantity: number;
    stockItemUnit: { id: number, stockUnit: StockUnit, stockItemSupplierQuantities: StockItemSupplierQuantities };
    rank: number;
    quantities: Array<StockItemSupplierQuantities>;
    averageReplenishmentCycleDays: number;
    lastCostPerBaseUnit: number;
}