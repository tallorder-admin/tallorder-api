export class Address {


    constructor(obj: any) {
        this.id = obj.id;
        this.ownerType = obj.ownerType;
        this.addType = obj.addType;
        this.addLine1 = obj.addLine1;
        this.addLine2 = obj.addLine2;
        this.addLine3 = obj.addLine3;
        this.city = obj.city.dataValues;
        this.zip = obj.zip;
        this.country = obj.country;
        this.posLat = obj.posLat;
        this.posLong = obj.posLong;
    }

    id: number;
    ownerType: string;
    addType: string;
    addLine1: string;
    addLine2: string;
    addLine3: string;
    city: string;
    zip: string;
    country: object;
    posLat:number;
    posLong: number;
}