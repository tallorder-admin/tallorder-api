
import { RecipeIngredient } from './RecipeIngredient';
import { StockLevel } from './StockLevel';
import { StockUnit} from './StockUnit';
import { ItemLocal} from './ItemLocal';
import { TaxGroup} from './TaxGroup';
import {StockItem} from "./StockItem";




export class MenuItem {

    id: number;

    tenant: number;

    recipeIngredients: Array<RecipeIngredient>;

    addOnGroups: Array<number>;

    optionGroups: Array<number>;

    portionGroups: Array<number>;

    stockLevel: StockLevel;

    stockItem: StockItem;

    tags: Array<{id: number, description: string, tagDescriptionStringId: number }>;

    printSlipName: string;

    parentId: number;

    itemId: number;

    isInventoryHeading: boolean;

    inventoryParentId: number;

    shortName: string;

    itemLocal: ItemLocal;

    description: {
        id: number,
        brandItemId: number,
        language:{},
        itemName: string,
        description: string,
        nutrition: string,
        url: string
    };

    category: {id: number, name: string, sequence: number};

    sequence: number;

    isModOnly: boolean;

    isIngredientOnly: boolean;

    isProduced: boolean;

    unit: StockUnit;

    brandItemId: number;

    shortCutId: number;

    cost: number;

    price: number;

    priceVar: string;

    isTaxable: boolean;

    taxGroup: TaxGroup;

    allowLocalPrice: boolean;

    reqMgrSignOff:boolean;

    notes: string;

    type: string;

    prepTime: string;
    serveFrom: string;
    serveUntil: string;
    serveSunday: boolean;
    serveMonday: boolean;
    serveTuesday: boolean;
    serveWednesday: boolean;
    serveThursday: boolean;
    serveFriday: boolean;
    serveSaturday: boolean;
    hostPlu: string;
    hostedAccountId: string;
    hostedAccountCode: string;
    barCode: string;
    hostSalesDept: string;


    promoServiceGroupId: number;
    waiterOnly: boolean;
    availableTogo: boolean;
    availableTa: boolean;
    availableSelfService: boolean;
    autoAcceptTogo: boolean;
    req86Countdown: boolean;

}