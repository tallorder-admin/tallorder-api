import { MenuItem} from './MenuItem';

export class StockLevel {


    constructor(obj: any) {
        this.id = obj.ID;
        this.tenant = obj.TenantID;
        this.lastUpdateSource = obj.LastUpdateSource;
        this.available = obj.Available;
        this.backOrder = obj.BackOrder;
        this.reserved = obj.Reserved;
        this.runningTotal = obj.RunningTotal;
    }

    id: number;

    tenant: number;

    lastUpdateSource: string;

    available: number;

    backOrder: number;

    reserved: number;

    runningTotal: number;

}