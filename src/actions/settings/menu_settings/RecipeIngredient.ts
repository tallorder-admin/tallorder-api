import {StockUnit} from './StockUnit';

export class RecipeIngredient {

    id: number;

    tenant: number;

    recipeMenuItem: { id: number };

    ingredientMenuItem: { id: number };

    quantity: number;

    stockUnit: StockUnit;

    wastePercentage: number;

}