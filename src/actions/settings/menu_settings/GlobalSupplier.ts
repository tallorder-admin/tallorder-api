export class GlobalSupplier {


    constructor(obj: any) {
        this.id = obj.ID;
        this.name = obj.Name;
        this.contactPerson = obj.ContactPerson;
        this.phone = obj.Phone;
        this.email = obj.Email;
        this.website = obj.Website;
        this.termsAndConditions = obj.TermsAndConditions;
        this.creditLimit = obj.CreditLimit;

    }

    id:number;
    name: string;
    contactPerson: string;
    phone: string;
    email: string;
    website: string;
    termsAndConditions: string;
    creditLimit: number;

}