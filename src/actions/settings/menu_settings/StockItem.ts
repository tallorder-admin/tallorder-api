import {AppEnum} from './AppEnum';
import {StockUnit} from './StockUnit';
import {StockItemSupplier} from './StockItemSupplier';

export class StockItem {


    constructor(obj: any) {
        this.id = obj.ID;
        this.name = obj.Name;

        this.type = obj.Type;

        if (obj.StockUnit)
            this.baseUnit = new StockUnit(obj.StockUnit.dataValues);

        this.stockItemSuppliers =  [];


        obj.StockItemSuppliers.forEach(stockItemSupplier => {
            this.stockItemSuppliers.push(new StockItemSupplier(stockItemSupplier.dataValues));
        });



        this.stockTakeTypes = [];

        obj.StockItemStockTakeTypes.forEach(stockItemStockTakeType=> {
            this.stockTakeTypes.push(
                {
                    id: stockItemStockTakeType.StockTakeType.dataValues.ID,
                    hostId: stockItemStockTakeType.StockTakeType.dataValues.HostID,
                    name: stockItemStockTakeType.StockTakeType.dataValues.Name,
                    description: stockItemStockTakeType.StockTakeType.dataValues.Description
                }
            );
        });

        this.lastCostPerBaseUnit = obj.LastCostPerBaseUnit;
        this.avgCostPerBaseUnit = obj.AverageCostPerBaseUnit;
        this.runningStockValue = obj.RunningStockValue;
        this.runningStockQuantity = obj.RunningStockQuantity;
        this.stockTakePeriod = obj.StockTakePeriod;
        this.isRandomStockTakeItem = obj.IsRandomStockTake;
        this.reorderLevel = obj.ReorderLevel;
        this.alertLevel = obj.AlertLevel;
        this.allowNegativeStockLevel = obj.AllowNegativeStockLevel;


    }

    id: number;
    name: string;
    type: AppEnum;
    baseUnit: StockUnit;
    lastCostPerBaseUnit: number;
    avgCostPerBaseUnit: number;
    runningStockValue: number;
    runningStockQuantity: number;
    stockTakePeriod: string;
    isRandomStockTakeItem: boolean;
    reorderLevel: number;
    alertLevel: number;
    allowNegativeStockLevel: boolean;
    stockItemSuppliers: Array<StockItemSupplier>;
    stockTakeTypes: Array<{ id: number, hostId: string, name: string, description: string }>;
}