

export class StockUnit {


    constructor(obj: any) {
        this.id = obj.ID;
        this.unit = obj.Unit;
    }

    id:number;

    unit: string;

}