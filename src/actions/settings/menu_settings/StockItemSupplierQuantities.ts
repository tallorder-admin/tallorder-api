export class StockItemSupplierQuantities {


    constructor(obj: any) {
        this.id = obj.ID;
        this.description = obj.Description;
        this.economicOrderQuantity = obj.EconomicOrderQuantity;
        this.conversionFactor = obj.ConversionFactor;
        this.barcode = obj.Barcode;
        this.plu = obj.Plu;
    }

    id: number;
    description: string;
    economicOrderQuantity: string;
    conversionFactor: string;
    barcode: string;
    plu: string;


}