import {Context} from "../../util/Context";

export class DeviceRegister {

    /**
     * Get all devices
     * @param {number} tenantId
     * @returns {Promise<Array<DeviceRegister>>}
     */
    public static async getAll(tenantId: number): Promise<Array<DeviceRegister>> {
        Context.logger.debug(`Retrieving all DeviceRegister entries for tenant ${tenantId}`);

        const data = await Context.tallorder.DeviceRegister.findAll({where: {TenantID: tenantId}, raw: true});
        if(data.length === 0) {
            console.log('throwing error');
            throw new Error('No devices found');
        } else {
            return data;
        }

    }

    /**
     * Get single device
     * @param {number} tenantId
     * @param {number} deviceId
     * @returns {Promise<Array<DeviceRegister>>}
     */
    public static async deviceView(tenantId: number, deviceId: number): Promise<Array<DeviceRegister>> {
        Context.logger.debug(`Retrieving single DeviceRegister entry for id ${deviceId} and tenant ${tenantId}`);

        const data = Context.tallorder.DeviceRegister.findOne({where: {TenantID: tenantId, ID: deviceId}, include: [Context.tallorder.DeviceTypes]});
        if(data.length === 0) {
            console.log('throwing error');
            throw new Error('No device found');
        } else {
            return data;
        }

    }

}

