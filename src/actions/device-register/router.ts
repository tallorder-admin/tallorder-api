import {DeviceRegister} from "./DeviceRegister";
import {GenericActions} from "../Generic";
import {Context} from "../../util/Context";
import {HttpError} from "../../util/Error";
import {Response} from "../../util/Response";
import {AccessController} from "../auth/AccessController";

exports.getAllDevicesHttp = async (event, context, callback) => {
    const logger = Context.logger;
    logger.info('-------- deviceRegister getAllDevicesHttp function --------');
    logger.info(JSON.stringify(event, null, 2));
    logger.info(JSON.stringify(context, null, 2));
    context.callbackWaitsForEmptyEventLoop = false;

    try {

        if (!event.queryStringParameters || !event.queryStringParameters.tid) {
            throw new HttpError(400, 'Input', "tid not specified");
        }

        if (!event.requestContext.authorizer.iss) {
            throw new HttpError(400, 'Input', "This function requires authorizer to be set");
        }
        Context.userId = event.requestContext.authorizer.iss.slice(2);

        const tenantId: number = event.queryStringParameters.tid;

        await GenericActions.bootstrap(['tallorder']);

        const access = await AccessController.isAllowedAccess(Context.userId, tenantId, 'DeviceRegisterController', 'index');
        if(access === false) {
            throw new HttpError(401, 'Access Denied', 'Insufficient access rights');
        }

        const devices = await DeviceRegister.getAll(tenantId);
        callback(null, Response.format(devices));

    } catch (error) {
        logger.info(`Error in execution ${error.toString()}`);
        callback(null, Response.error(error));
    }

};

exports.getDeviceHttp = async (event, context, callback) => {
    const logger = Context.logger;
    logger.info('-------- deviceRegister getDeviceHttp function --------');
    logger.info(JSON.stringify(event, null, 2));
    logger.info(JSON.stringify(context, null, 2));
    context.callbackWaitsForEmptyEventLoop = false;

    try {

        if (!event.queryStringParameters || !event.queryStringParameters.tid || !event.pathParameters.id) {
            let msg = '';
            if(!event.queryStringParameters.tid) {
                msg = msg + "tid not specified ";
            }
            if(!event.pathParameters.id) {
                msg = msg + "id not specified ";
            }
            throw new HttpError(400, 'Input', msg);
        }

        if (!event.requestContext.authorizer.iss) {
            throw new HttpError(400, 'Input', "This function requires authorizer to be set");
        }
        Context.userId = event.requestContext.authorizer.iss.slice(2);

        const tenantId: number = event.queryStringParameters.tid;
        const deviceId : number = event.pathParameters.id;

        await GenericActions.bootstrap(['tallorder']);

        const access = await AccessController.isAllowedAccess(Context.userId, tenantId, 'DeviceRegisterController', 'view');
        if(access === false) {
            throw new HttpError(401, 'Access Denied', 'Insufficient access rights');
        }

        const device = await DeviceRegister.deviceView(tenantId, deviceId);
        callback(null, Response.format(device));

    } catch (error) {
        logger.info(`Error in execution ${error.toString()}`);
        callback(null, Response.error(error));
    }
};
