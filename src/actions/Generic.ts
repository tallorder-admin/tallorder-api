import * as dbTables from '../models/tallorder-ts/db.tables';
import * as dbBITables from '../models/tallorderBI-ts/db.tables';
import {Sequelize} from 'sequelize-typescript';
import {Context} from "../util/Context";
import {ConfigLocal} from "../config/ConfigMain";
import {associations} from "../util/Associations";

export class GenericActions {

    logger = Context.logger;
    private static _stage : string;
    private static _localConfig : object;

    public static async bootstrap(connections = ['tallorder','tallorderBI']) {

        Context.auth = {jwtKey: 'ta110rd3rJwtK3y'};

        Context.logger = {
            info: (scope, text) => console.log(`${scope} ${text ? text : ''}`),
            debug: (scope, text) => console.log(`${scope} ${text ? text : ''}`),
            silly: (scope, text) => console.log(`${scope} ${text ? text : ''}`),
            warn: (scope, text) => console.log(`${scope} ${text ? text : ''}`),
            error: (text) => console.log(`ERROR ${text ? text : ''}`),
        };

        const logger = Context.logger;

        Context.isLambda = !!(
            (process.env.LAMBDA_TASK_ROOT && process.env.AWS_EXECUTION_ENV) ||
            false
        );

        if(Context.isLambda) {
            this._stage = process.env.stage;
            logger.info(`stage ${ process.env.stage }`);
        } else {
            this._stage = 'dev';
        }
        this._localConfig = ConfigLocal.config(this._stage);

        if(connections.includes("tallorder")) {
            logger.info('-------- Connecting to tallorder OPS--------');
            const sequelize = new Sequelize({
                host: this._localConfig["dbHost"],
                name: 'tallorder',
                logging: false,
                dialect: 'mysql',
                username: this._localConfig["dbUser"],
                password: this._localConfig["dbPass"],
                pool: {
                    max: 5,
                    min: 0,
                    idle: 20000,
                    acquire: 20000
                }
            });
            logger.info('-------- Authenticating OPS...');
            await sequelize.authenticate();
            console.log('Connection to OPS has been established successfully.');

            Context.sequelize = sequelize;
            Context.tallorder = dbTables.getModels(sequelize);

            associations(Context.tallorder);
        }

        if(connections.includes("tallorderBI")) {
            logger.info('-------- Connecting to tallorder BI --------');
            const sequelizeBI = new Sequelize({
                host: this._localConfig["dbHost"],
                name: 'tallorderBI',
                logging: false,
                dialect: 'mysql',
                username: this._localConfig["dbUser"],
                password: this._localConfig["dbPass"],
                pool: {
                    max: 5,
                    min: 0,
                    idle: 20000,
                    acquire: 20000
                }
            });

            logger.info('-------- Authenticating BI...');

            await sequelizeBI.authenticate();

            console.log('Connection to BI has been established successfully.');

            Context.tallorderBI = dbBITables.getModels(sequelizeBI);
        }



    }




}