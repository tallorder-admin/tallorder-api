export class DeviceRegister {
    ID?: number;
    TenantID: number;
    DeviceTypeID?: number;
    Device?: string;
    DateReg?: any;
    Active?: number;
    Online?: number;
    DeviceUID?: string;
    Master?: number;
    Address?: string;
    Hostname?: string;
    DefaultPrinter?: number;
    DevicePIN?: string;
    PosGPS?: string;
    LastConnectDateTime?: any;
    Version?: string;
    AuthCode?: string;
    PushDeviceToken?: string;
    IsArchived?: number;
    LastUpdate?: any;
    LocationID?: number;
}
